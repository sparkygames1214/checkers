﻿using Model;
using UnityEngine;

public interface IGameRoomGroupInfo
{
    string RoomName { get; }
    string Id { get; }
    int BetAmount { get; }
    int PlayerCount { get; }
    Model.Theme Theme { get; }
}

[System.Serializable]
public struct GameRoomGroupGroupInfo : IGameRoomGroupInfo
{
#pragma warning disable 649
    [SerializeField] private string _id;
    [SerializeField] private string _roomName;
    [SerializeField] private int _betAmount;
    [SerializeField] private Theme _theme;
#pragma warning restore 649
    public string Id => _id;

    public string RoomName => _roomName;

    public int BetAmount => _betAmount;

    // ReSharper disable once UnassignedGetOnlyAutoProperty
    public int PlayerCount { get; set; }

    public Theme Theme => _theme;
}