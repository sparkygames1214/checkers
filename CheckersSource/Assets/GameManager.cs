﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

// ReSharper disable once HollowTypeName
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public static event Action<Scene,object> BeforeSceneLoaded;
    public static event Action<Scene> AfterSceneLoaded;
    public static event Action<Scene> SceneUnLoaded;


    public static GameProgress GameProgress
    {
        get => _gameProgress;
        private set
        {
            _gameProgress = value;
            if (_gameProgress!=null)
            {
                _gameProgress.Terminated += progress => GameProgress = null;
            }
        }
    }

    public static int BoardSize
    {
        get => SelectedRule.BoardSize;
    }

    public static IEnumerable<Rule> Rules => global::Rules.Default;

    public static Rule SelectedRule
    {
        get
        {
          var id =  PlayerPrefs.GetString(nameof(SelectedRule), Rules.First().Id);
          return Rules.FirstOrDefault(rule => rule.Id == id);
        }
        set => PlayerPrefs.SetString(nameof(SelectedRule),value.Id);
    }

    private GameStartUpData _gameStartUpData;
    private PlayerSelectionStartUpData _playerSelectionStartUpData;

    private readonly Dictionary<Scene, object> _loadedSceneDataDict = new Dictionary<Scene, object>();
    private static GameProgress _gameProgress;

    public static IEnumerable<Scene> LoadedScenes => Utils.GetRange(SceneManager.sceneCount)
        .Select(i =>  (Scene)Enum.Parse(typeof(Scene),SceneManager.GetSceneAt(i).name));// Instance._loadedSceneList;

    public static Scene ActiveScene => (Scene)Enum.Parse(typeof(Scene), SceneManager.GetActiveScene().name);




    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
        SceneManager.sceneUnloaded += SceneManagerOnSceneUnloaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneManagerOnSceneLoaded;
        SceneManager.sceneUnloaded -= SceneManagerOnSceneUnloaded;
    }

    public static void StartTheGame(GameStartUpData data)
    {
        Instance._gameStartUpData = data;
        GameProgress = data.GameProgress ?? new GameProgress();
        LoadScene(Scene.MainScene,data);
    }

    public static void OpenThePlayerSelectionScene(PlayerSelectionStartUpData data)
    {
        LoadScene(Scene.PlayerSelection,data);
    }

    private void SceneManagerOnSceneUnloaded(UnityEngine.SceneManagement.Scene scene)
    {
       SceneUnLoaded?.Invoke((Scene) Enum.Parse(typeof(Scene),scene.name));
    }

    public static void LoadScene(Scene scene, object data = null, LoadSceneMode mode = LoadSceneMode.Single)
    {
        if (data != null)
        {
            var dict = Instance._loadedSceneDataDict;
            if (dict.ContainsKey(scene))
            {
                dict[scene] = data;
            }
            else
            {
                dict.Add(scene, data);
            }
        }
        BeforeSceneLoaded?.Invoke(scene,data);
        SceneManager.LoadScene(scene.ToString(), mode);
    }


    private void SceneManagerOnSceneLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode loadSceneMode)
    {
        AfterSceneLoaded?.Invoke((Scene)Enum.Parse(typeof(Scene),scene.name));
    }


    public static object GetAndRemoveLoadedSceneData(Scene scene)
    {
        var data = Instance._loadedSceneDataDict[scene];
        Instance._loadedSceneDataDict.Remove(scene);
        return data;
    }

    public static T GetAndRemoveLoadedSceneData<T>(Scene scene)
    {
        if (!Instance._loadedSceneDataDict.ContainsKey(scene))
            return default(T);

        var data = Instance._loadedSceneDataDict[scene];
        Instance._loadedSceneDataDict.Remove(scene);
        return (T) data;
    }
}


public enum Scene
{
    Splash=0,
    AuthScene = 1,
    MainMenu = 2,
    RoomSelectionScene = 3,
    PlayerSelection = 4,
    MainScene = 5
}

public struct PlayerSelectionStartUpData
{
    public IGameRoomGroupInfo GameRoomGroupInfo { get; set; }
    public bool IsPrivateRoom { get; set; }
    public string SelectedRoomName { get; set; }
    public bool IsJoinToExistingRoom => !string.IsNullOrWhiteSpace(SelectedRoomName);
    public PhotonPlayerSelectionManager.CreatedRoomListener OnCreatedRoom { get; set; }
    
}

public struct GameStartUpData
{
    public bool IsNetworkMode { get; set; }
    public UnitType MyUnitType { get; set; }
    public bool VsPlayer { get; set; }
    public IGameRoomGroupInfo GameRoomGroupInfo { get; set; }
    public Rule Rule { get; set; }

//    public GameStartUpNetworkData? GameStartUpNetworkData { get; set; }

    public GameProgress GameProgress { get; set; }

    public Model.Theme Theme => GameRoomGroupInfo?.Theme;
    public PlayerData OpponentPlayerData { get; set; }

    public struct PlayerData
    {
        public string Name { get; set; }
        public string AvatarUrl { get; set; }
        public PhotonPlayer PhotonPlayer { get; set; }

        public static PlayerData FromPhotonPlayer(PhotonPlayer player)
        {
            return new PlayerData
            {
                Name = player.NickName,
                AvatarUrl = player.GetAvatarUrl(),
                PhotonPlayer = player
            };
        }
    }
}

//public struct GameStartUpNetworkData
//{
//    public UnitType MyUnitType { get; set; } 
////    public PhotonPlayer MyPhotonPlayer { get; set; }
//    public PhotonPlayer OpponentPhotonPlayer { get; set; }
//}







