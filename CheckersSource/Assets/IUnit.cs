﻿// ReSharper disable once CheckNamespace

using System;
using System.Collections.Generic;
using System.Linq;
using Game;
using JetBrains.Annotations;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace Game
{
    public interface IUnit:IUnityObject
    {
        string Id { get; }
        event Action<IUnit> Clicked;
        Vector2 Size { get; set; }
        IBox Box { get; set; }
        CheckerBoard CheckerBoard { get; set; }
        bool Crowned { get; }
        UnitType UnitType { get; set; }
        void SetCrown(Action onComplete = null);
    }


    public interface IUnityObject
    {
        // ReSharper disable once InconsistentNaming
         Transform transform { get;}
        // ReSharper disable once InconsistentNaming
        GameObject gameObject { get; }
    }
}

namespace Model
{
    public class UnitData
    {
        public bool Crowned { get; set; }
        public VecInt Coordinate { get; set; }
        public BoxData Box { get; set; }
        public UnitType UnitType { get; set; }
    }

    public class BoxData
    {
        public VecInt Coordinate { get; set; }
        public UnitData Unit { get; set; }
    }

    public class MoveInfoData
    {
        public UnitData Unit { get; set; }
        public IEnumerable<MoveData> Moves { get; set; }
    }

    public class MoveData
    {
        public BoxData StartBox { get; set; }
        public BoxData EndBox { get; set; }
        public UnitData DestroyableUnit { get; set; }
        public bool IsCut => DestroyableUnit != null;
    }


    public interface IBoardData
    {
        VecInt BoardSize { get; }
        BoxData this[int x,int y] { get; }
        IEnumerable<BoxData> BoxEnumerable { get; }
        void ApplyModification(IEnumerable<MoveData> moves);
        void ClearModifications();
    }

    public class BoardData : IBoardData
    {
        public VecInt BoardSize { get; set; }

        public BoxData this[int x, int y] => _boxList[x * BoardSize.y + y];


        public IEnumerable<BoxData> BoxEnumerable => _boxList;

        private readonly List<BoxData> _boxList = new List<BoxData>();

        private readonly List<MoveData> _modifications = new List<MoveData>();

        public BoardData(IEnumerable<BoxData> boxDatas,VecInt boardSize)
        {
            var datas = boxDatas.ToList();
            _boxList.AddRange(datas);
            BoardSize = boardSize;
        }

        public void ApplyModification(IEnumerable<MoveData> moves)
        {
      

            foreach (var moveData in moves)
            {
                var unit = moveData.StartBox.Unit;
                moveData.StartBox.Unit = null;
                unit.Coordinate = moveData.EndBox.Coordinate;
                moveData.EndBox.Unit = unit;

                if (moveData.IsCut)
                {
                    var coordinate = moveData.DestroyableUnit.Coordinate;
                    var boxData = this[coordinate.x,coordinate.y];
                    boxData.Unit = null;
                }
                _modifications.Add(moveData);
            }

        }

        public void ClearModifications()
        {
            var moveDatas = _modifications.ToList();
            moveDatas.Reverse();
            foreach (var moveData in moveDatas)
            {
                var unit = moveData.EndBox.Unit;
                moveData.EndBox.Unit = null;
                unit.Coordinate = moveData.StartBox.Coordinate;
                moveData.StartBox.Unit = unit;

                if (moveData.IsCut)
                {
                    var coordinate = moveData.DestroyableUnit.Coordinate;
                    var boxData = this[coordinate.x, coordinate.y];
                    boxData.Unit = moveData.DestroyableUnit;
                }

            }
            _modifications.Clear();
        }
    }

    public abstract class MoveCalculator
    {
        public IBoardData Board { get; set; }
        public Rule Rule { get; }
        public VecInt BoxSize => Board.BoardSize;

        protected MoveCalculator(Rule rule)
        {
            Rule = rule;
        }

        public abstract IEnumerable<MoveInfoData[]> GetSimpleMoves(UnitType unit);
        public abstract IEnumerable<MoveInfoData[]> GetAllCuts(UnitType unit);

        public IEnumerable<MoveInfoData[]> GetMoves(UnitType unit)
        {
            var moves = GetAllCuts(unit).ToList();
            return moves.Count == 0 ? GetSimpleMoves(unit) : moves;
        }

        public IEnumerable<MoveInfoData[]> GetSimpleMoveForTargetBox(BoxData box,UnitType type)
        {
            return GetSimpleMoves(type).Where(datas => { return datas.Any(data => data.Moves.First().EndBox == box); });
        }
    }

    public class CornerMoveCalculator : MoveCalculator
    {
        public CornerMoveCalculator(Rule rule) : base(rule)
        {
        }

        public override IEnumerable<MoveInfoData[]> GetSimpleMoves(UnitType toUnit)
        {
            return Board.BoxEnumerable.Where(box => box.Unit != null && box.Unit.UnitType == toUnit).Select(
                box =>
                {
                    var unit = box.Unit;
                    var list = GetSimpleMovesForBox(unit.Box, unit.Crowned && Rule.CanLeadMoveMultiBox,
                        unit.Crowned ? MoveDirection.Both : unit.UnitType.Forward()).ToList();
                    return new
                    {
                        Unit = unit,
                        ms = list
                    };
                }).Where(arg => arg.ms.Count > 0).Select(arg =>
                {
                    return arg.ms.Select(move => new MoveInfoData
                    {
                        Unit = arg.Unit,
                        Moves = new[] { move }
                    }).ToArray();
                });
        }

        [NotNull]
        // ReSharper disable once MethodTooLong
        private IEnumerable<MoveData> GetSimpleMovesForBox(BoxData startBox, bool canSkipBox, MoveDirection direction)
        {
            var moves = new List<MoveData>();
            var coordinates = GetCornerCoordinates(startBox.Coordinate);
            var boxes = Board;

            void AllSimpleMovesForPath(IEnumerable<VecInt> coordinatePath)
            {
                if (coordinatePath != null)
                {
                    var simpleMovesForPath = GetSimpleMovesForPath(startBox, coordinatePath.Select(i => boxes[i.x, i.y]), canSkipBox);
                    if (simpleMovesForPath != null) moves.AddRange(simpleMovesForPath);
                }
            }

            switch (direction)
            {
                case MoveDirection.Positive:
                    AllSimpleMovesForPath(coordinates[0]);
                    AllSimpleMovesForPath(coordinates[3]);
                    break;
                case MoveDirection.Negative:
                    AllSimpleMovesForPath(coordinates[1]);
                    AllSimpleMovesForPath(coordinates[2]);
                    break;
                case MoveDirection.Both:
                    AllSimpleMovesForPath(coordinates[0]);
                    AllSimpleMovesForPath(coordinates[1]);
                    AllSimpleMovesForPath(coordinates[2]);
                    AllSimpleMovesForPath(coordinates[3]);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }

            return moves;
        }

        // ReSharper disable once TooManyDeclarations
        private IEnumerable<MoveData> GetSimpleMovesForPath(BoxData startBox, [NotNull]IEnumerable<BoxData> path, bool canSkipBox)
        {
            var boxes = path.ToList();
            if (boxes.Count == 0)
            {
                return null;
            }

            if (!canSkipBox)
            {
                return boxes[0].Unit == null
                    ? new[] { new MoveData { StartBox = startBox, EndBox = boxes[0] } }
                    : null;
            }

            for (var i = 0; i < boxes.Count; i++)
            {
                var box = boxes[i];
                if (box.Unit != null)
                {
                    return Enumerable.Range(0, i).Select(j => new MoveData
                    {
                        StartBox = startBox,
                        EndBox = boxes[j]
                    });
                }
            }

            return Enumerable.Range(0, boxes.Count).Select(j => new MoveData
            {
                StartBox = startBox,
                EndBox = boxes[j]
            });

        }

        public override IEnumerable<MoveInfoData[]> GetAllCuts(UnitType toUnit)
        {
            var allCuts = Board.BoxEnumerable.Where(box => box.Unit != null && box.Unit.UnitType == toUnit).Select(
                box =>
                {
                    var unit = box.Unit;
                    var list = GetAllMoveCuts(box, unit.Crowned && Rule.CanLeadMoveMultiBox,
                        !unit.Crowned || Rule.IsLeadCutEndPositionFixed,

                        unit.Crowned || Rule.AllowBackwardCutForPawns ? MoveDirection.Both : unit.UnitType.Forward(),
                        unit.UnitType.Other(), unit.Crowned || Rule.AllowPawnCutLead,
                        !unit.Crowned && !Rule.ContinuePawnJumpAtBoardEnd)
                        .Select(moves => moves.ToArray()).ToList();
                    return new
                    {
                        Unit = unit,
                        ms = list
                    };
                }).Where(arg => arg.ms.Any()).ToList();

            var cutOrders = Rule.CutOrders.ToList();

            if (cutOrders.Count == 0)
            {
                cutOrders.Add(CutOrder.MultipleCut);
            }

            var scoreAndCuts = allCuts.Select(arg => new { Score = 0L, arg.Unit, BestMoves = arg.ms });

            for (var i = 0; i < cutOrders.Count; i++)
            {
                var j = i;
                scoreAndCuts = scoreAndCuts.Select(arg =>
                {
                    var score = ScoreToCutReason(cutOrders[j], arg.Unit, arg.BestMoves, out var moves);
                    return new
                    {
                        Score = arg.Score + score * (long)Mathf.Pow(10, 5 - j),
                        arg.Unit,
                        BestMoves = moves,
                    };
                });
            }

            var sortedCuts = scoreAndCuts.OrderByDescending(arg => arg.Score).ToList();

        

            return sortedCuts
                .Where(arg => arg.Score == sortedCuts.First().Score)
                .Select(arg => arg.BestMoves.Select(moves => new MoveInfoData
                {
                    Unit = arg.Unit,
                    Moves = moves
                }).ToArray());
        }

        private int ScoreToCutReason(CutOrder order, UnitData unit, List<MoveData[]> moves, out List<MoveData[]> bestMoves)
        {
            var list = moves.Select(enumerable => enumerable.ToList()).ToList();
            switch (order)
            {
                case CutOrder.MultipleCut:
                    var ls = list.Select(info => new
                    { Val = info.Count(move => move.DestroyableUnit != null), Info = info })
                        .OrderByDescending(arg => arg.Val)
                        .ToList();
                    if (ls.Any())
                    {

                        bestMoves = ls.Where(arg => arg.Val == ls.First().Val).Select(arg => arg.Info.ToArray())
                            .ToList();
                        return ls.First().Val;
                    }

                    bestMoves = moves;
                    return 0;

                case CutOrder.MultipleLeadCut:
                    ls = list.Select(info => new
                    {
                        Val = info.Count(move => move.DestroyableUnit != null && move.DestroyableUnit.Crowned),
                        Info = info
                    })
                        .OrderByDescending(arg => arg.Val)
                        .ToList();
                    if (ls.Any())
                    {
                        bestMoves = ls.Where(arg => arg.Val == ls.First().Val).Select(arg => arg.Info.ToArray())
                            .ToList();
                        return ls.First().Val;
                    }

                    bestMoves = moves;
                    return 0;
                case CutOrder.MultipleCutWithLead:
                    ls = list.Select(info => new
                    {
                        Val = info.Count(move => move.DestroyableUnit != null),
                        Info = info
                    })
                        .OrderByDescending(arg => arg.Val)
                        .ToList();
                    if (ls.Any() && unit.Crowned)
                    {
                        bestMoves = unit.Crowned
                            ? ls.Where(arg => arg.Val == ls.First().Val).Select(arg => arg.Info.ToArray()).ToList()
                            : new List<MoveData[]>();
                        Debug.Log("Value:" + ls.First().Val);
                        return ls.First().Val;
                    }

                    bestMoves = moves;
                    return 0;
                case CutOrder.CaptureKinFirst:
                    ls = list.Select(info => new
                    {
                        Val = info.FindIndex(move => move.DestroyableUnit != null && move.DestroyableUnit.Crowned),
                        Info = info
                    })
                        .Where(arg => arg.Val >= 0)
                        .OrderBy(arg => arg.Val)
                        .ToList();
                    if (ls.Any())
                    {
                        bestMoves =
                            ls.Where(arg => arg.Val == ls.First().Val).Select(arg => arg.Info.ToArray()).ToList();
                        return 10 - ls.First().Val;
                    }

                    bestMoves = moves;
                    return 0;
                default:
                    throw new ArgumentOutOfRangeException(nameof(order), order, null);
            }
        }

        private MoveData GetCut(BoxData startBox, [NotNull] IEnumerable<BoxData> path, bool canSkipBox,
            UnitType targetType, bool allowLeadCut = true, IEnumerable<UnitData> ignoreUnits = null)
        {
            var boxes = path.ToList();
            if (boxes.Count <= 1)
            {
                return null;
            }

            for (var index = 0; index < (canSkipBox ? boxes.Count : 1); index++)
            {
                var box = boxes[index];
                if (box.Unit != null)
                {
                    if (box.Unit.UnitType != targetType || (!allowLeadCut && box.Unit.Crowned))
                    {
                        return null;
                    }

                    return index < boxes.Count - 1
                           && (boxes[index + 1].Unit == null ||
                               (ignoreUnits != null && ignoreUnits.Any(unit => unit == boxes[index + 1].Unit))
                           ) // next box is null
                           && (index == 0 || !box.Unit.Crowned) // cannot cut crow with skip box
                        ? new MoveData
                        {
                            StartBox = startBox,
                            EndBox = boxes[index + 1],
                            DestroyableUnit = boxes[index].Unit
                        }
                        : null;
                }
            }

            return null;
        }

        private IEnumerable<MoveData> GetCutsWithNonFixedEnd(BoxData startBox, [NotNull] IEnumerable<BoxData> path,
            UnitType targetType, IEnumerable<UnitData> ignoreUnits = null)
        {
            var boxes = path.ToList();
            if (boxes.Count <= 1)
            {
                yield break;
            }

            var ignoreUnitsList = ignoreUnits?.ToList() ?? new List<UnitData>();

            for (var index = 0; index < boxes.Count - 1; index++)
            {
                var box = boxes[index];
                if (box.Unit != null)
                {
                    if (box.Unit.UnitType == targetType && boxes[index + 1].Unit == null && ignoreUnitsList.All(unit => unit != box.Unit))
                    {
                        for (var i = index + 1; i < boxes.Count; i++)
                        {
                            if (boxes[i].Unit != null)
                                yield break;
                            yield return new MoveData
                            {
                                StartBox = startBox,
                                EndBox = boxes[i],
                                DestroyableUnit = box.Unit
                            };
                        }

                    }
                    yield break;
                }
            }
        }

        private IEnumerable<IEnumerable<MoveData>> GetAllMoveCuts(BoxData startBox, bool canSkipBox, bool fixedEnd,
            MoveDirection direction, UnitType targetType, bool allowLeadCut = true, bool endJumpAtEnd = false, List<MoveData> lastMoves = null)
        {
            var coordinates = GetCornerCoordinates(startBox.Coordinate);
            var list = new List<List<MoveData>>();
            var boxes = Board;

            void AllMovesForPath(IEnumerable<VecInt> coordinatePath)
            {
                if (coordinatePath == null)
                {
                    return;
                }


                if (fixedEnd)
                {

                    if (endJumpAtEnd && direction == MoveDirection.Both)
                    {
                        var endRow = targetType.Backward() == MoveDirection.Positive ? Board.BoardSize.y - 1 : 0;
                        if (endRow == startBox.Coordinate.y)
                        {
                            return;
                        }
                    }


                    var moveNullable = GetCut(startBox, coordinatePath.Select(i => boxes[i.x, i.y]),
                        canSkipBox, targetType, allowLeadCut, lastMoves?.Select(move => move.DestroyableUnit));
                    if (moveNullable != null)
                    {
                        var move = moveNullable;

                        var previousMoves = lastMoves == null ? new List<MoveData>() : lastMoves.ToList();

                        if (previousMoves.All(m => m.DestroyableUnit != move.DestroyableUnit))
                        {
                            var currentMoves = new List<MoveData>(previousMoves) { move };
                            var cuts = GetAllMoveCuts(moveNullable.EndBox, canSkipBox, true, direction,
                                targetType, allowLeadCut, endJumpAtEnd, currentMoves);


                            foreach (var cut in cuts)
                            {
                                if (cut != null)
                                {
                                    list.Add(cut.ToList());
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var move in GetCutsWithNonFixedEnd(startBox, coordinatePath.Select(i => boxes[i.x, i.y]),
                        targetType, lastMoves?.Select(move => move.DestroyableUnit)))
                    {
                        var previousMoves = lastMoves == null ? new List<MoveData>() : new List<MoveData>(lastMoves);

                        if (previousMoves.All(m => m.DestroyableUnit != move.DestroyableUnit))
                        {
                            var currentMoves = new List<MoveData>(previousMoves) { move };
                            var cuts = GetAllMoveCuts(move.EndBox, canSkipBox, false, direction,
                                targetType, allowLeadCut, endJumpAtEnd, currentMoves);


                            foreach (var cut in cuts)
                            {
                                if (cut != null)
                                {
                                    list.Add(cut.ToList());
                                }
                            }
                        }
                    }
                }
            }

            if (direction == MoveDirection.Positive)
            {
                AllMovesForPath(coordinates[0]);
                AllMovesForPath(coordinates[3]);
            }
            else if (direction == MoveDirection.Negative)
            {
                AllMovesForPath(coordinates[1]);
                AllMovesForPath(coordinates[2]);
            }
            else if (direction == MoveDirection.Both)
            {
                AllMovesForPath(coordinates[0]);
                AllMovesForPath(coordinates[1]);
                AllMovesForPath(coordinates[2]);
                AllMovesForPath(coordinates[3]);
            }

            return list.Count > 0 || lastMoves == null
                ? list.Select(moves => (IEnumerable<MoveData>)moves)
                : new List<IEnumerable<MoveData>> { lastMoves };
        }

        public IEnumerable<VecInt>[] GetCornerCoordinates(VecInt coordinate)
        {
            var coordinates = new IEnumerable<VecInt>[4];
            //TopRight
            coordinates[0] = coordinate.x >= BoxSize.x - 1 || coordinate.y >= BoxSize.y - 1
                ? null
                : Enumerable.Range(0, Mathf.Min((BoxSize.x - 1 - coordinate.x), (BoxSize.y - 1 - coordinate.y)))
                    .Select(i => new VecInt(coordinate.x + (i + 1), coordinate.y + (i + 1))).ToList();

            //BottomRight
            coordinates[1] = coordinate.x >= BoxSize.x - 1 || coordinate.y <= 0
                ? null
                : Enumerable.Range(0, Mathf.Min((BoxSize.x - 1 - coordinate.x), (coordinate.y)))
                    .Select(i => new VecInt(coordinate.x + (i + 1), coordinate.y - (i + 1))).ToList();

            //BottomLeft
            coordinates[2] = coordinate.x <= 0 || coordinate.y <= 0
                ? null
                : Enumerable.Range(0, Mathf.Min((coordinate.x), coordinate.y))
                    .Select(i => new VecInt(coordinate.x - (i + 1), coordinate.y - (i + 1))).ToList();

            //TopLeft
            coordinates[3] = coordinate.x <= 0 || coordinate.y >= BoxSize.y - 1
                ? null
                : Enumerable.Range(0, Mathf.Min(coordinate.x, (BoxSize.y - 1 - coordinate.y)))
                    .Select(i => new VecInt(coordinate.x - (i + 1), coordinate.y + (i + 1))).ToList();


            return coordinates;
        }
    }
}