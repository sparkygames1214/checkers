﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using Object = UnityEngine.Object;

public class ImageTask:CustomYieldInstruction
{
    private static readonly Dictionary<string,ImageTask> _urlVsImgDict = new Dictionary<string, ImageTask>();
    public bool IsDone { get; set; }
    public Sprite Sprite { get; set; }
    public string Error { get; set; }
    private readonly bool _caching;
    private SimpleMono _mono;

    public override bool keepWaiting => !IsDone;

    public ImageTask(string url,bool caching=true,bool forceReload=false,Action<ImageTask> onComplete=null)
    {
        _caching = caching;

        if (string.IsNullOrEmpty(url))
        {
            Error = "Empty Url";
            IsDone = true;
            return;
        }

        if (!forceReload && _urlVsImgDict.ContainsKey(url))
        {
            var imageTask = _urlVsImgDict[url];
            IsDone = imageTask.IsDone;
            Sprite = imageTask.Sprite;
            Error = imageTask.Error;
            onComplete?.Invoke(this);
            return;
        }

        var gameObject = DontDestroyMono.CreateChild();
        _mono= gameObject.AddComponent<SimpleMono>();
        _mono.StartCoroutine(Enumerator(url, ()=>onComplete?.Invoke(this)));
    }

    private IEnumerator Enumerator(string url,Action onComplete=null)
    {
        if (url.StartsWith("local"))
        {
            Sprite = ResourceManager.GetAvatarByUrl(url);
        }
        else
        {
            var www = new WWW(url);
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                www.texture.wrapMode = TextureWrapMode.Clamp;
                www.texture.filterMode = FilterMode.Bilinear;
                Sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.one / 2);
            }
            Error = www.error;

        }

        IsDone = true;
        if (_caching)
        {
            if (!_urlVsImgDict.ContainsKey(url))
            {
                _urlVsImgDict.Add(url,this);
            }
            else
            {
                _urlVsImgDict[url] = this;
            }
        }

        onComplete?.Invoke();
        Object.Destroy(_mono.gameObject);
        _mono = null;
    }

    public void Cancel()
    {
        _mono.StopAllCoroutines();
        Object.Destroy(_mono.gameObject);
        IsDone = true;
        Error = "Cancelled";
    }
}