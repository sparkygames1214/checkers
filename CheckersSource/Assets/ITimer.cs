﻿using System;

public interface ITimer
{
    event EventHandler Completed;
    event Action<object,float> Tick;
    bool Running { get;  }
    float Duration { get; set; }
    float TimeLeft { get; }
    void StartTimer();
    void StopTimer();
    void ResetTimer();
}