﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    // ReSharper disable once HollowTypeName
    public class GameHandler : MonoBehaviour, IInilizable<GameHandler.InitParams>, IGameHandler
    {
        public event Action<GameHandlerGameOverResult> GameOver;
        public event Action<IPlayer> TurnStart;
        public event Action<IPlayer> TurnEnd;

        public Unit UnitPrefab { get; private set; }

        public bool Initialized { get; private set; }

        public IPlayer CurrentPlayer => CurrentTurnState != TurnState.Playing ? null : _currentOrLastActivePlayer;

        public IEnumerable<IUnit> Units => _allUnitList;

        public IEnumerable<IUnit> ActiveUnits => _activeUnitList;

        public TurnState CurrentTurnState { get; set; }
        public IPlayer WhitePlayer { get; private set; }
        public IPlayer BlackPlayer { get; private set; }
        public GameHandlerState CurrentState { get; private set; }
        public float TurnElapsedTime => CurrentTurnState == TurnState.Playing ? _timer.Duration - _timer.TimeLeft : -1;

        public float TurnTime
        {
            get { return _turnTime; }
            set
            {
                if (CurrentState != GameHandlerState.None)
                    throw new InvalidOperationException("Cannot be Change the time after start the game");

                _turnTime = value;
            }
        }

        private ICheckerBoard _board;
        private IPlayer _currentOrLastActivePlayer;

        private readonly List<IUnit> _activeUnitList = new List<IUnit>();
        private readonly List<IUnit> _allUnitList = new List<IUnit>();
        private Model.MoveCalculator _moveCalculator;
        private ITimer _timer;
        private float _turnTime = 30f;

        private VecInt BoardSize => _board.BoardSize;

        // ReSharper disable once MethodTooLong
        public void Init(InitParams item)
        {
            if (Initialized)
            {
                return;
            }

            UnitPrefab = item.UnitPrefab;
            TurnTime = item.TurnTime;
            _timer = item.Timer;
            _timer.Duration = TurnTime;
            _board = item.Board;
            _moveCalculator = new Model.CornerMoveCalculator(LevelManager.Instance.Rule);
            WhitePlayer = item.WhitePlayer;
            BlackPlayer = item.BlackPlayer;

            //Set Necessary Properties of player
            WhitePlayer.GameHandler = this;
            BlackPlayer.GameHandler = this;

            WhitePlayer.ActiveUnits = () => _activeUnitList.FindAll(unit => unit.UnitType == UnitType.White);
            BlackPlayer.ActiveUnits = () => _activeUnitList.FindAll(unit => unit.UnitType == UnitType.Black);

            var boardBoxes = _board.Boxes;

            var boardSize = item.Board.BoardSize.y;
            var countInRow = item.Board.BoardSize.x / 2;
            var rowCount = item.Rule.PawnInitialRowCount;
            var isBlackCorner = item.Rule.IsBlackCorners;
            //Create Checkers
            for (var i = 0; i < countInRow * rowCount; i++) //black
            {
                var unit = Instantiate(UnitPrefab, ((MonoBehaviour) _board).transform);
                var y = (i) / countInRow; //find the row
                var x = ((i) % countInRow) * 2 + (isBlackCorner ? (y % 2 == 0  ? 0 : 1): (y % 2 == 0 ? 1 : 0)); //find the column
                Debug.Log($"x:{x} y:{y}");
                var box = boardBoxes[x, y];

                unit.transform.position = box.transform.position;
                unit.UnitType = UnitType.Black;
                unit.Box = box;
                box.Unit = unit;
                unit.Init(new Unit.InitParams {UnitId = $"{x},{y}"});
                _activeUnitList.Add(unit);
            }

            for (var i = 0; i < countInRow * rowCount; i++)
            {
                var unit = Instantiate(UnitPrefab);
                var y = boardSize - 1 - ((i) / countInRow); //find the row
                var x = ((i) % countInRow) * 2 + (isBlackCorner ? (y % 2 == 0 ? 0 : 1) : (y % 2 == 0 ? 1 : 0)); //find the column
                var box = boardBoxes[x, y];
                unit.transform.position = box.transform.position;
                unit.UnitType = UnitType.White;
                unit.Box = box;
                box.Unit = unit;
                unit.Init(new Unit.InitParams {UnitId = $"{x},{y}"});
                _activeUnitList.Add(unit);
            }

            _allUnitList.AddRange(_activeUnitList);

            Initialized = true;
        }

        /// <summary>
        /// Call This method to Start the Game after Initialized.It will throw error if Not in correct TurnState
        /// </summary>
        public void StartTheGame()
        {
            if (!Initialized)
                throw new InvalidOperationException("This Should be Initialized before Call this Method.");

            if (CurrentState != GameHandlerState.None)
                throw new InvalidOperationException("Game is Already Started.");

            CurrentTurnState = TurnState.Playing;
            StartTurnToPlayer(BlackPlayer);
//            GameStarted = true;
            CurrentState = GameHandlerState.Playing;
        }

        public void StopTheGame()
        {
            CurrentState = GameHandlerState.Stopped;
        }

        private void StartTurnToPlayer(IPlayer player, Action completed = null)
        {
            _timer.ResetTimer();
            _currentOrLastActivePlayer = player;
            player.SetTurn(true, () =>
            {
                _timer.Completed += TimerOnCompleted;
                _timer.StartTimer();

                completed?.Invoke();
                TurnStart?.Invoke(player);
            });
        }

        private void TimerOnCompleted(object sender, EventArgs eventArgs)
        {
            OverTheGame(GetOther(CurrentPlayer), GameOverReason.TimeUp);
        }


        private void EndTurnToPlayer(IPlayer player, Action completed = null)
        {
            TurnEnd?.Invoke(player);
            player.SetTurn(false, completed);
        }

        /// <summary>
        /// Request the GameHandler to MoveInfo Or Cut the Unit.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="request"></param>
        /// <returns>The Details of move if ChallengeAccepted otherwise null.MoveInfo task should be Mark as Completed after moved</returns>
        // ReSharper disable once MethodTooLong
        public MoveTask RequestForMove(IPlayer player, MoveRequest request)
        {
            if (CurrentState != GameHandlerState.Playing || CurrentPlayer != player)
                return null;

            _moveCalculator.Board = _board.ToBoardData();


            var infos = _board.FromMoveInfoData(_moveCalculator.GetMoves(request.Unit.UnitType)).SelectMany(moveInfos => moveInfos)
                .Where(info => info.Unit == request.Unit).ToList();

            var requestedMoves = request.Moves.ToList();
            var moveMatched = infos.Any(info =>
            {
                var moves = info.Moves.ToList();

                for (var i = 0; i < moves.Count; i++)
                {
                    if (!Equals(moves[i], requestedMoves[i]))
                    {
                        return false;
                    }
                }

                return true;
            });
            IUnit missedCuttingUnit = null;
            if (moveMatched)
            {
                if (!requestedMoves.First().IsCut)
                {
                    var playerActiveUnits = player.ActiveUnits();
                    missedCuttingUnit = playerActiveUnits.FirstOrDefault(unit =>
                    {
                        var info = infos.FirstOrDefault();
                        return info.Moves != null && info.Moves.FirstOrDefault().IsCut;
                    });
                }
            }

            _timer.Completed -= TimerOnCompleted;
            _timer.StopTimer();

            return moveMatched
                ? new MoveTask(() =>
                {
                    requestedMoves.ForEach(move =>
                    {
                        if (move.DestroyableUnit != null)
                        {
                            move.DestroyableUnit.Box.Unit = null;
                            var unit = move.DestroyableUnit;
                            _activeUnitList.Remove(unit);
                            move.DestroyableUnit = null;
                        }
                    });
                    request.Unit.Box.Unit = null;
                    request.Unit.Box = requestedMoves.Last().EndBox;
                    request.Unit.Box.Unit = request.Unit;

                    // ReSharper disable once TooManyChainedReferences
                    var coordinate = request.Unit.Box.Coordinate;
                    if (coordinate.y == BoardSize.y - 1 && request.Unit.UnitType == UnitType.Black ||
                        coordinate.y == 0 && request.Unit.UnitType == UnitType.White)
                    {
                        request.Unit.SetCrown(() =>
                        {
                            CompleteThisPlayerTurnOrGameAndGoNext(player, missedCuttingUnit);
                        });
                    }
                    else
                    {
                        CompleteThisPlayerTurnOrGameAndGoNext(player, missedCuttingUnit);
                    }
                })
                {
                    Moves = requestedMoves
                }
                : null;
        }

        // ReSharper disable once MethodTooLong

        private void OverTheGame(IPlayer player, GameOverReason reason)
        {
            CurrentTurnState = TurnState.None;
            CurrentState = GameHandlerState.Over;
            GameOver?.Invoke(new GameHandlerGameOverResult
            {
                GameOverReason = reason,
                Winner = player
            });
        }

        private void CompleteThisPlayerTurnOrGameAndGoNext(IPlayer player, IUnit missCutUnit = null)
        {
            if (missCutUnit != null)
            {
                missCutUnit.Box.Unit = null;
                var box = missCutUnit.Box;
                missCutUnit.Box = null;

                var holder = _board.GetFreeHolder(missCutUnit.UnitType);
                AnimationManager.BasicMoveTowards(f =>
                    {
                        var lerp = Vector3.Lerp(box.transform.position, holder.transform.position, f);
                        lerp.z = Mathf.Min(lerp.z, Mathf.Lerp(0, -3, (0.5f - Mathf.Abs(f - 0.5f)) / 0.5f));
                        missCutUnit.transform.position = lerp;
                    }, () =>
                    {
                        _board.HoldTheUnitTo(missCutUnit, holder);
                        _activeUnitList.Remove(missCutUnit);
                        CompleteThisPlayerTurnOrGameAndGoNext(player);
                    }, 9 / (holder.transform.position - missCutUnit.transform.position).magnitude);
                return;
            }


            var nextPlayer = player == WhitePlayer ? BlackPlayer : WhitePlayer;

            _moveCalculator.Board = _board.ToBoardData();

            if (!_moveCalculator.GetMoves(nextPlayer.Type).Any())
            {
                OverTheGame(player, GameOverReason.End);
                return;
            }

            EndTurnToPlayer(player, () => { StartTurnToPlayer(nextPlayer); });
        }

        public IUnit GetUnitById(string id)
        {
            return _allUnitList.Find(unit => unit.Id == id);
        }

        private IPlayer GetOther(IPlayer player)
        {
            return Equals(player, WhitePlayer) ? BlackPlayer : WhitePlayer;
        }

        public struct InitParams
        {
            public ICheckerBoard Board { get; set; }
            public IPlayer WhitePlayer { get; set; }
            public IPlayer BlackPlayer { get; set; }
            public ITimer Timer { get; set; }
            public float TurnTime { get; set; }
            public Unit UnitPrefab { get; set; }
            public Rule Rule { get; set; }
        }
    }

    public enum GameHandlerState
    {
        None,
        Playing,
        Stopped,
        Over
    }

    public enum TurnState
    {
        None,
        Waiting,
        Playing
    }

    public interface IPlayer
    {
        event Action<IPlayer, MoveRequest> OnPlayerRequestAcceptedToMove;
        string Name { get; }
        string AvatarUrl { get; }
        UnitType Type { get; }
        UnitFinder ActiveUnits { get; set; }
        IGameHandler GameHandler { get; set; }
        ITurnAdapter TurnAdapter { get; set; }
        bool IsMyTurn { get; }
        void SetTurn(bool isMine, Action completed = null);
        bool RequestToMove(MoveRequest moveRequest);
    }

    public interface ITurnAdapter
    {
        void StartTurn(IPlayer player, Action completed);
        void EndTurn(IPlayer player, Action completed);
    }

    public delegate IEnumerable<IUnit> UnitFinder();
}