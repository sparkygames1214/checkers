﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;

namespace Model
{
    public partial class Player
    {
        private PlayerData _playerData;
        private PlayerStatics _playerStatics;
        public event Action<Player> Updated;
        public event Action<object, PlayerData> DataUpdated;
        public event Action<object, PlayerStatics> StaticsUpdated;

        public PlayerData PlayerData
        {
            get => _playerData;
            set
            {
                _playerData = value;
                DataUpdated?.Invoke(this, PlayerData);
                Updated?.Invoke(this);
            }
        }

        public PlayerStatics PlayerStatics
        {
            get => _playerStatics;
             set
            {
                _playerStatics = value;
                StaticsUpdated?.Invoke(this, PlayerStatics);
                Updated?.Invoke(this);
            }
        }


        public string Name { get; set; }
        public string Id { get; set; }
        public string AvatarUrl { get; set; }
    }

    public partial class Player
    {
        public int Coins => PlayerData.Coins;


        public  void ConsumeCoins(int coins, TaskAction<bool> completed = null)
        {
           
            if (PlayerData.Coins < coins)
            {
                completed?.Invoke(false);
            }
            else
            {
                var playerData = PlayerData;
                playerData.Coins -= coins;
                UpdatePlayerData(playerData, error =>
                {
                    completed?.Invoke(error!=null);
                });
            }
        }

        public  void AddCoins(int coins, TaskAction<bool> completed = null)
        {
            var playerData = PlayerData;
            playerData.Coins += coins;
            UpdatePlayerData(playerData, error =>
            {
                completed?.Invoke(error != null);
            });
        }
    }

    public partial class Player
    {
        public void UpdatePlayerDisplayName(string displayName, TaskAction<PlayFabError> completed = null)
        {
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest
            {
                DisplayName = displayName
            }, result =>
            {
                Name = result.DisplayName;
                completed?.Invoke(null);
            }, error => { completed?.Invoke(error); });
        }

        public void UpdateAvatarUrl(string url, TaskAction<PlayFabError> completed = null)
        {
            PlayFabClientAPI.UpdateAvatarUrl(new UpdateAvatarUrlRequest
            {
                ImageUrl = url
            }, result =>
            {
                AvatarUrl = url;
                completed?.Invoke(null);
            }, error => completed?.Invoke(error));
        }

        public void UpdatePlayerStats(PlayerStatics statics, Action<PlayFabError> completed = null)
        {
            PlayerStatics = statics;
            PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest
            {
                Statistics = statics.ToDict().Select(pair => new StatisticUpdate
                {
                    Value = pair.Value,
                    StatisticName = pair.Key
                }).ToList(),
            }, result =>
            {
                PlayerStatics = statics;
                completed?.Invoke(null);
            }, completed);
        }

        public void UpdatePlayerData(PlayerData data, Action<PlayFabError> completed = null)
        {
            PlayerData = data;
            PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string>(data.ToDict()),
            }, result =>
            {
                PlayerData = data;
                completed?.Invoke(null);
            }, completed);
        }

        // ReSharper disable once TooManyDeclarations
        public void GetPlayerData(TaskAction<PlayerData, PlayFabError> completed)
        {
            PlayFabClientAPI.GetUserData(new GetUserDataRequest
            {
                PlayFabId = Id,
            }, result =>
            {
                var data = result.Data;
                var dict = data.ConvertDict(pair => pair.Key, pair => pair.Value.Value);
                var playerData = PlayerData.FromDict(dict);
                PlayerData = playerData;
                completed?.Invoke(playerData, null);
            }, error => { });
        }

        // ReSharper disable once TooManyDeclarations
        public static void GetPlayerProfile(string userId, TaskAction<Player, PlayFabError> completed)
        {
            PlayFabClientAPI.GetPlayerCombinedInfo(new GetPlayerCombinedInfoRequest
            {
                PlayFabId = userId,
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                {
                    GetPlayerProfile = true, GetPlayerStatistics = true, GetUserData = true, ProfileConstraints =
                        new PlayerProfileViewConstraints
                        {
                            ShowAvatarUrl = true,
                            ShowDisplayName = true
                        }
                }
            }, result =>
            {
                var userData = result.InfoResultPayload.UserData;
                var userDict = userData.ConvertDict(pair => pair.Key, pair => pair.Value.Value);

                var player = new Player
                {
                    Id = userId,
                    PlayerData = PlayerData.FromDict(userDict),
                    AvatarUrl = result.InfoResultPayload.PlayerProfile.AvatarUrl,
                    Name = result.InfoResultPayload.PlayerProfile.DisplayName,
                    PlayerStatics = PlayerStatics.FromDict(
                        result.InfoResultPayload.PlayerStatistics.ToDictionary(value => value.StatisticName,
                            value => value.Value))
                };

                completed?.Invoke(player, null);
            }, error => { });
        }
    }


    public struct PlayerData
    {
        public int Coins { get; set; }

        public IDictionary<string, string> ToDict()
        {
            return new Dictionary<string, string>()
            {
                {"Coins", Coins.ToString()}
            };
        }

        public static PlayerData FromDict(IDictionary<string, string> dict)
        {
            var playerData = new PlayerData();

            if (dict.ContainsKey("Coins"))
            {
                playerData.Coins = int.Parse(dict["Coins"]);
            }

            return playerData;
        }
    }

    public struct PlayerStatics
    {
        public int Experience { get; set; }
        public int GamePlayed { get; set; }
        public int GameWon { get; set; }

        public IDictionary<string, int> ToDict()
        {
            return new Dictionary<string, int>()
            {
                {nameof(Experience), Experience},
                {nameof(GamePlayed), GamePlayed},
                {nameof(GameWon), GameWon},
            };
        }

        public static PlayerStatics FromDict(IDictionary<string, int> dict)
        {
            var playerStatics = new PlayerStatics();

            if (dict.ContainsKey(nameof(Experience)))
            {
                playerStatics.Experience = dict[nameof(Experience)];
            }
            if (dict.ContainsKey(nameof(Experience)))
            {
                playerStatics.GamePlayed = dict[nameof(GamePlayed)];
            }
            if (dict.ContainsKey(nameof(GameWon)))
            {
                playerStatics.GameWon = dict[nameof(GameWon)];
            }

            return playerStatics;
        }
    }
}