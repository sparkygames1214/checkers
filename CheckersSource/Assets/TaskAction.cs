﻿using System;

public class TaskAction:Cancellable
{
    private readonly Action _action;


    public bool IsCompleted { get;private set; }


    public TaskAction(Action action)
    {
        _action = action;
    }

    public void Invoke()
    {
        if(!IsCanceled)
        {
            if (_action != null) _action();
        }
        else
        {
            IsCompleted = true;
        }
    }
}

public class Cancellable
{
    public event Action Canceled;

    private bool _isCanceled;


    public bool IsCanceled
    {
        get { return _isCanceled; }
        set
        {
            if (value == _isCanceled)
                return;

            if (!value)
                throw new InvalidOperationException();

            _isCanceled = true;
            if (Canceled != null) Canceled();
        }
    }

}


public class TaskAction<T>:Cancellable
{
    private readonly Action<T> _action;

    public bool IsCompleted { get;private set; }
    
    public TaskAction(Action<T> action)
    {
        _action = action;
    }

    public void Invoke(T t)
    {
        if(!IsCanceled)
        {
            if (_action != null) _action(t);
            IsCompleted = true;
        }

    }
}

public class TaskAction<T,TJ>:Cancellable
{
    private readonly Action<T,TJ> _action;

    public bool IsCompleted { get; private set; }

    

    public TaskAction(Action<T,TJ> action)
    {
        _action = action;
    }

    public void Invoke(T t,TJ j)
    {
        if(!IsCanceled)
        {
            if (_action != null) _action(t, j);
            IsCompleted = true;
        }
    }
}



public class TaskAction<T, TJ , TK>:Cancellable
{
    private readonly Action<T, TJ,TK> _action;
    public bool IsCompleted { get; private set; }




    public TaskAction(Action<T, TJ,TK> action)
    {
        _action = action;
    }

    public void Invoke(T t, TJ j,TK k)
    {
        if(!IsCanceled)
        {
            if (_action != null) _action(t, j, k);
            IsCompleted = true;
        }
    }
}

