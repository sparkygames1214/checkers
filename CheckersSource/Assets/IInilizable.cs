﻿public interface IInilizable
{
    bool Initialized { get; }
    void Init();
}

public interface IInilizable<in T>
{
    bool Initialized { get; }
    void Init(T item);
}