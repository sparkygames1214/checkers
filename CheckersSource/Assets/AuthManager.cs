﻿using System;
using System.Collections.Generic;
using System.Linq;
//using Facebook.Unity;
using Model;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using LoginResult = PlayFab.ClientModels.LoginResult;
using Random = UnityEngine.Random;

// ReSharper disable once HollowTypeName
public partial class AuthManager : MonoBehaviour
{
	private const int INITIAL_COINS = 1000;

	private static Player _player;

	public static event Action LoggedIn;
	public static event Action LoggedOut;
	public static event Action ConnectedToFacebook;

	public static AuthManager Instance { get; private set; }
	public static string PlayerId { get; private set; }

	public static string TitleId => ResourceManager.PlayFabShardSettings.TitleId;

	public static Player Player
	{
		get => _player;
		private set
		{
			var lastPlayer = _player;
			_player = value;

			if (lastPlayer == null && Player != null)
			{
				LoggedIn?.Invoke();
			}
			else if (Player == null && lastPlayer != null)
				LoggedOut?.Invoke();
		}
	}

	public static LoginMethod CurrentOrLastLoginMethod
	{
		get { return (LoginMethod)PlayerPrefs.GetInt(nameof(CurrentOrLastLoginMethod), 0); }
		private set { PlayerPrefs.SetInt(nameof(CurrentOrLastLoginMethod), (int)value); }
	}

	//private static FacebookManager FacebookManager => FacebookManager.Instance;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}

	public static void LogInWithFacebook(TaskAction<Exception> completed = null)
	{
		//if (!FacebookManager.IsLogIn)
		//{
		//	FacebookManager.LogIn(token =>
		//	{
		//		if (token == null)
		//		{
		//			completed?.Invoke(new Exception("Some thing went wrong!"));
		//			return;
		//		}

		//		LogInWithFacebook(completed);
		//	});
		//	return;
		//}

		//LogInWithFacebook(FacebookManager.AccessToken, new TaskAction<PlayFabError>(error => completed?.Invoke(error != null ? new Exception(error.ErrorMessage) : null)));
	}

	//public static void LogInWithFacebook(AccessToken accessToken, TaskAction<PlayFabError> completed = null)
	//{
	//	PlayFabClientAPI.LoginWithFacebook(new LoginWithFacebookRequest
	//	{
	//		AccessToken = accessToken.TokenString,
	//		CreateAccount = true,
	//		TitleId = TitleId
	//	}, (result) =>
	//	{
	//		if (completed != null && completed.IsCanceled)
	//			return;

	//		RequestPhotonToken(result, (err) =>
	//		{
	//			if (err != null)
	//			{
	//				completed?.Invoke(err);
	//				return;
	//			}

	//			CurrentOrLastLoginMethod = LoginMethod.Facebook;
	//			var nameIfNull = FacebookManager.FullName;
	//			LoadPlayer(PlayerId, completed, result.NewlyCreated, recommendedUserNameIfNull: nameIfNull, avatarUrl: FacebookManager.BuildAvatarUrl(accessToken.UserId));
	//		});
	//	}, error =>
	//	{
	//		OnPlayFabError(error);
	//		completed?.Invoke(error);
	//	});
	//}

	// ReSharper disable once MethodTooLong
	public static void LogInAsGuest(TaskAction<PlayFabError> completed = null)
	{
		if (Application.internetReachability != NetworkReachability.NotReachable)
		{

			PlayFabClientAPI.LoginWithAndroidDeviceID(new LoginWithAndroidDeviceIDRequest
			{
				TitleId = TitleId,
				CreateAccount = true,
				AndroidDeviceId = SystemInfo.deviceUniqueIdentifier
			}, (result) =>
			{
				if (completed != null && completed.IsCanceled)
					return;
				RequestPhotonToken(result, (err) =>
				{
					if (err != null)
					{
						completed?.Invoke(err);
						return;
					}
					if (completed != null && completed.IsCanceled)
						return;

					CurrentOrLastLoginMethod = LoginMethod.Guest;
					LoadPlayer(PlayerId, completed, result.NewlyCreated, result.NewlyCreated ? $"Guest{Random.Range(0, 50000)}" : null, ResourceManager.SampleAvatarUrls.GetRandom());
				});
			}, error =>
			{
				OnPlayFabError(error);
				completed?.Invoke(error);
			});
		}

		else
		{
			CurrentOrLastLoginMethod = LoginMethod.Offline;

			LoadPlayer("Offline", completed, false, "Offline", "");
			completed.Invoke(null);
		}
	}

	public static void LogOut()
	{
		//if (FacebookManager.IsLogIn)
		//{
		//	FacebookManager.LogOut();
		//}
		Player = null;
		CurrentOrLastLoginMethod = LoginMethod.None;
	}

	private static void LoadPlayer(string playerId, TaskAction<PlayFabError> completed = null, bool newlyCreated = false, string recommendedUserNameIfNull = "Guest", string avatarUrl = "")
	{
		if (Application.internetReachability != NetworkReachability.NotReachable)
		{
			Player.GetPlayerProfile(playerId, new TaskAction<Player, PlayFabError>((account, error) =>
			{
				if (completed != null && completed.IsCanceled)
					return;

				if (newlyCreated)
				{
					account.UpdatePlayerData(new PlayerData
					{
						Coins = INITIAL_COINS
					}, fabError =>
					{
						if (fabError != null)
						{
							completed?.Invoke(fabError);
							return;
						}
						LoadPlayer(playerId, completed, false, recommendedUserNameIfNull, avatarUrl);
					});
					return;
				}


				if (string.IsNullOrEmpty(account.AvatarUrl) && !string.IsNullOrEmpty(avatarUrl))
				{
					account.UpdateAvatarUrl(avatarUrl, new TaskAction<PlayFabError>(fabError =>
					{
						if (fabError != null)
						{
							completed?.Invoke(fabError);
							return;
						}
						LoadPlayer(playerId, completed, false, recommendedUserNameIfNull);
					}));
					return;
				}

				if (string.IsNullOrEmpty(account.Name))
				{
					account.UpdatePlayerDisplayName(recommendedUserNameIfNull, new TaskAction<PlayFabError>(fabError => { LoadPlayer(playerId, completed); }));
					return;
				}

				Player = account;
				Debug.Log($"Player Name:{Player.Name} Email:{Player.AvatarUrl} Id:{Player.Id} Error:{error}");
				completed?.Invoke(error);
			}));
		}
		else
		{
			Player = new Player
			{
				AvatarUrl = "",
				Id = "Offline",
				Name = "Offline",
				PlayerData = new PlayerData { Coins = 1000 },
				PlayerStatics = new PlayerStatics
				{
					Experience = 0,
					GamePlayed = 0,
					GameWon = 0
				}
			};
		}
	}

	private static void RequestPhotonToken(LoginResult obj, Action<PlayFabError> completed = null)
	{
		//We can player PlayFabId. This will come in handy during next step
		PlayerId = obj.PlayFabId;

		PlayFabClientAPI.GetPhotonAuthenticationToken(new GetPhotonAuthenticationTokenRequest()
		{
			PhotonApplicationId = PhotonNetwork.PhotonServerSettings.AppID
		}, result =>
		{
			AuthenticateWithPhoton(result);
			completed?.Invoke(null);
		}, error =>
		{
			OnPlayFabError(error);
			completed?.Invoke(error);
		});
	}

	private static void OnPlayFabError(PlayFabError obj)
	{
	}

	private static void AuthenticateWithPhoton(GetPhotonAuthenticationTokenResult obj)
	{
		PhotonNetwork.playerName = PlayerId;
		//We set AuthType to custom, meaning we bring our own, PlayFab authentication procedure.
		var customAuth = new AuthenticationValues { AuthType = CustomAuthenticationType.Custom };
		//We add "username" parameter. Do not let it confuse you: PlayFab is expecting this parameter to contain player PlayFab ID (!) and not username.
		customAuth.AddAuthParameter("username", PlayerId); // expected by PlayFab custom auth service
														   //We add "token" parameter. PlayFab expects it to contain Photon Authentication Token issues to your during previous step.
		customAuth.AddAuthParameter("token", obj.PhotonCustomAuthenticationToken);
		customAuth.UserId = PlayerId;
		//We finally tell Photon to use this authentication parameters throughout the entire application.
		PhotonNetwork.AuthValues = customAuth;
	}


	public static void GetLeadersboard(TaskAction<Player[], PlayFabError> completed)
	{
		PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest
		{
			StatisticName = nameof(Player.PlayerStatics.Experience),
			StartPosition = 0,
			MaxResultsCount = 100,
			ProfileConstraints = new PlayerProfileViewConstraints
			{
				ShowAvatarUrl = true,
				ShowDisplayName = true,
				ShowStatistics = true
			}
		}, result =>
		{
			var players = result.Leaderboard.Select(entry => new Player
			{
				Id = entry.PlayFabId,
				Name = entry.DisplayName,
				AvatarUrl = entry.Profile.AvatarUrl,
				PlayerStatics = PlayerStatics.FromDict(entry.Profile.Statistics.ToDictionary(model => model.Name, model => model.Value))
			}).ToArray();
			completed?.Invoke(players, null);
		}, error =>
		{
			completed?.Invoke(null, error);
		});
	}

	public static void GetFiends(TaskAction<IEnumerable<PlayFab.ClientModels.FriendInfo>, PlayFabError> action)
	{
		PlayFabClientAPI.GetFriendsList(new GetFriendsListRequest
		{
			IncludeFacebookFriends = true,
			ProfileConstraints = new PlayerProfileViewConstraints
			{
				ShowAvatarUrl = true,
				ShowDisplayName = true,
				ShowLastLogin = true
			}
		}, result =>
		{
			action?.Invoke(result.Friends, null);
		}, error =>
		{
			action?.Invoke(null, error);
		});
	}

	//public static void ConnectToFacebook(TaskAction<PlayFabError> action)
	//{
	//	if (!FacebookManager.IsLogIn)
	//	{
	//		FacebookManager.LogIn(token => ConnectToFacebook(action));
	//		return;
	//	}

	//	PlayFabClientAPI.LinkFacebookAccount(new LinkFacebookAccountRequest
	//	{
	//		AccessToken = FacebookManager.AccessToken.TokenString
	//	}, result =>
	//	{
	//		Player.UpdateAvatarUrl(FacebookManager.AvatarUrl);
	//		Player.UpdatePlayerDisplayName(FacebookManager.FirstName);
	//		CurrentOrLastLoginMethod = LoginMethod.Facebook;
	//		ConnectedToFacebook?.Invoke();
	//		action?.Invoke(null);
	//	}, err =>
	//	{
	//		action?.Invoke(err);
	//	});
	//}

	public static void TryAutoLogIn(TaskAction<bool, Exception> action)
	{
		switch (CurrentOrLastLoginMethod)
		{
			case LoginMethod.None:
				action?.Invoke(false, null);
				break;
			case LoginMethod.Guest:
				LogInAsGuest(new TaskAction<PlayFabError>(error =>
				{
					action?.Invoke(error == null, error != null ? new Exception(error.ErrorMessage) : null);
				}));
				break;
			case LoginMethod.Facebook:
				//LogInWithFacebook(new TaskAction<Exception>(error =>
				//{
				//	action?.Invoke(error == null, error);
				//}));
				break;

		}
	}

	public enum LoginMethod
	{
		None, Offline, Guest, Facebook
	}
}