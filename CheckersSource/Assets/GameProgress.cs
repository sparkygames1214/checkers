﻿// /*
// Created by Darsan
// */

using System;

public class GameProgress
{
    private bool _isTerminated;
    public event Action<GameProgress> Terminated;

    public bool IsTerminated
    {
        get { return _isTerminated; }
        private set
        {

            if(_isTerminated == value)
                return;
            if (!value)
            {
                throw new Exception("Terminate Cannot be undo");
            }

            if (PhotonNetwork.room != null)
                PhotonNetwork.LeaveRoom();

            _isTerminated = true;
            Terminated?.Invoke(this);
        }
    }

    public bool Terminate()
    {
        if(IsTerminated)
            return false;

        IsTerminated = true;
        return true;
    }
}