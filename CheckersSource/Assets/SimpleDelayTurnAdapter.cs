﻿using System;
using UnityEngine;

namespace Game
{
    public class SimpleDelayTurnAdapter:ITurnAdapter
    {
      
        public Vector2 StartDelayLimits { get; set; }
        public Vector2 EndDelayLimits { get; set; }

        public void StartTurn(IPlayer player, Action completed)
        {
            var targetTime = Time.time + UnityEngine.Random.Range(StartDelayLimits.x,StartDelayLimits.y);
            LateCall.Create().Call(() => Time.time>targetTime,completed);
        }

        public void EndTurn(IPlayer player, Action completed)
        {
            var targetTime = Time.time + UnityEngine.Random.Range(EndDelayLimits.x, EndDelayLimits.y);
            LateCall.Create().Call(() => Time.time > targetTime, completed);
        }

        public SimpleDelayTurnAdapter(Vector2 startDelayLimits,Vector2 endDelayLimits)
        {
            StartDelayLimits = startDelayLimits;
            EndDelayLimits = endDelayLimits;
        }

    }
}