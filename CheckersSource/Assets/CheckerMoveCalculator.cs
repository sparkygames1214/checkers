﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace Game
{
    //Get Move Infos
    public class CheckerMoveCalculator
    {
        public VecInt BoxSize =>_board.BoardSize;
        private readonly ICheckerBoard _board;

        public CheckerMoveCalculator(ICheckerBoard board)
        {
            _board = board;
        }


        [NotNull]
        public IEnumerable<MoveInfo> GetMoveInfos(IUnit unit)
        {
            var direction = unit.Crowned
                ? MoveDirection.Both
                : (unit.UnitType == UnitType.Black 
                    ? MoveDirection.Positive 
                    : MoveDirection.Negative);


            var allMoves = GetAllMoves(unit.Box,false,direction,unit.UnitType.Other()).ToList();

            return allMoves.Select(moves => new MoveInfo
            {
                Moves = moves,
                Unit = unit
            });
        }

        public  IEnumerable<VecInt>[] GetCornerCoordinates(VecInt coordinate)
        {
            var coordinates = new IEnumerable<VecInt>[4];
            //TopRight
            coordinates[0] = coordinate.x >= BoxSize.x - 1 || coordinate.y >= BoxSize.y - 1
                ? null
                : new List<VecInt>(GetRange(Mathf.Min((BoxSize.x - 1 - coordinate.x), (BoxSize.y - 1 - coordinate.y)))
                    .Select(i => new VecInt(coordinate.x+ (i + 1), coordinate.y+ (i + 1))));

            //BottomRight
            coordinates[1] = coordinate.x >= BoxSize.x - 1 || coordinate.y <= 0
                ? null
                : new List<VecInt>(GetRange(Mathf.Min((BoxSize.x - 1 - coordinate.x), ( coordinate.y)))
                    .Select(i => new VecInt(coordinate.x + (i + 1), coordinate.y - (i + 1))));

            //BottomLeft
            coordinates[2] = coordinate.x <= 0 || coordinate.y <= 0
                ? null
                : new List<VecInt>(GetRange(Mathf.Min((coordinate.x), coordinate.y))
                    .Select(i => new VecInt(coordinate.x - (i + 1), coordinate.y - (i + 1))));

            //TopLeft
            coordinates[3] = coordinate.x <= 0 || coordinate.y >= BoxSize.y - 1
                ? null
                : new List<VecInt>(GetRange(Mathf.Min(coordinate.x, (BoxSize.y - 1 - coordinate.y)))
                    .Select(i => new VecInt(coordinate.x - (i + 1), coordinate.y + (i + 1))));


            return coordinates;
        }


        private  IEnumerable<int> GetRange(int count)
        {
            for (var i = 0; i < count; i++)
            {
                yield return i;
            }
        }

//        public bool CanGetCutOut(IUnit unit,VecInt moveToCoordinate)
//        {
//
//        }

        private  Move? GetCut(IBox startBox, [NotNull]IEnumerable<IBox> path, bool canSkipBox,
            UnitType targetType,IEnumerable<IUnit> ignoreUnits=null)
        {
            
            var boxes = path.ToList();
            if (boxes.Count <=1)
            {
                return null;
            }

            for (var index = 0; index < (canSkipBox?boxes.Count:1); index++)
            {
                var box = boxes[index];
                if (box.Unit != null)
                {
                    if (box.Unit.UnitType != targetType)
                    {
                        return null;
                    }

                    return index < boxes.Count - 1 
                           && (boxes[index + 1].Unit == null || (ignoreUnits != null && ignoreUnits.Any(unit => unit == boxes[index+1].Unit))) // next box is null
                                                   &&(index==0 || !box.Unit.Crowned) // cannot cut crow with skip box
                        ? (Move?)new Move
                        {
                            StartBox = startBox,
                            EndBox = boxes[index + 1],
                            DestroyableUnit = boxes[index].Unit
                        }
                        : null;
                }
            }

            return null;
        }

        private IEnumerable<IEnumerable<Move>> GetAllMoves(IBox startBox, bool canSkipBox, MoveDirection direction,UnitType targetUnitType)
        {
            var cuts = GetAllMoveCuts(startBox,canSkipBox,direction,targetUnitType).ToList();

            return cuts.Any()
                ? cuts
                : GetAllSimpleMoves(startBox, canSkipBox, direction).Select(move => (IEnumerable<Move>)new[] {move});
        }

        [NotNull]
        // ReSharper disable once MethodTooLong
        private IEnumerable<Move> GetAllSimpleMoves(IBox startBox, bool canSkipBox, MoveDirection direction)
        {
            var moves = new List<Move>();
            var coordinates = GetCornerCoordinates(startBox.Coordinate);
            var boxes = _board.Boxes;

            void AllSimpleMovesForPath(IEnumerable<VecInt> coordinatePath)
            {
                if (coordinatePath != null)
                {
                    var simpleMovesForPath = GetSimpleMovesForPath(startBox, coordinatePath.Select(i => boxes[i.x, i.y]), canSkipBox);
                    if (simpleMovesForPath != null) moves.AddRange(simpleMovesForPath);
                }
            }

            switch (direction)
            {
                case MoveDirection.Positive:
                    AllSimpleMovesForPath(coordinates[0]);
                    AllSimpleMovesForPath(coordinates[3]);
                    break;
                case MoveDirection.Negative:
                    AllSimpleMovesForPath(coordinates[1]);
                    AllSimpleMovesForPath(coordinates[2]);
                    break;
                case MoveDirection.Both:
                    AllSimpleMovesForPath(coordinates[0]);
                    AllSimpleMovesForPath(coordinates[1]);
                    AllSimpleMovesForPath(coordinates[2]);
                    AllSimpleMovesForPath(coordinates[3]);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("direction", direction, null);
            }

            return moves;
        }

        [NotNull]
        // ReSharper disable once MethodTooLong
        // ReSharper disable once ExcessiveIndentation
        // ReSharper disable once FlagArgument
        private IEnumerable<IEnumerable<Move>> GetAllMoveCuts(IBox startBox, bool canSkipBox,MoveDirection direction,UnitType targetType,List<Move> lastMoves = null)
        {
            var coordinates = GetCornerCoordinates(startBox.Coordinate);
            var list = new List<List<Move>>();
            var boxes = _board.Boxes;

            void AllMovesForPath(IEnumerable<VecInt> coordinatePath)
            {
                if (coordinatePath == null)
                {
                    return;
                }

                var moveNullable = GetCut(startBox, coordinatePath.Select(i => boxes[i.x, i.y]), canSkipBox && lastMoves == null, targetType, lastMoves?.Select(move => move.DestroyableUnit));
                if (moveNullable != null)
                {
                    var move = (Move) moveNullable;

                    var previousMoves = lastMoves == null ? new List<Move>() : new List<Move>(lastMoves);

                    if (previousMoves.All(m => m.DestroyableUnit != move.DestroyableUnit))
                    {
                        var currentMoves = new List<Move>(previousMoves) {move};
                        var cuts = GetAllMoveCuts(((Move) moveNullable).EndBox, false, direction, targetType, currentMoves);


                        foreach (var cut in cuts)
                        {
                            if (cut != null)
                            {
//                                var moves = new List<Move>(currentMoves);
//                                moves.AddRange(cut);
                                list.Add(cut.ToList());
                            }
                        }
                    }
                }
            }

            if (direction == MoveDirection.Positive)
            {
                AllMovesForPath(coordinates[0]);
                AllMovesForPath(coordinates[3]);
            }
            else if (direction == MoveDirection.Negative)
            {
                AllMovesForPath(coordinates[1]);
                AllMovesForPath(coordinates[2]);
            }
            else if (direction == MoveDirection.Both)
            {
                AllMovesForPath(coordinates[0]);
                AllMovesForPath(coordinates[1]);
                AllMovesForPath(coordinates[2]);
                AllMovesForPath(coordinates[3]);
            }

            return list.Count>0 || lastMoves==null? list.Select(moves => (IEnumerable<Move>)moves) : new List<IEnumerable<Move>>{lastMoves};
        }

        // ReSharper disable once TooManyDeclarations
        private  IEnumerable<Move> GetSimpleMovesForPath(IBox startBox,[NotNull]IEnumerable<IBox> path, bool canSkipBox)
        {
            var boxes = path.ToList();
            if (boxes.Count == 0)
            {
                return null;
            }

            if (!canSkipBox)
            {
                return boxes[0].Unit == null
                    ? new []{new Move {StartBox = startBox, EndBox = boxes[0]}}
                    : null;
            }

            for (var i = 0; i < boxes.Count; i++)
            {
                var box = boxes[i];
                if (box.Unit != null)
                {
                    return GetRange(i).Select(j => new Move
                    {
                        StartBox = startBox,
                        EndBox = boxes[j]
                    });
                }
            }

            return GetRange(boxes.Count).Select(j => new Move
            {
                StartBox = startBox,
                EndBox = boxes[j]

            });

        }
    }


    public abstract class MoveCalculator 
    {
        public ICheckerBoard Board { get; }
        public Rule Rule { get; }
        public VecInt BoxSize => Board.BoardSize;

        protected MoveCalculator(ICheckerBoard board,Rule rule)
        {
            Board = board;
            Rule = rule;
        }

        public abstract IEnumerable<MoveInfo[]> GetSimpleMoves(UnitType unit);
        public abstract IEnumerable<MoveInfo[]> GetAllCuts(UnitType unit);

        public IEnumerable<MoveInfo[]> GetMoves(UnitType unit)
        {
            var moves = GetAllCuts(unit).ToList();
            return moves.Count == 0 ? GetSimpleMoves(unit) : moves;
        }
    }


    public enum MoveDirection
    {
        Positive,Negative,Both
    }

    
}