﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoardSelectionPanel : ShowHidable
{
    [SerializeField]private List<BoardTileUI> _tiles = new List<BoardTileUI>();

    private BoardTileUI SelectedBoardTile
    {
        get { return _tiles.FirstOrDefault(ui => ui.Selected); }
        set
        {
            _tiles.Except(new []{value}).Foreach(ui => ui.Selected = false);
            value.Selected = true;
        }
    }

    private void Awake()
    {
        _tiles.ForEach(ui => ui.Clicked +=TileUIOnClicked);
    }

    private void TileUIOnClicked(BoardTileUI tile)
    {
        SelectedBoardTile = tile;
    }

    public override void Show(bool animate = true, Action completed = null)
    {
//        SelectedBoardTile = _tiles.FirstOrDefault(ui => ui.BoardSize == GameManager.BoardSize);
        base.Show(animate, completed);
    }

    public void OnClickOk()
    {
//        GameManager.BoardSize = SelectedBoardTile.BoardSize;
        Hide();
    }
}