﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ProgressPanel : ShowHidable
{

    [SerializeField] private Image _backgroundImg;

    private float _backgroundAlpha;

    public float BackgroundAlpha
    {
        get { return _backgroundAlpha; }
        set
        {
            var color = _backgroundImg.color;
            color.a = value;
            _backgroundImg.color = color;
            _backgroundAlpha = value;
        }
    }
}