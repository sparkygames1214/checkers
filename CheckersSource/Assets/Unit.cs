﻿using System;
using System.Linq;
using Game;
using UnityEngine;

namespace Game
{
    public class Unit : MonoBehaviour, IUnit,IInilizable<Unit.InitParams>
    {
        public event Action<IUnit> Clicked;

        [SerializeField] private GameObject[] _blackAndWhiteModels = new GameObject[2];
        [SerializeField] private GameObject[] _blackAndWhiteCrownModels= new GameObject[2];

        private IBox _box;

        public Vector2 Size
        {
            get => transform.localScale;
            set => transform.localScale = new Vector3(value.x, value.y, transform.localScale.z);
        }

        public UnitType UnitType
        {
            get => _blackAndWhiteModels.First().activeSelf ? UnitType.Black : UnitType.White;
            set
            {
                _blackAndWhiteModels[0].SetActive(value == UnitType.Black);
                _blackAndWhiteModels[1].SetActive(value == UnitType.White);
            }
        }

        public bool Crowned
        {
            get =>
                UnitType == UnitType.Black
                    ? _blackAndWhiteCrownModels[0].activeSelf
                    : _blackAndWhiteCrownModels[1].activeSelf;
            private set => _blackAndWhiteCrownModels[UnitType== UnitType.Black?0:1].SetActive(value);
        }

        public CheckerBoard CheckerBoard { get; set; }
        public bool Initialized { get; private set; }

        public string Id { get; set; }

        public IBox Box
        {
            get => _box;
            set
            {
                if(_box == value)
                    return;

                if (_box != null)
                {
                    _box.Clicked -=BoxOnClicked;
                }

                _box = value;
                if (_box != null)
                {
                    _box.Clicked += BoxOnClicked;
                }

            }
        }

        private void BoxOnClicked(IBox box)
        {
            Clicked?.Invoke(this);
        }


        /// <summary>
        /// Call this Function to Crown The Unit
        /// </summary>
        /// <param name="onComplete">Callback</param>
        public void SetCrown(Action onComplete = null)
        {
            Crowned = true;
            onComplete?.Invoke();
        }

        public void Init(InitParams item)
        {
            Id = item.UnitId;
            Initialized = true;
        }


        public struct InitParams
        {
            public string UnitId { get; set; }
        }
    }
}

public enum UnitType
{
    Black,
    White
}

public static class UnitTypeExtensions
{
    public static UnitType Other(this UnitType type) => type == UnitType.Black ? UnitType.White : UnitType.Black;

    public static MoveDirection Forward(this UnitType type) =>
        type == UnitType.Black ? MoveDirection.Positive : MoveDirection.Negative;

    public static MoveDirection Backward(this UnitType type) =>
        type == UnitType.Black ? MoveDirection.Negative: MoveDirection.Positive;
}