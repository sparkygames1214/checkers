﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Purchasing;

public class Purchaser : IStoreListener
{
    /// <summary>
    /// Item Purchase Completed with id and success state
    /// </summary>
    public event Action<string, bool> OnItemPurchased;

    public bool inilized
    {
        get { return _mStoreController != null && _mStoreExtensionProvider != null; }
    }

    public IEnumerable<string> ConsumableItems { get; private set; }
    public IEnumerable<string> NonConsumableItems { get; private set; }


    private static IStoreController _mStoreController;          // The Unity Purchasing system.
    private static IExtensionProvider _mStoreExtensionProvider; // The store-specific Purchasing subsystems.


    public Purchaser([NotNull]IEnumerable<string> consumableItems, [NotNull]IEnumerable<string> nonConsumableItems)
    {
        NonConsumableItems = nonConsumableItems;
        ConsumableItems = consumableItems;
        Init();
    }


    private void Init()
    {
        //TODO:UnComment After InApp Plugin added
        if (inilized)
        {
            return;
        }
        if (_mStoreController == null)
        {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            foreach (var premiumItem in ConsumableItems)
            {
                builder.AddProduct(premiumItem, ProductType.Consumable);
            }
            foreach (var nonConsumableItem in NonConsumableItems)
            {
                builder.AddProduct(nonConsumableItem, ProductType.NonConsumable);
            }
            UnityPurchasing.Initialize(this, builder);

        }
    }

    public string GetPrice(string productId)
    {
        var product = GetProduct(productId);
        return product != null ? product.metadata.localizedPriceString : null;
    }

    /// <summary>
    /// Buy the product with item
    /// </summary>
    /// <param name="productId">Product Id</param>
    /// <param name="callback">Buy product call back with success state</param>
    public void BuyProduct(string productId, Action<bool> callback)
    {
        Action<string, bool> onPurchase = null;
        onPurchase = (id, success) =>
        {
            if (id != productId)
                return;
            OnItemPurchased -= onPurchase;
            if (callback != null) callback(success);
        };
        OnItemPurchased += onPurchase;

        BuyProductID(productId);
    }


    /// <summary>
    /// The Item Already Brought for Non consumable Products
    /// </summary>
    /// <param name="productId">product id</param>
    /// <returns></returns>
    public bool ItemAlreadyPurchased(string productId)
    {
        var product = GetProduct(productId);
        if (product != null && product.definition.type != ProductType.Consumable && product.hasReceipt)
        {
            return true;
        }

        return false;
    }

    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (inilized)
        {
            Product product = _mStoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                _mStoreController.InitiatePurchase(product);
            }
            else
            {
                if (OnItemPurchased != null) OnItemPurchased(productId, false);
            }
        }
        else
        {
            if (OnItemPurchased != null) OnItemPurchased(productId, false);
            Init();
        }
    }


    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        _mStoreController = controller;
        _mStoreExtensionProvider = extensions;
    }

    public Product GetProduct(string id)
    {
        return _mStoreController != null ? _mStoreController.products.WithID(id) : null;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInilized Failed");
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log("Purchased Product:" + args.purchasedProduct.definition.id);
        if (OnItemPurchased != null) OnItemPurchased(args.purchasedProduct.definition.id, true);
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        if (OnItemPurchased != null) OnItemPurchased(product.definition.id, false);
    }
}