﻿using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CheckerBoard))]
// ReSharper disable once CheckNamespace
public class CheckerBoardCustomEditor : Editor{
    private CheckerBoard _checkerBoard;

    private void OnEnable()
    {
         _checkerBoard = (CheckerBoard)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Refresh"))
        {
            _checkerBoard.Refresh();
        }
    }
}
