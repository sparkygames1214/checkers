﻿using System;
using Model;
using UnityEditor;
using UnityEngine;

public static class EditorExtensions 
{

    private static readonly string GAME_ROOM_INFO_PATH =
        string.Format("Assets/Resources/{0}.asset", GameRoomGroupsInfoScriptable.DEFAULT_FILE_NAME);

    private static readonly string RULES_PATH =
        string.Format("Assets/Resources/{0}.asset", Rules.DEFAULT_FILE_NAME);
    private static readonly string COINS_SCRIPTABLE_PATH =
        string.Format("Assets/Resources/{0}.asset", CoinsItemsScriptable.DEFAULT_FILE_NAME);

    [MenuItem("MyGames/GameRoomInfos")]
    static void LoadOrCreateGameRoomScriptable()
    {

        Utils.CheckAndCreateDirectories(GAME_ROOM_INFO_PATH);
        var asset = AssetDatabase.LoadAssetAtPath<GameRoomGroupsInfoScriptable>(GAME_ROOM_INFO_PATH);

        if (asset == null)
        {
            asset = ScriptableObject.CreateInstance<GameRoomGroupsInfoScriptable>();
            AssetDatabase.CreateAsset(asset, GAME_ROOM_INFO_PATH);
            AssetDatabase.SaveAssets();
        }

        Selection.activeObject = asset;
        
    }

    [MenuItem("MyGames/Rules")]
    static void LoadOrCreateRules()
    {

        Utils.CheckAndCreateDirectories(RULES_PATH);
        var asset = AssetDatabase.LoadAssetAtPath<Rules>(RULES_PATH);

        if (asset == null)
        {
            asset = ScriptableObject.CreateInstance<Rules>();
            AssetDatabase.CreateAsset(asset, RULES_PATH);
            AssetDatabase.SaveAssets();
        }

        Selection.activeObject = asset;
        
    }

    [MenuItem("MyGames/Coins InApp")]
    static void LoadOrCreateCoinsItemScriptable()
    {

        Utils.CheckAndCreateDirectories(COINS_SCRIPTABLE_PATH);
        var asset = AssetDatabase.LoadAssetAtPath<CoinsItemsScriptable>(COINS_SCRIPTABLE_PATH);

        if (asset == null)
        {
            asset = ScriptableObject.CreateInstance<CoinsItemsScriptable>();
            AssetDatabase.CreateAsset(asset, COINS_SCRIPTABLE_PATH);
            AssetDatabase.SaveAssets();
        }

        Selection.activeObject = asset;

    }

    [MenuItem("MyGames/ClearPrefs")]
    static void ClearPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    public static Vector3 ToVector(this Axis axis)
    {
        switch (axis)
        {
            case Axis.X:
                return Vector3.right;
            case Axis.Y:
                return Vector3.up;
            case Axis.Z:
                return Vector3.forward;
            default:
                throw new ArgumentOutOfRangeException(nameof(axis), axis, null);
        }
    }

}
