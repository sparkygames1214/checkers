﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class UtilsEditorWindow : EditorWindow
{
    [SerializeField] private SpacingSetting _spacingSetting;
    [SerializeField] private AngleSpaceSetting _angleSpacingSetting;
    [SerializeField] private OrderSetting _orderSetting;
    [SerializeField] private TranslateSetting _translateSetting;
    [SerializeField] private AlignSetting _alignSetting;
    [SerializeField] private RenameSetting _renameSetting;
    [SerializeField] private VariantSetting _variantSetting;


    [MenuItem("MyGames/Utils")]
    public static void Init()
    {
        var window = EditorWindow.GetWindow<UtilsEditorWindow>();
        window.titleContent = new GUIContent("Utils");

        window.Show();
    }

    private void OnEnable()
    {
//        _spacingSetting = new SpacingSetting
//        {
//            axis = Axis.Z,
//            update = false,
//            increment = false
//        };
    }

    private void OnGUI()
    {
        DrawSpacing();
        DrawAngleSpacing();
        DrawOrder();
        DrawTranslate();
        DrawAlign();
        DrawRename();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

    }







    private void DrawRename()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Rename", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;

        _renameSetting.prefix = EditorGUILayout.TextField("Prefix", _renameSetting.prefix);
        _renameSetting.useSiblingIndex = EditorGUILayout.Toggle("Use Sibling Index", _renameSetting.useSiblingIndex);

        EditorGUI.BeginDisabledGroup(Selection.gameObjects.Length < 2);
        if (GUILayout.Button("Apply"))
        {
            ApplyRename();
        }
        EditorGUI.EndDisabledGroup();

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }

    private void ApplyRename()
    {
        var gameObjects = Selection.gameObjects;
        if (gameObjects == null || gameObjects.Length < 2)
            return;

        var objects = gameObjects.OrderBy(o => o.transform.GetSiblingIndex()).Select(o => o.transform).ToList();
        for (var i = 0; i < objects.Count; i++)
        {
            objects[i].name = _renameSetting.useSiblingIndex
                ? $"{_renameSetting.prefix}{objects[i].GetSiblingIndex()}"
                : $"{_renameSetting.prefix}{i}";
        }
    }

    private void DrawAlign()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Align", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;

        _alignSetting.axis=
            (Axis)EditorGUILayout.EnumPopup("Axis", _alignSetting.axis);

        _alignSetting.fixedObject = (Fixed) EditorGUILayout.EnumPopup("Fixed", _angleSpacingSetting.fixedPos);

        EditorGUI.BeginDisabledGroup(Selection.gameObjects.Length < 2);
        if (GUILayout.Button("Apply"))
        {
            ApplyAlign();
        }

        EditorGUI.EndDisabledGroup();

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }

    private void ApplyAlign()
    {
        var gameObjects = Selection.gameObjects;
        if (gameObjects == null || gameObjects.Length < 2)
            return;

        var transforms = gameObjects.OrderBy(go => Vector3.Dot(go.transform.position, _alignSetting.axis.ToVector()))
            .Select(o => o.transform).ToList();

        var target = _alignSetting.fixedObject== Fixed.First ? transforms.First() : transforms.Last();

        transforms.ForEach(transform =>
        {
            transform.position = target.position +
                                 _angleSpacingSetting.axis.ToVector() * Vector3.Dot(
                                     (transform.position - target.position), _angleSpacingSetting.axis.ToVector());
        });
    }

    private void DrawTranslate()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Translate", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;

        _translateSetting.type = (TranslateType) EditorGUILayout.EnumPopup("Type", _translateSetting.type);

        switch (_translateSetting.type)
        {
            case TranslateType.Position:
                _translateSetting.positionTranslateValue =
                    EditorGUILayout.FloatField("Move Value", _translateSetting.positionTranslateValue);
                _translateSetting.positionAxis =
                    (Axis) EditorGUILayout.EnumPopup("Axis", _translateSetting.positionAxis);
                break;
            case TranslateType.Angle:
                _translateSetting.angleTranslateValue =
                    EditorGUILayout.FloatField("Rotate Value", _translateSetting.angleTranslateValue);
                _translateSetting.angleAxis = (Axis) EditorGUILayout.EnumPopup("Axis", _translateSetting.angleAxis);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

//        _translateSetting.update = EditorGUILayout.Toggle("Update", _translateSetting.update);


        EditorGUI.BeginDisabledGroup(Selection.gameObjects.Length < 1);
        if (GUILayout.Button("Apply"))
        {
            ApplyTranslate();
        }

        EditorGUI.EndDisabledGroup();


        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }

    private void ApplyTranslate()
    {
        var gameObjects = Selection.gameObjects;
        if (gameObjects == null || gameObjects.Length < 1)
            return;

        foreach (var gameObject in gameObjects)
        {
            switch (_translateSetting.type)
            {
                case TranslateType.Position:
                    gameObject.transform.position += _translateSetting.positionTranslateValue *
                                                     _translateSetting.positionAxis.ToVector();
                    break;
                case TranslateType.Angle:
                    gameObject.transform.rotation *= Quaternion.AngleAxis(_translateSetting.angleTranslateValue,
                        _translateSetting.angleAxis.ToVector());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    private void DrawOrder()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Order", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;

        _orderSetting.axis = (Axis) EditorGUILayout.EnumPopup("Order", _orderSetting.axis);
        _orderSetting.rename = EditorGUILayout.Toggle("Rename", _orderSetting.rename);
        if (_orderSetting.rename)
        {
            EditorGUI.indentLevel++;
            _orderSetting.prefix = EditorGUILayout.TextField("Prefix", _orderSetting.prefix);
            EditorGUI.indentLevel--;
        }

        EditorGUI.BeginDisabledGroup(Selection.gameObjects.Length < 2);
        if (GUILayout.Button("Apply"))
        {
            ApplyOrder();
        }

        EditorGUI.EndDisabledGroup();

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }

    private void ApplyOrder()
    {
        var gameObjects = Selection.gameObjects;
        if (gameObjects == null || gameObjects.Length < 2)
            return;

        var transforms = gameObjects.OrderBy(go => Vector3.Dot(go.transform.position, _orderSetting.axis.ToVector()))
            .Select(o => o.transform).ToList();

        for (var index = 0; index < transforms.Count; index++)
        {
            var transform = transforms[index];
            if (_orderSetting.rename)
                transform.gameObject.name = $"{_orderSetting.prefix}{index}";
            transform.SetAsLastSibling();
        }
    }

    private void DrawSpacing()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Spacing", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        EditorGUI.BeginChangeCheck();

        _spacingSetting.axis = (Axis) EditorGUILayout.EnumPopup("Axis", _spacingSetting.axis);

        _spacingSetting.space = EditorGUILayout.FloatField("Base Space", _spacingSetting.space);
        _spacingSetting.fixedPos = (Fixed) EditorGUILayout.EnumPopup("Fixed", _spacingSetting.fixedPos);

        _spacingSetting.increment = EditorGUILayout.Toggle("Has Increment", _spacingSetting.increment);

        if (_spacingSetting.increment)
        {
            EditorGUI.indentLevel++;
            _spacingSetting.incrementValue =
                EditorGUILayout.FloatField("Increment Value", _spacingSetting.incrementValue);
            EditorGUI.indentLevel--;
        }

        _spacingSetting.update = EditorGUILayout.Toggle("Update", _spacingSetting.update);

        if (EditorGUI.EndChangeCheck() && _spacingSetting.update)
        {
            ApplySpacing();
        }

        if (!_spacingSetting.update)
        {
            EditorGUI.BeginDisabledGroup(Selection.gameObjects.Length < 2);
            if (GUILayout.Button("Apply"))
            {
                ApplySpacing();
            }

            EditorGUI.EndDisabledGroup();
        }

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }

    private void DrawAngleSpacing()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Angle Spacing", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        EditorGUI.BeginChangeCheck();

        _angleSpacingSetting.axis = (Axis) EditorGUILayout.EnumPopup("Axis", _angleSpacingSetting.axis);

        _angleSpacingSetting.spaceAngle = EditorGUILayout.FloatField("Base Space", _angleSpacingSetting.spaceAngle);
        _angleSpacingSetting.fixedPos = (Fixed) EditorGUILayout.EnumPopup("Fixed", _angleSpacingSetting.fixedPos);

        _angleSpacingSetting.increment = EditorGUILayout.Toggle("Has Increment", _angleSpacingSetting.increment);

        if (_angleSpacingSetting.increment)
        {
            EditorGUI.indentLevel++;
            _angleSpacingSetting.incrementValue =
                EditorGUILayout.FloatField("Increment Value", _angleSpacingSetting.incrementValue);
            EditorGUI.indentLevel--;
        }

        _angleSpacingSetting.update = EditorGUILayout.Toggle("Update", _angleSpacingSetting.update);

        if (EditorGUI.EndChangeCheck() && _angleSpacingSetting.update)
        {
            ApplyAngleSpacing();
        }

        if (!_angleSpacingSetting.update)
        {
            EditorGUI.BeginDisabledGroup(Selection.gameObjects.Length < 2);
            if (GUILayout.Button("Apply"))
            {
                ApplyAngleSpacing();
            }

            EditorGUI.EndDisabledGroup();
        }

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }

    private void ApplyAngleSpacing()
    {
        var gameObjects = Selection.gameObjects;
        if (gameObjects == null || gameObjects.Length < 2)
            return;
        var transforms = gameObjects
            .OrderBy(go => Vector3.Dot(go.transform.position, _angleSpacingSetting.axis.ToVector()))
            .Select(o => o.transform).ToList();
        var startAngle = _angleSpacingSetting.fixedPos == Fixed.First
            ? transforms.First().rotation
            : (transforms.Last().rotation) * Quaternion.AngleAxis(
                  -(transforms.Count - 1) * _angleSpacingSetting.spaceAngle - (_angleSpacingSetting.increment
                      ? _angleSpacingSetting.incrementValue
                        * GetTriAngularNumber(transforms.Count - 2)
                      : 0), _angleSpacingSetting.axis.ToVector());


        for (var i = 0; i < transforms.Count; i++)
        {
            var angle = i * _angleSpacingSetting.spaceAngle +
                        ((_angleSpacingSetting.increment && i > 1)
                            ? _angleSpacingSetting.incrementValue *
                              GetTriAngularNumber(i - 1)
                            : 0);
            transforms[i].rotation = startAngle * Quaternion.AngleAxis(angle, _angleSpacingSetting.axis.ToVector());
            Debug.Log($"Angle:{angle}");
        }
    }

    private void ApplySpacing()
    {
        var gameObjects = Selection.gameObjects;
        if (gameObjects == null || gameObjects.Length < 2)
            return;
        var transforms = gameObjects.OrderBy(go => Vector3.Dot(go.transform.position, _spacingSetting.axis.ToVector()))
            .Select(o => o.transform).ToList();
        var startPos = _spacingSetting.fixedPos == Fixed.First
            ? transforms.First().position
            : (transforms.Last().position - _spacingSetting.axis.ToVector() *
               (_spacingSetting.space * (transforms.Count - 1)
                + (_spacingSetting.increment
                    ? (_spacingSetting.incrementValue * GetTriAngularNumber(transforms.Count - 2))
                    : 0)));


        for (var i = 0; i < transforms.Count; i++)
        {
            transforms[i].position = startPos + (i * (_spacingSetting.space) +
                                                 ((_spacingSetting.increment && i > 1)
                                                     ? _spacingSetting.incrementValue * GetTriAngularNumber(i - 1)
                                                     : 0))
                                     * _spacingSetting.axis.ToVector();
        }

        
    }

    private static int GetTriAngularNumber(int index)
    {
        return index * (index + 1) / 2;
    }

    [System.Serializable]
    private struct SpacingSetting
    {
        public float space;
        public bool increment;
        public float incrementValue;
        public Axis axis;
        public bool update;
        public Fixed fixedPos;
    }

    [System.Serializable]
    private struct AngleSpaceSetting
    {
        public float spaceAngle;
        public bool increment;
        public float incrementValue;
        public Axis axis;
        public bool update;
        public Fixed fixedPos;
    }

    [System.Serializable]
    private struct OrderSetting
    {
        public Axis axis;
        public bool rename;
        public string prefix;
    }

    [System.Serializable]
    public struct RenameSetting
    {
        public string prefix;
        public bool useSiblingIndex;
    }

    [System.Serializable]
    private struct TranslateSetting
    {
        public TranslateType type;
        public float angleTranslateValue;
        public Axis angleAxis;
        public float positionTranslateValue;
        public Axis positionAxis;
        public bool update;
    }

    [System.Serializable]
    public struct AlignSetting
    {
        public Axis axis;
        public Fixed fixedObject;
    }

    [System.Serializable]
    public struct VariantSetting
    {
        public VariantType variantType;
    }

    public enum VariantType
    {
        AllOther, Orange, Green, LightBlue, Pink
    }

    public enum TranslateType
    {
        Position,
        Angle
    }

    public enum Fixed
    {
        First,
        Last
    }
}

public enum Axis
{
    X,
    Y,
    Z
}
//
//public static class EditorExtensions
//{
//    
//}