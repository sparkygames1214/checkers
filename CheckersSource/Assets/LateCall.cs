﻿using System;
using System.Collections;
using UnityEngine;

public class LateCall : MonoBehaviour
{
    public delegate bool Condition();

    public void Call(Condition condition, Action onFinished)
    {
        StartCoroutine(CallAfterConditionCor(condition, () =>
        {
            if (onFinished != null) onFinished();
            Destroy(gameObject);
        }));
    }

    IEnumerator CallAfterConditionCor(Condition condition, Action onFinished)
    {
        if (condition == null)
            yield break;
        while (!condition())
        {
            yield return null;
        }

        if (onFinished != null) onFinished();
    }
    public static LateCall Create()
    {
        var go = new GameObject("LateCall");
        return go.AddComponent<LateCall>();
    }
}