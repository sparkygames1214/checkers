﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    public struct MoveInfo
    {
        public IUnit Unit { get; set; }
        public IEnumerable<Move> Moves { get; set; }
    }

    public struct Move
    {
        public IBox StartBox { get; set; }
        public IBox EndBox { get; set; }
        public IUnit DestroyableUnit { get; set; }
        public bool IsCut => DestroyableUnit != null;

        public override bool Equals(object obj)
        {
            if (obj is Move move)
            {
                return move.DestroyableUnit == DestroyableUnit && move.StartBox == StartBox &&
                       move.EndBox == EndBox;
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }


    public static class MoveUtils
    {
        public static IEnumerable<IBox> GetMiddleBoxes(Move move,ICheckerBoard checkerBoard)
        {
            var checkerBoardBoxes = checkerBoard.Boxes;
            var vecInt = move.EndBox.Coordinate - move.StartBox.Coordinate;
            var middleBoxCount = Mathf.Abs(vecInt.x) - 1;
            for (int i = 0; i < middleBoxCount; i++)
            {
                var coordinate = move.StartBox.Coordinate;
                var box = checkerBoardBoxes[coordinate.x + (vecInt.x > 0 ? 1 : -1) * (i + 1),
                    coordinate.y + (vecInt.y > 0 ? 1 : -1) * (i + 1)];

                yield return box;
            }
        }
        
        public static string GetEncodedMoveRequest(MoveRequest request)
        {
            var requestEncode = new MoveRequestEncode
            {
                unitId = request.Unit.Id,
                moves = request.Moves.Select(move => new MoveEncode
                {
                    startCoordinate = move.StartBox.Coordinate,
                    endCoordinate = move.EndBox.Coordinate,
                    destroyUnitId = move.DestroyableUnit?.Id
                    // ReSharper disable once TooManyChainedReferences
                }).ToList()
            };

            return JsonUtility.ToJson(requestEncode);
        }


        public static MoveRequest GetMoveRequestFromEncodedString(string codeString,IGameHandler gameHandler,ICheckerBoard checkerBoard)
        {
            var moveRequestEncode = JsonUtility.FromJson<MoveRequestEncode>(codeString);
            var boxes = checkerBoard.Boxes;
            return new MoveRequest
            {
                Moves = moveRequestEncode.moves?.Select(encode => new Move{StartBox = boxes[encode.startCoordinate.x,encode.startCoordinate.y],EndBox = boxes[encode.endCoordinate.x,encode.endCoordinate.y]
                    ,DestroyableUnit = gameHandler.GetUnitById(encode.destroyUnitId)}),
                Unit = gameHandler.GetUnitById(moveRequestEncode.unitId)
            };
            
        }

        [System.Serializable]
        private class MoveRequestEncode
        {
            public string unitId;
            public List<MoveEncode> moves;
        }

        [System.Serializable]
        private class MoveEncode
        {
            public VecInt startCoordinate;
            public VecInt endCoordinate;

            public string destroyUnitId; //this is for just validation purpose
        }
    }


   

}