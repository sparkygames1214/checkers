﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    public interface IProgressBar
    {
        bool Active { get; set; }
        float Value { get; set; }
		int Kill { set; }
		UnitType UnitType { get; set; }
    }

    public class ProgressBar : MonoBehaviour, IProgressBar
    {
        [SerializeField]private List<GameObject> _bars = new List<GameObject>();
        [SerializeField]private Sprite[] _barBlackAndWhiteSprites = new Sprite[2];

        private float _value;
		private int _kill;
		private bool _active;
        private UnitType _unitType;

        public bool Active
        {
            get { return _active; }
            set
            {
                _active = value;
                Value = 0;
            }
        }

        public float Value
        {
            get { return _value; }
            set
            {
                _value = value;
                var activeBar = Mathf.RoundToInt(_bars.Count * value);
                for (var i = 0; i < _bars.Count; i++)
                {
                    _bars[i].SetActive(activeBar>i);
                }
            }
        }

        public UnitType UnitType
        {
            get { return _unitType; }
            set
            {
                _unitType = value;
                foreach (var sp in _bars.Select(b =>b.GetComponent<SpriteRenderer>()))
                {
                    sp.sprite = _barBlackAndWhiteSprites[value == UnitType.Black ? 0 : 1];
                }
            }
        }

		public int Kill
		{
			set
			{
				_kill = value;
			}
		}
	}
}