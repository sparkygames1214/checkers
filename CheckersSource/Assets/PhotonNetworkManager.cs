﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

// ReSharper disable once HollowTypeName
public class PhotonNetworkManager : Photon.PunBehaviour
{
    public static PhotonNetworkManager Instance { get; private set; }

    public static event Action JoinedLobby;
    public static event Action ConnectedToPhoton;
    public static event Action DisconnectedFromPhoton;
    public static event Action LeftLobby;

    private const string GAME_VERSION = "1.0";

    public static bool Connected => PhotonNetwork.connected;

    public static bool Initialized { get; private set; }
    public static PhotonPlayer Player => PhotonNetwork.player;

    public static IEnumerable<RoomInfo> Rooms => PhotonNetwork.GetRoomList();

    private static TaskAction<Exception> _connectOrFailedCallback;
    private static TaskAction<Exception> _joinedLobbyOrFailedCallback;
    private static TaskAction<IEnumerable<FriendInfo>, Exception> _findFriendsCallback;

    public static IEnumerable<FriendInfo> Friends
    {
        get { return PhotonNetwork.Friends ?? new List<FriendInfo>(); }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }


    }

    private void OnEnable()
    {
        if (AuthManager.Player != null)
        {
            CheckerPlayFabManagerOnLoggedIn();
        }
        AuthManager.LoggedIn += CheckerPlayFabManagerOnLoggedIn;
        AuthManager.LoggedOut += CheckerPlayFabManagerOnLogOut;
    }

    private void CheckerPlayFabManagerOnLogOut()
    {
        Disconnect();
    }


    private void OnDisable()
    {
        AuthManager.LoggedIn -= CheckerPlayFabManagerOnLoggedIn;
        AuthManager.LoggedOut -= CheckerPlayFabManagerOnLogOut;

    }


    private void CheckerPlayFabManagerOnLoggedIn()
    {
        Invoke(nameof(InitLater), 2f);
    }

    private void InitLater()
    {
        Init();
    }

    public static void Init()
    {
        if (Initialized)
            return;
        Connect();
        Initialized = true;
    }


    public static void Connect(TaskAction<Exception> connectionCompleteOrFailed = null, TaskAction<Exception> joinedLobbyOrFailed = null)
    {
        PhotonNetwork.playerName = AuthManager.Player.Name;
        if (!PhotonNetwork.connected)
        {
            PhotonNetwork.ConnectUsingSettings(GAME_VERSION);
        }
    }

    public override void OnConnectedToPhoton()
    {
        base.OnConnectedToPhoton();

        
        Debug.Log(nameof(OnConnectedToPhoton) + PhotonNetwork.player);
        
        PhotonNetwork.player.SetPlayFabId(AuthManager.Player.Id);
        PhotonNetwork.player.SetAvatarUrl(AuthManager.Player.AvatarUrl);

        _connectOrFailedCallback?.Invoke(null);
        _connectOrFailedCallback = null;


        ConnectedToPhoton?.Invoke();
    }


    public void GetFriendList([NotNull]IEnumerable<string> friendPlayFabIds, TaskAction<IEnumerable<FriendInfo>, Exception> completed)
    {
        _findFriendsCallback = completed;
        PhotonNetwork.FindFriends(friendPlayFabIds.ToArray());
    }

    public override void OnUpdatedFriendList()
    {
        base.OnUpdatedFriendList();
        _findFriendsCallback?.Invoke(PhotonNetwork.Friends, null);
        _findFriendsCallback = null;
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        base.OnConnectionFail(cause);

        _connectOrFailedCallback?.Invoke(new Exception(cause.ToString()));
        _connectOrFailedCallback = null;
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();

        _joinedLobbyOrFailedCallback?.Invoke(null);
        _joinedLobbyOrFailedCallback = null;

        JoinedLobby?.Invoke();
    }

    public override void OnLeftLobby()
    {
        base.OnLeftLobby();
        LeftLobby?.Invoke();
    }

    public override void OnDisconnectedFromPhoton()
    {
        base.OnDisconnectedFromPhoton();
        DisconnectedFromPhoton?.Invoke();
    }

    public static void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }

    public static IEnumerable<RoomInfo> GetAllRoomInfos()
    {
        return PhotonNetwork.GetRoomList();
    }
}