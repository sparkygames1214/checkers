﻿using System;
using UnityEngine;

namespace Model
{
    public class FriendInfo
    {
        public string Id { get; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool Challenged { get; set; }
        public bool Online { get; set; }
        public DateTime? LastSeen { get; set; }

        public FriendInfo(string id)
        {
            Id = id;
        }
    }
}

// ReSharper disable once HollowTypeName

public class Singleton<T>:MonoBehaviour where T:Singleton<T>
{
    public static T Instance { get; protected set; }

    protected virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = (T)this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
}

public class AutoCreateSingleton<T> : Singleton<T> where T:Singleton<T>
{
    public new static T Instance
    {
        get
        {
            var instance = Singleton<T>.Instance;
            if (instance==null)
            {
                var go = new GameObject(typeof(T).FullName);
               instance = go.AddComponent<T>();
            }
            return instance;
        }
        protected set { Singleton<T>.Instance = value; }
    }
}