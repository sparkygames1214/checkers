﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DailyRewardTileUI : MonoBehaviour,IPointerClickHandler
{
    public event Action<DailyRewardTileUI> Clicked;

    [SerializeField] private TMPro.TMP_Text _priceTxt;
    [SerializeField] private TMPro.TMP_Text _dayTxt;
    [SerializeField] private GameObject _selectedEffect;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private GameObject _lockEffect;
    private DailyRewardPrice _dailyRewardPrice;
    private int _day;
    private bool _locked;
    private bool _collected;
    private bool _selected;


    public DailyRewardPrice DailyRewardPrice
    {
        get => _dailyRewardPrice;
        set
        {
            _priceTxt.text = value.coins.ToString();
            _dailyRewardPrice = value;
        }
    }

    public int Day
    {
        get => _day;
        set
        {
            _dayTxt.text = $"DAY {value}";
            _day = value;
        }
    }

    public bool Locked
    {
        get => _locked;
        set
        {
            _lockEffect.SetActive(value);
            _locked = value;
        }
    }

    public bool Collected
    {
        get => _collected;
        set
        {
            _canvasGroup.alpha = value ? 0.5f : 1f;
            _collected = value;
        }
    }

    public bool Selected
    {
        get => _selected;
        set
        {
            _selectedEffect.SetActive(value);
            _selected = value;
        }
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        Clicked?.Invoke(this);    
    }
}

[Serializable]
public struct DailyRewardPrice
{
    public int coins;
}