﻿// /*
// Created by Darsan
// */

using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RuleTileUI : MonoBehaviour, IPointerClickHandler
{
    public event Action<RuleTileUI> Clicked;

    [SerializeField] private GameObject _selectedEffect;
    [SerializeField] private TMPro.TMP_Text _nameTxt;

    private Rule _rule;


    public bool Selected
    {
        get => _selectedEffect.activeSelf;
        set => _selectedEffect.SetActive(value);
    }

    public Rule Rule
    {
        get => _rule;
        set
        {
            _rule = value;
            _nameTxt.text = value.Name;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Clicked?.Invoke(this);
    }
}