﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using Model;
using UnityEngine;

namespace Game
{
    public class AIPlayer : BasePlayer
    {
        public Vector2 DelayMoveTimeBound { get; set; }

        protected override void TurnStarted()
        {
            Invoke(nameof(MakeMove), Random.Range(DelayMoveTimeBound.x, DelayMoveTimeBound.y));
        }


        // ReSharper disable once MethodTooLong
        private void MakeMove()
        {
            var boardData = checkBoard.ToBoardData();
            MoveCalculator.Board = boardData;
            var activeUnitMoves = checkBoard.FromMoveInfoData(MoveCalculator.GetMoves(Type)).ToList();
            var bestMove = activeUnitMoves.FirstOrDefault(infos =>
            {
                var moveInfos = infos.ToList();
                return moveInfos.Count > 0 && ((moveInfos.First().Moves.FirstOrDefault()).DestroyableUnit != null);
            });

            if (bestMove != null)
            {
                var infos = bestMove.ToList();
                if (infos.Any())
                {
                    ApplyMove(infos.First());
                    return;
                }
            }

            var allCuts = MoveCalculator.GetAllCuts(Type.Other()).SelectMany(datas => datas).ToList();

            if (allCuts.Any())
            {
                Debug.Log("Get Simple Move For Target Box");
                var moveData = allCuts.First().Moves.First();
                var list = MoveCalculator.GetSimpleMoveForTargetBox(moveData.EndBox, Type).SelectMany(datas => datas)
                    .ToList();
                if (list.Any())
                {
                    Debug.Log("Simple Move For Target Box");
                    ApplyMove(checkBoard.FromMoveInfoData(list.First()));
                    return;
                }

                var infoDatas = MoveCalculator.GetSimpleMoves(Type).SelectMany(datas => datas)
                    .Where(data => data.Unit.Coordinate == moveData.DestroyableUnit.Coordinate).ToList();
                if (infoDatas.Any())
                {
                    ApplyMove(checkBoard.FromMoveInfoData(infoDatas.First()));
                    return;
                }
            }


            activeUnitMoves.Randomize();

            var cutsDatas = activeUnitMoves.Select(infos => infos.First()).Select(info =>
            {
                boardData.ClearModifications();
                boardData.ApplyModification(info.Moves.Select(move => move.ToData(boardData)).ToList());
                return
                    new
                    {
                       OpponentCuts = MoveCalculator.GetAllCuts(Type.Other()).SelectMany(datas => datas).FirstOrDefault(),
                       MoveInfo = info,
                       MyCuts = MoveCalculator.GetAllCuts(Type).SelectMany(datas => datas).ToList()
                    };
            }).ToList();

            var nonCutsMove = cutsDatas.Where(arg => arg.OpponentCuts == null).OrderByDescending(arg => arg.MyCuts.Count).ThenByDescending(
                arg =>
                {
                    var disFromBase = Mathf.Abs(arg.MoveInfo.Unit.Box.Coordinate.y - (Type.Forward() == MoveDirection.Positive?0:checkBoard.BoardSize.y));
                    return arg.MoveInfo.Unit.Crowned ? -1 : disFromBase;
                }).ToList();
            var cutMoves = cutsDatas.Where(arg => arg.OpponentCuts != null).OrderBy(arg => arg.OpponentCuts.Moves.Count()).ToList();

            if (nonCutsMove.Count > 3)
            {
                Debug.Log("Non cuts Above 3");
                ApplyMove(nonCutsMove.First().MoveInfo);
                return;
            }

            if (nonCutsMove.Count > 0 && cutMoves.Count > 0)
            {
                Debug.Log("Non cuts And Cuts Grater than 0");

                var singleCuts = cutMoves.Where(arg => arg.OpponentCuts.Moves.Count()==1).Select(arg =>
                {
                    boardData.ClearModifications();
                    boardData.ApplyModification(arg.MoveInfo.Moves.Select(move =>move.ToData(boardData)).ToList());
                    boardData.ApplyModification(arg.OpponentCuts.Moves);
                    var list = MoveCalculator.GetAllCuts(Type).SelectMany(datas => datas).ToList();
                   return new
                    {
                        arg.MoveInfo,
                        Cuts = list.Count
                    };
                }).ToList();

                var cutWithCuts = singleCuts.Where(arg => arg.Cuts>0).OrderBy(arg => arg.Cuts).ToList();
                Debug.Log("Cut With Cuts:"+cutWithCuts.Count);
                if (cutWithCuts.Count > 0 && Random.value>0.5f)
                {
                    ApplyMove(cutWithCuts.First().MoveInfo);
                    Debug.Log("Cut With Cut Selected");
                    return;
                }
                Debug.Log("Non Cut With Cut Selected");

                ApplyMove(nonCutsMove.First().MoveInfo);
                return;

            }

            if (cutMoves.Count > 0)
            {
                ApplyMove(cutMoves.First().MoveInfo);
                return;
            }


            foreach (var moveInfos in activeUnitMoves)
            {
                var infos = moveInfos.ToList();
                if (infos.Any())
                {
                    ApplyMove(infos.First());
                    return;
                }
            }
        }

        private void ApplyMove(MoveInfo infos)
        {
            var response = RequestForMove(new MoveRequest()
            {
                Unit = infos.Unit,
                Moves = (infos.Moves)
            });
            if (response != null)
            {
                MakeMoves(infos.Unit, response.Moves, completed: () => { response.MarkAsComplete(); });
            }
        }

        public override bool RequestToMove(MoveRequest moveRequest)
        {
            return false;
        }
    }
}