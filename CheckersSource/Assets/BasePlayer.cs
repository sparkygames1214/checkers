﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace Game
{
    public abstract class BasePlayer : MonoBehaviour, IPlayer, IInilizable<BasePlayer.InitParams>
    {
        public event Action<IPlayer, MoveRequest> OnPlayerRequestAcceptedToMove;

        public string Name { get; protected set; }
        public string AvatarUrl { get; protected set; }
		public int Kills
		{
			get
			{
				return _kills;
			}
			protected set
			{
				_kills = value;
			}
		}

        public Model.MoveCalculator MoveCalculator { get;protected set; }
		public UnitType Type { get; protected set; }
        public UnitFinder ActiveUnits { get; set; }
        public IGameHandler GameHandler { get; set; }
        public virtual bool IsMyTurn { get; protected set; }
        public bool MovedInThisTurn { get; set; }
        public bool Initialized { get; private set; }
        public ITurnAdapter TurnAdapter { get; set; }

        protected ICheckerBoard checkBoard;
        private IProgressBar _timerUI;
		private int _kills = 0;

        public IProgressBar TimerUI
        {
            get { return _timerUI; }
            set
            {
                if(value!=null)
                value.UnitType = Type;
                _timerUI = value;
            }
        }

        public void Init(InitParams item)
        {
            if (Initialized)
            {
                return;
            }

            if (item.TurnAdapter == null || item.CheckerBoard == null)
            {
                throw new Exception("Compensatory Objects are missing!.");
            }

            TurnAdapter = item.TurnAdapter;
            Type = item.UnitType;
            checkBoard = item.CheckerBoard;
            Name = item.Name;
            AvatarUrl = item.AvatarUrl;
            MoveCalculator = new Model.CornerMoveCalculator(GameManager.SelectedRule);
            OnInit();
            Initialized = true;
        }

        protected virtual void OnInit()
        {
            
        }

        public void SetTurn(bool isMine, Action completed = null)
        {
            IsMyTurn = isMine;
            if (isMine)
            {
                if (TimerUI != null)
                    TimerUI.Active = true;
                TurnAdapter.StartTurn(this, () =>
                {
                    TurnStarted();
                    completed?.Invoke();
                });
            }
            else
            {
                if (TimerUI != null)
                    TimerUI.Active = false;
                TurnAdapter.EndTurn(this, () =>
                {
                    TurnEnded();
                    completed?.Invoke();
                });
            }
        }

        protected virtual void TurnStarted()
        {
        }

        protected virtual void TurnEnded()
        {
        }

        protected virtual void Update()
        {
            if (TimerUI == null || !TimerUI.Active || GameHandler.TurnElapsedTime < 0)
                return;
           
            
            TimerUI.Value = GameHandler.TurnElapsedTime / GameHandler.TurnTime;
        }

        protected MoveTask RequestForMove(MoveRequest request)
        {
            var requestForMove = GameHandler?.RequestForMove(this, request);
            if (requestForMove != null)
            {
                OnPlayerRequestAcceptedToMove?.Invoke(this, request);
            }

            return requestForMove;
        }

        protected void MakeMoves(IUnit unit, IEnumerable<Move> moves, Action completed = null)
        {
            StartCoroutine(MakeMovesEnumerator(unit, moves, completed));
        }

        // ReSharper disable once MethodTooLong
        private IEnumerator MakeMovesEnumerator(IUnit unit, IEnumerable<Move> moves, Action onComplete = null)
        {
            var list = moves.ToList();
            var speed = 5f;

            for (var i = 0; i < list.Count; i++)
            {
                var move = list[i];
                var m = move;


                yield return AnimationManager.BasicMoveTowards(f =>
                    {
                        var targetPos = Vector3.Lerp(m.StartBox.transform.position, m.EndBox.transform.position, f);
                        targetPos.z -= Mathf.Lerp(0, 1f, 0.5f - Mathf.Abs(0.5f - f));
                        unit.transform.position = targetPos;
                    }, speed: speed / (m.StartBox.transform.position - m.EndBox.transform.position).magnitude);

                if(i<list.Count-1)
                yield return new WaitForSeconds(0.2f);
            }


            var destroyMoveList = new List<Move>();
            foreach (var move in list)
            {
                if (move.DestroyableUnit != null)
                {
                    yield return new WaitForSeconds(0.2f);
                    var holder =
                        checkBoard.GetFreeHolder(Type == UnitType.White ? UnitType.Black : UnitType.White);
                    AnimationManager.BasicMoveTowards(nor =>
                        {
                            var cuttedTargetPos = Vector3.Lerp(move.DestroyableUnit.Box.transform.position,
                                holder.position, nor);
                            cuttedTargetPos.z -= Mathf.Lerp(0, 3f, 0.5f - Mathf.Abs(0.5f - nor));
                            move.DestroyableUnit.transform.position = cuttedTargetPos;
                        }, () =>
                        {
                            checkBoard.HoldTheUnitTo(move.DestroyableUnit, holder);
                            destroyMoveList.Remove(move);
							Kills = Kills + 1;
							TimerUI.Kill = Kills;
                        },
						speed: 3*speed / (move.DestroyableUnit.Box.transform.position - holder.transform.position).magnitude);
                    destroyMoveList.Add(move);
                }
            }
            yield return new WaitUntil(() => !destroyMoveList.Any());
            yield return new WaitForSeconds(0.3f);
            onComplete?.Invoke();

        }


    

        public abstract bool RequestToMove(MoveRequest moveRequest);


        public struct InitParams
        {
            public UnitType UnitType { get; set; }
            public ICheckerBoard CheckerBoard { get; set; }
            public ITurnAdapter TurnAdapter { get; set; }
            public string Name { get; set; }
            public string AvatarUrl { get; set; }
        }
    }
}