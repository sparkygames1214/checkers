﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerSelectionPanel : MonoBehaviour
{

    [SerializeField] private RectTransform _opponentTarget;
    [SerializeField] private Image _avatarPrefab;
    [SerializeField] private float _spacing;
    [SerializeField] private float _speed;
    [SerializeField] private TMPro.TextMeshProUGUI _myNameTxt, _opponentNameTxt;
    [SerializeField] private Image _myImg;
    [SerializeField] private Button _exitBtn;

//    public PhotonPlayer Opponent { get; private set; }

    private GameStartUpData? GameStartEventData { get; set; }

    private readonly List<Image> _avatarTiles = new List<Image>();

    private Vector2 AvatarSize
    {
        get
        {
            var corners = new Vector3[4];
            _opponentTarget.GetWorldCorners(corners);
            return new Vector2(corners[2].x - corners[0].x,corners[2].y - corners[0].y);
        }
    }


    private void Awake()
    {
       Invoke(nameof(StartSinglePlayerIfPlayerNotFound),20);
    }

    private void StartSinglePlayerIfPlayerNotFound()
    {
        if (GameStartEventData!=null)
        {
            return;
        }

        PhotonNetwork.LeaveRoom();

        var playerData = ResourceManager.FakePlayerDatas.GetRandom();
        GameStartEventData = new GameStartUpData
        {
            IsNetworkMode = false,
            GameRoomGroupInfo = ResourceManager.GameRoomGroupInfos.First(),
            GameProgress = new GameProgress(),
            OpponentPlayerData = new GameStartUpData.PlayerData
            {
                Name = playerData.name,
                AvatarUrl = playerData.avatarUrl
            },
            MyUnitType = Random.value>0.5f? UnitType.White : UnitType.Black
        };
    }

    private void OnEnable()
    {
        GameStartEventData = null;
        StartCoroutine(Execute());
        PhotonPlayerSelectionManager.ReceivedGameStart +=PhotonPlayerSelectionManagerOnReceivedGameStart;
    }

    public void OnClickExit()
    {
        PhotonPlayerSelectionManager.Instance.Terminate();
        SceneManager.LoadScene("MainMenu");
    }

    private void PhotonPlayerSelectionManagerOnReceivedGameStart(GameStartUpData gameStartUpData)
    {
        if(GameStartEventData!=null)
            return;

        GameStartEventData = gameStartUpData;
    }

    private IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        var list = ResourceManager.SampleAvatars.ToList();
        for (var i = 0; i < list.Count; i++)
        {
            var image = Instantiate(_avatarPrefab, _opponentTarget.position + Vector3.up * (_spacing + AvatarSize.y) * i, Quaternion.identity, _opponentTarget);
            image.sprite = list[i];
            _avatarTiles.Add(image);
        }

        _myNameTxt.text = AuthManager.Player.Name;
        if (!string.IsNullOrEmpty(AuthManager.Player.AvatarUrl))
        {
            StartCoroutine(SetImage(_myImg, AuthManager.Player.AvatarUrl));
        }
        else
        {
            _myImg.sprite = ResourceManager.SampleAvatars.GetRandom();
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        PhotonPlayerSelectionManager.ReceivedGameStart -= PhotonPlayerSelectionManagerOnReceivedGameStart;
    }



    private IEnumerator Execute()
    {
        yield return new WaitUntil(() => _avatarTiles.Count>0);
        while (GameStartEventData==null)
        {
            MoveAvatarTiles(_speed*Time.deltaTime);
            yield return null;
        }

        var nextTile = GetNextTile();
        // ReSharper disable once PossibleInvalidOperationException
        var opponentPhotonPlayer = GameStartEventData.Value.OpponentPlayerData;

        _opponentNameTxt.text = opponentPhotonPlayer.Name;

        if (!string.IsNullOrEmpty(opponentPhotonPlayer.AvatarUrl))
        {
            StartCoroutine(SetImage(nextTile, opponentPhotonPlayer.AvatarUrl));
        }

        var leftDistance = nextTile.transform.position.y - _opponentTarget.transform.position.y;

        while (leftDistance>0)
        {
            var target = Mathf.Clamp(Mathf.Lerp(leftDistance, -20,
                2 * Time.deltaTime),0,Mathf.Infinity);

            MoveAvatarTiles(leftDistance-target);
            leftDistance = target;
            yield return null;
        }
        yield return new WaitForSeconds(0.3f);
        // ReSharper disable once PossibleInvalidOperationException
        GameManager.StartTheGame(GameStartEventData.Value);

    }

    private IEnumerator SetImage(Image img, string url)
    {       
        var task = new ImageTask(url);
        yield return task;
        img.sprite = task.Sprite;
    }

    /// <summary>
    /// Get the Avatar Tile Next Will Set
    /// </summary>
    /// <returns></returns>
    private Image GetNextTile()
    {
        return _avatarTiles
            .Where(image => image.transform.position.y > _opponentTarget.transform.position.y + AvatarSize.y / 3)
            .OrderBy(image => image.transform.position.y).First();
    }

    public void StartTheGame(PhotonPlayer player)
    {

    }

    private void MoveAvatarTiles(float delta)
    {
        foreach (var tile in _avatarTiles)
        {
            tile.transform.position -= Vector3.up*delta;
        }

        if (_avatarTiles.First().transform.position.y < _opponentTarget.transform.position.y - 4 * AvatarSize.y / 3)
        {
            var fImg = _avatarTiles.First();
            fImg.transform.position = _avatarTiles.Last().transform.position + Vector3.up * (_spacing + AvatarSize.y);
            _avatarTiles.RemoveAt(0);
//            Debug.Log("Avatar Size Y:"+AvatarSize.y);
            _avatarTiles.Add(fImg);
        }
    }
}
