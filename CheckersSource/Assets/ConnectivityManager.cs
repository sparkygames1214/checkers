﻿// ReSharper disable once HollowTypeName
using System;
using UnityEngine;

// ReSharper disable once HollowTypeName
public class ConnectivityManager : AutoCreateSingleton<ConnectivityManager>
{
    private  bool _lastConnection;
    private const float CHECK_TIME_RATE = 0.1f;

    public static event Action<bool> ConnectivityStateChanged;

    public static bool HasConnection
    {
        get { return Application.internetReachability != NetworkReachability.NotReachable; }
    }

    private bool LastConnection
    {
        get { return _lastConnection; }
        set
        {
            if (_lastConnection==value)
            {
                return;
            }

            _lastConnection = value;
            if (ConnectivityStateChanged != null) ConnectivityStateChanged(value);
        }
    }

    protected override void Awake()
    {
        base.Awake();
        InvokeRepeating("CheckInternetConnection",0,CHECK_TIME_RATE);
    }

    void CheckInternetConnection()
    {
        LastConnection = HasConnection;
    }
}