﻿using System;
using System.Collections;
using UnityEngine;

namespace Game
{
}

public class Timer :MonoBehaviour,ITimer
{
    public event EventHandler Completed;
    public event Action<object,float> Tick;
    public bool Running { get; set; }

    public float Duration
    {
        get { return _duration; }
        set
        {
            if(Running)
                throw new InvalidOperationException();
            TimeLeft = _duration;
            _duration = value;
        }
    }

    public float TimeLeft { get; set; }

    private IEnumerator _timerCor;
    private float _duration;


    public void StartTimer()
    {
        if(Running)
            return;
        _timerCor = RunTime();
        StartCoroutine(_timerCor);
        Running = true;

    }

    public void StopTimer()
    {
        if(!Running)
            return;
        StopCoroutine(_timerCor);
        Running = false;
    }

    public void ResetTimer()
    {
        if(Running)
            StopTimer();

        TimeLeft = Duration;
    }

    private IEnumerator RunTime()
    {
        while (TimeLeft>0)
        {
            yield return null;
            TimeLeft = Mathf.Clamp(TimeLeft-Time.deltaTime,0,Mathf.Infinity);
            Tick?.Invoke(this, TimeLeft);
        }

        Completed?.Invoke(this, EventArgs.Empty);
    }
}