﻿// /*
// Created by Darsan
// */

using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Rule : ScriptableObject
{

    [SerializeField] private string _id;
    [SerializeField] private string _name;
    [Header("----------Rules-------------")]
    [SerializeField] private PawnMovementType _pawnMovementType;
    [ConditionalField(nameof(_pawnMovementType),PawnMovementType.Corner)]
    [SerializeField] private bool _isBlackCorners;
    [SerializeField] private int _boardSize;
    [SerializeField] private int _pawnInitialRowCount;

    [ConditionalField(nameof(_pawnMovementType), PawnMovementType.Corner)]
    [SerializeField] private bool _allowBackwardCutForPawns;
    [ConditionalField(nameof(_allowBackwardCutForPawns),true)]
    [SerializeField] private bool _continuePawnJumpAtBoardEnd = true;
    [SerializeField] private bool _allowPawnCutLead;


    [Header("Lead - King/Queen")]
    [SerializeField]private bool _isQueen;
    [SerializeField] private bool _canLeadMoveMultiBox;
    [SerializeField] private bool _isLeadCutEndPositionFixed;


    [Header("Cutting")]
    [SerializeField]private List<CutOrder> _cutOrders = new List<CutOrder>();


    public string Name => _name;
    public bool ContinuePawnJumpAtBoardEnd => _continuePawnJumpAtBoardEnd;
    public string Id => _id;
    public PawnMovementType PawnMovementType => _pawnMovementType;
    public bool IsBlackCorners => _isBlackCorners;
    public int BoardSize => _boardSize;
    public int PawnInitialRowCount => _pawnInitialRowCount;
    public bool AllowBackwardCutForPawns => _allowBackwardCutForPawns;
    public bool AllowPawnCutLead => _allowPawnCutLead;
    public bool IsQueen => _isQueen;
    public bool CanLeadMoveMultiBox => _canLeadMoveMultiBox;
    public bool IsLeadCutEndPositionFixed => _isLeadCutEndPositionFixed;
    public IEnumerable<CutOrder> CutOrders => _cutOrders;
}


public enum PawnMovementType
{
    Corner
}

public enum CutOrder
{
    MultipleCut,
    MultipleLeadCut,
    MultipleCutWithLead,
    CaptureKinFirst
}