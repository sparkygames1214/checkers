﻿using System.Collections;
using System.Collections.Generic;
using MainMenu;
using UnityEngine;

public class SettingsPanel : ShowHidable {

    public void OnClickBoardSelection()
    {
//        UIManager.Instance.BoardSelectionPanel.Show();
    }

    public void OnClickLogOut()
    {
        AuthManager.LogOut();
        GameManager.LoadScene(Scene.AuthScene);
    }
}
