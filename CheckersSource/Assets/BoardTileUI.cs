﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BoardTileUI : MonoBehaviour,IPointerClickHandler
{
    public event Action<BoardTileUI> Clicked; 

    [SerializeField] private GameObject _selectedEffect;
    [SerializeField] private int _boardSize;

    public bool Selected
    {
        get { return _selectedEffect.activeSelf;}
        set { _selectedEffect.SetActive(value); }
    }

    public int BoardSize
    {
        get { return _boardSize; }
        set { _boardSize = value; }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Clicked?.Invoke(this);
    }
}