﻿namespace Game
{
    public class GamePlayer : BasePlayer
    {
        public override bool RequestToMove(MoveRequest moveRequest)
        {
            var task = RequestForMove(moveRequest);

            if (task == null)
            {
                return false;
            }
            MakeMoves(moveRequest.Unit,task.Moves, () =>
            {
                task.MarkAsComplete();
            });
            return true;
        }
    }
}