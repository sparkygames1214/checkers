﻿using UnityEngine;

namespace Game
{
    public class Theme : MonoBehaviour
    {
        [SerializeField] private ProgressBar _myProgressBar;
        [SerializeField] private ProgressBar _opponentProgressBar;
        [SerializeField] private CheckerBoard _board;
        [SerializeField] private Unit _unit;

        public ProgressBar MyProgressBar => _myProgressBar;
        public ProgressBar OpponentProgressBar => _opponentProgressBar;
        public CheckerBoard Board => _board;
        public Unit Unit => _unit;
    }
}