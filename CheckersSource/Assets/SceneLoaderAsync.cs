﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoaderAsync : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private string _sceneName;
    [SerializeField] private float _minLoadTime = 1f;

    private IEnumerator Start()
    {
        var operation = SceneManager.LoadSceneAsync(_sceneName);
        operation.allowSceneActivation = false;

        var normalized = 0f;
        while (normalized<1f)
        {
            normalized = Mathf.MoveTowards(normalized, 1f, Time.deltaTime * 1f / _minLoadTime);
            _slider.normalizedValue = Mathf.Min(operation.progress, normalized);
            yield return null;
        }

        operation.allowSceneActivation = true;

        while (!operation.isDone)
        {
            _slider.normalizedValue = operation.progress;
            yield return null;
        }
    }
}
