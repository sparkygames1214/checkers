﻿using System;
using UnityEngine;

namespace Game
{
    // ReSharper disable once HollowTypeName
    public class LevelManager : MonoBehaviour
    {
        public static LevelManager Instance { get; private set; }

        public static event Action<bool> GameOver;
        public static event Action GameStart;

        [SerializeField] private GameHandler _gameHandler;
        [SerializeField] private CheckerBoard _board;
//        [SerializeField] private Player _player;
//        [SerializeField] private AIPlayer _randomMovePlayer;
//        [SerializeField] private GamePlayer _gamePlayer;
        [SerializeField] private NetworkLevelManager _networkLevelManager;
        [SerializeField] private UIManager _uiManager;
        [SerializeField] private Timer _timer;

        public GameStartUpData GameStartUpData { get; private set; }

        public UnitType ThisUnitType { get; private set; }

        public IPlayer MyPlayer { get; private set; }
        public IPlayer OpponentPlayer { get; private set; }

        public float TurnTime { get; private set; }
        public IGameHandler GameHandler => _gameHandler;

        public bool IsNetworkMode => GameStartUpData.IsNetworkMode;

        public Theme Theme { get; private set; }

        public Rule Rule => GameManager.SelectedRule;

        // ReSharper disable once MethodTooLong
        private void Awake()
        {
            Instance = this;

            GameStartUpData = GameManager.GetAndRemoveLoadedSceneData<GameStartUpData>(Scene.MainScene);
            Theme = Instantiate(GameStartUpData.Theme.themePrefab);
            _board = Theme.Board;

            TurnTime = 30;
            _board.BoardSize = new VecInt(Rule.BoardSize, Rule.BoardSize);
            _board.Init();

            //Set For this unit type
            ThisUnitType = GameStartUpData.MyUnitType;
            SetForThisUnitType(ThisUnitType);

            IPlayer whitePlayer;
            IPlayer blackPlayer;

            var player = new GameObject("Player").AddComponent<Player>();
            player.transform.parent = transform;

            if (GameStartUpData.IsNetworkMode)
            {
                MyPlayer = player;
                var gamePlayer = new GameObject("GamePlayer").AddComponent<GamePlayer>();
                gamePlayer.transform.parent = transform;
                OpponentPlayer = gamePlayer ;
                // ReSharper disable once PossibleInvalidOperationException
                whitePlayer = GameStartUpData.MyUnitType == UnitType.White ? MyPlayer : OpponentPlayer;
                blackPlayer = Equals(whitePlayer, player) ? OpponentPlayer : MyPlayer;


                player.Init(new BasePlayer.InitParams
                {
                    CheckerBoard = _board,
                    UnitType = ThisUnitType,
                    TurnAdapter = _networkLevelManager,
                    Name = PhotonNetwork.player.NickName,
                    AvatarUrl = PhotonNetwork.player.GetAvatarUrl()
                });

//                _player.CheckerMoveCalculator = new CornerMoveCalculator(_board, Rule); //Set To My Player

                gamePlayer.Init(new BasePlayer.InitParams
                {
                    CheckerBoard = _board,
                    UnitType = ThisUnitType == UnitType.White ? UnitType.Black : UnitType.White,
                    TurnAdapter = _networkLevelManager,
                    Name = 
                        GameStartUpData.OpponentPlayerData.Name,
                    AvatarUrl = GameStartUpData.OpponentPlayerData.AvatarUrl
                });
                _networkLevelManager.Init(new NetworkLevelManager.InitParams
                {
                    CheckerBoard = _board,
                    GameHandler = _gameHandler,
                    MyPlayer = MyPlayer,
                    OpponentPlayer = OpponentPlayer,
                    LevelManager = this,
                    OpponentPhotonPlayer = GameStartUpData.OpponentPlayerData.PhotonPlayer
                });
                _networkLevelManager.OpponentPlayerLeft += () =>
                {
                    if (_gameHandler.CurrentState == GameHandlerState.Playing)
                    {
                        _gameHandler.StopTheGame();
                        OverTheGame(true);
                    }
                };
            }
            else if(!GameStartUpData.VsPlayer)
            {
                MyPlayer = player;
                var aiPlayer = new GameObject("AIPlayer").AddComponent<AIPlayer>();
                aiPlayer.transform.parent = transform;
                OpponentPlayer = aiPlayer;

                //TODO:Complete the setup if not Network Mode
                whitePlayer = GameStartUpData.MyUnitType == UnitType.White ? MyPlayer : OpponentPlayer;
                blackPlayer = Equals(whitePlayer, player) ? OpponentPlayer : MyPlayer;

                player.Init(new BasePlayer.InitParams
                {
                    CheckerBoard = _board,
                    UnitType = ThisUnitType,
                    TurnAdapter = new SimpleDelayTurnAdapter(new Vector2(0.1f, 0.1f), new Vector2(0.1f, 0.1f)),
                    Name = AuthManager.Player.Name,
                    AvatarUrl = AuthManager.Player.AvatarUrl
                });

//                _player.CheckerMoveCalculator = new CornerMoveCalculator(_board, Rule); 

                aiPlayer.Init(new BasePlayer.InitParams
                {
                    CheckerBoard = _board,
                    UnitType = ThisUnitType == UnitType.White ? UnitType.Black : UnitType.White,
                    TurnAdapter = new SimpleDelayTurnAdapter(new Vector2(0.1f, .1f), new Vector2(.1f, .1f)),
                    Name = GameStartUpData.OpponentPlayerData.Name,
                    AvatarUrl = GameStartUpData.OpponentPlayerData.AvatarUrl
                });
                aiPlayer.DelayMoveTimeBound = new Vector2(0.7f, 1.3f);
            }
            else
            {
                MyPlayer = player;
                var otherPlayer = new GameObject("OtherPlayer").AddComponent<Player>();
                otherPlayer.transform.parent = transform;
                OpponentPlayer = otherPlayer;

                //TODO:Complete the setup if not Network Mode
                whitePlayer = GameStartUpData.MyUnitType == UnitType.White ? MyPlayer : OpponentPlayer;
                blackPlayer = Equals(whitePlayer, player) ? OpponentPlayer : MyPlayer;

                player.Init(new BasePlayer.InitParams
                {
                    CheckerBoard = _board,
                    UnitType = ThisUnitType,
                    TurnAdapter = new SimpleDelayTurnAdapter(new Vector2(0.1f, 0.1f), new Vector2(0.1f, 0.1f)),
                    Name = AuthManager.Player.Name,
                    AvatarUrl = AuthManager.Player.AvatarUrl
                });

                //                _player.CheckerMoveCalculator = new CornerMoveCalculator(_board, Rule); 

                otherPlayer.Init(new BasePlayer.InitParams
                {
                    CheckerBoard = _board,
                    UnitType = ThisUnitType == UnitType.White ? UnitType.Black : UnitType.White,
                    TurnAdapter = new SimpleDelayTurnAdapter(new Vector2(0.1f, .1f), new Vector2(.1f, .1f)),
                    Name = GameStartUpData.OpponentPlayerData.Name,
                    AvatarUrl = GameStartUpData.OpponentPlayerData.AvatarUrl
                });
            }

            //            ((BasePlayer)MyPlayer).TimerUI = Theme.MyProgressBar;
            //            ((BasePlayer)OpponentPlayer).TimerUI = Theme.OpponentProgressBar;

            _gameHandler.Init(new GameHandler.InitParams
            {
                Board = _board,
                BlackPlayer = blackPlayer,
                WhitePlayer = whitePlayer,
                Timer = GameStartUpData.IsNetworkMode ? (ITimer) _networkLevelManager : _timer,
                TurnTime = TurnTime,
                UnitPrefab = Theme.Unit,
                Rule = Rule
            });


            _uiManager.Init();

            if (!GameStartUpData.IsNetworkMode)
                Invoke(nameof(StartTheGame), 2f);
        }

        public void StartTheGame()
        {
            _gameHandler.GameOver += result => OverTheGame(result.Winner == MyPlayer);
            _gameHandler.StartTheGame();
            GameStart?.Invoke();
        }

        private void SetForThisUnitType(UnitType unitType)
        {
            // ReSharper disable once TooManyChainedReferences
            // ReSharper disable once PossibleNullReferenceException
            var angles = _board.transform.eulerAngles;
            angles.z = unitType == UnitType.Black ? 0 : 180;
            _board.transform.eulerAngles = angles;
        }

        public void CancelGame()
        {
            GameStartUpData.GameProgress.Terminate();
           GameManager.LoadScene(Scene.MainMenu);
        }

        private void OverTheGame(bool win)
        {
            if (win)
            {
                AuthManager.Player.AddCoins(GameStartUpData.GameRoomGroupInfo.BetAmount);
            }

            var statics = AuthManager.Player.PlayerStatics;
            statics.Experience = (int) Mathf.Clamp(statics.Experience + (win ? 100 : -100), 0, Mathf.Infinity);

            statics.GamePlayed = statics.GamePlayed + 1;
            statics.GameWon += win ? 1 : 0;
            AuthManager.Player.UpdatePlayerStats(statics);


            GameOver?.Invoke(win);
        }
    }
}