﻿// /*
// Created by Darsan
// */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardPanel : ShowHidable
{
    [SerializeField] private DailyRewardTileUI _tilePrefab;
    [SerializeField] private RectTransform _content;
    [SerializeField] private List<DailyRewardPrice> _rewards = new List<DailyRewardPrice>();
    [SerializeField] private Button _collectBtn;

    private readonly List<DailyRewardTileUI> _tiles = new List<DailyRewardTileUI>();

    public static DateTime LastRewardTime
    {
        get => PrefManager.GetDate(nameof(LastRewardTime), DateTime.MinValue);
        private set => PrefManager.SetDate(nameof(LastRewardTime), value);
    }

    public static int LastRewardedDay
    {
        get => PrefManager.GetInt(nameof(LastRewardedDay));
        private set => PrefManager.SetInt(nameof(LastRewardedDay),value);
    }

    public static bool RewardAvailable => DateTime.Now.Subtract(LastRewardTime).Days >= 1;
    public bool CurrentDayCollected => CurrentDay - LastRewardedDay <=0;

    public int CurrentDay { get; private set; }

    private void Awake()
    {
        var days = DateTime.Now.Subtract(LastRewardTime).Days;
        if (days > 1)
        {
            LastRewardedDay = 0;
            CurrentDay = 1;
        }
        else if(days == 1)
        {
            CurrentDay = (LastRewardedDay + 1) % (_rewards.Count + 1);
        }
        else
        {
            CurrentDay = LastRewardedDay;
        }

        for (var i = 0; i < _rewards.Count; i++)
        {
            var rewardTileUI = Instantiate(_tilePrefab, _content);
            rewardTileUI.DailyRewardPrice = _rewards[i];
            rewardTileUI.Day = i + 1;
            rewardTileUI.Locked = i + 1 > CurrentDay;
            rewardTileUI.Selected = i + 1 == CurrentDay;
            rewardTileUI.Collected = i  < LastRewardedDay ;
            rewardTileUI.Clicked+=RewardTileUIOnClicked;
            _tiles.Add(rewardTileUI);
        }
    }

    private void RewardTileUIOnClicked(DailyRewardTileUI tile)
    {
        if(tile.Locked || tile.Collected)
            return;
        Collect();
    }

    public void Collect()
    {
        LastRewardedDay = CurrentDay;
        LastRewardTime = DateTime.Now;
        var dailyRewardTileUI = _tiles.First(ui => ui.Day == CurrentDay);
        AuthManager.Player.AddCoins(dailyRewardTileUI.DailyRewardPrice.coins);
        dailyRewardTileUI.Collected = true;
    }


    private void Update()
    {
        _collectBtn.interactable = !CurrentDayCollected;
    }
}