﻿// ReSharper disable once CheckNamespace

using System;

namespace Game
{
    public interface IGameHandler
    {
        event Action<IPlayer> TurnStart;
        event Action<IPlayer> TurnEnd;
        event Action<GameHandlerGameOverResult> GameOver;

        IPlayer BlackPlayer { get; }
        IPlayer CurrentPlayer { get; }
        TurnState CurrentTurnState { get; }
        GameHandlerState CurrentState { get; }
        IPlayer WhitePlayer { get; }
        float TurnElapsedTime { get; }
        float TurnTime { get; }

        void StartTheGame();
        MoveTask RequestForMove(IPlayer player, MoveRequest request);
        IUnit GetUnitById(string id);
    }

    public struct GameHandlerGameOverResult
    {
        public IPlayer Winner { get; set; }
        public GameOverReason GameOverReason { get; set; }
    }

    public enum GameOverReason
    {
        End,PlayerLeft,TimeUp
    }
}