﻿// /*
// Created by Darsan
// */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rules : ScriptableObject,IEnumerable<Rule>
{
    public static Rules Default => Resources.Load<Rules>(DEFAULT_FILE_NAME);

    public const string DEFAULT_FILE_NAME = nameof(Rules);


    [SerializeField]private List<Rule> _rules = new List<Rule>();


    public IEnumerator<Rule> GetEnumerator() => _rules.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}