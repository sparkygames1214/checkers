﻿//using System;
//using System.Collections.Generic;
//using Facebook.Unity;
//using UnityEngine;

//// ReSharper disable once HollowTypeName
//public class FacebookManager : AutoCreateSingleton<FacebookManager>,IInilizable {

//    public bool Initialized { get; private set; }
//    public bool FBInitialized => FB.IsInitialized;

//    public bool IsLogIn => AccessToken.CurrentAccessToken != null;

//    public AccessToken AccessToken => AccessToken.CurrentAccessToken;

//    public string FirstName { get; private set; } = "";
//    public string FullName => $"{FirstName} {LastName}";
//    public string LastName { get; private set; } = "";

//    public string AvatarUrl => BuildAvatarUrl(AccessToken.UserId);

//    protected override void Awake()
//    {
//        base.Awake();
//        Init();
//    }


//    public void Init()
//    {
//        if (Initialized)
//        {
//            return;
//        }
//        if (!FB.IsInitialized)
//        {
//            // Initialize the Facebook SDK
            
//        }
//        else
//        {
//            // Already initialized, signal an app activation App Event
//            FB.ActivateApp();
//        }
//        Initialized = true;
//    }


//    // ReSharper disable once MethodTooLong
//    public void LogIn(Action<AccessToken> completed=null)
//    {
//        if(IsLogIn)
//        {
//            throw new NotImplementedException();
//        }

//        if (!FBInitialized)
//        {
//            InitializeFB(() => LogIn(completed));
//            return;
//        }
//        var perms = new List<string>() { "public_profile", "email"};
//        FB.LogInWithReadPermissions(perms, result =>
//        {
//            if (result.Error != null)
//            {
//                completed?.Invoke(null);
//                return;
//            }
//            FB.API("me?fields=first_name,last_name", HttpMethod.GET, graphResult =>
//            {
//                if(graphResult.Error==null)
//                {
//                    FirstName = graphResult.ResultDictionary["first_name"].ToString();
//                    LastName = graphResult.ResultDictionary["last_name"].ToString();
//                }

//                completed?.Invoke(result.AccessToken);
//            });
//        });
//    }

//    public static string BuildAvatarUrl(string playerId)
//    {
//        return "https" + "://graph.facebook.com/" + playerId + "/picture?type=normal";
//    }

//    public void LogOut()
//    {
//        FB.LogOut();
//    }

//    private void InitializeFB(Action completed=null)
//    {
//        FB.Init(()=>
//        {
//            FB.ActivateApp();
//            completed?.Invoke();
//        }, shown => { });
//    }
//}
