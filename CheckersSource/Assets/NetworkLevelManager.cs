﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    // ReSharper disable once HollowTypeName
    public partial class NetworkLevelManager : Photon.PunBehaviour, IInilizable<NetworkLevelManager.InitParams>
    {
        private const byte MOVE_CODE = 20;
        private const byte GAME_START_REQUEST_CODE = 10;
        private const byte GAME_START_REQUEST_ACCEPTED_CODE = 11;
        private const byte TURN_END_REQUEST_CODE = 21;
        private const byte TURN_END_REQUEST_ACCEPTED_CODE = 22;
        private const byte TIME_UP_REQUEST_CODE = 23;
        private const byte WANT_TO_PLAY_AGAIN_REQUEST_CODE = 24;
        private const byte RESTART_REQUEST_CODE = 25;

        public static NetworkLevelManager Instance { get; private set; }

        public event Action GameStart;
        public event Action OpponentPlayerLeft;
        public event Action<byte,object,int> EventCalled;
        

        [SerializeField] private Timer _timer;

        public bool Initialized { get; private set; }

        public PlayerGroup MyPlayer { get; set; }
        public PlayerGroup OpponentPlayer { get; set; }
        public bool GameStarted { get; set; }

        private ICheckerBoard _checkerBoard;
        private IGameHandler _gameHandler;
        private LevelManager _levelManager;
        public bool Active { get { return gameObject.activeSelf; } set{gameObject.SetActive(value);} }

        public bool IsMasterClient => PhotonNetwork.isMasterClient;

        public PlayerGroup? CurrentPlayer =>
            _gameHandler.CurrentPlayer != null
                ? (PlayerGroup?) GetPlayerGroup(_gameHandler.CurrentPlayer)
                : null;

        private readonly List<int> _endTurnRequestedPendingPlayers = new List<int>();
        private readonly List<int> _requestedPendingPlayers = new List<int>();

        private Action _pendingTurnComplete;


        private void Awake()
        {
            Instance = this;
        }


        private void OnEnable()
        {
            PhotonNetwork.OnEventCall += OnEvent;

            _timer.Tick += TimerOnTick;
            _timer.Completed += TimerOnCompleted;
//            GameManager.GameProgress.Terminated +=GameProgressOnTerminated;
        }

        private void GameProgressOnTerminated(GameProgress obj)
        {
            PhotonNetwork.LeaveRoom();
        }

        private void OnDisable()
        {
            // ReSharper disable once DelegateSubtraction
            PhotonNetwork.OnEventCall -= OnEvent;

            _timer.Tick -= TimerOnTick;
            _timer.Completed -= TimerOnCompleted;
//            GameManager.GameProgress.Terminated -= GameProgressOnTerminated;
        }


        // ReSharper disable once MethodTooLong
        private void OnEvent(byte eventCode, object content, int senderId)
        {
            switch (eventCode)
            {
                case GAME_START_REQUEST_CODE:
                    if (IsMasterClient)
                    {
                        if (!_requestedPendingPlayers.Contains(senderId))
                        {
                            _requestedPendingPlayers.Add(senderId);
                        }

                        if (_requestedPendingPlayers.Count == 2)
                        {
                            PhotonNetwork.RaiseEvent(GAME_START_REQUEST_ACCEPTED_CODE, null, true, null);
                            OnEvent(GAME_START_REQUEST_ACCEPTED_CODE,null,PhotonNetwork.player.ID);
                           _requestedPendingPlayers.Clear();
                        }
                    }
                    break;

                case GAME_START_REQUEST_ACCEPTED_CODE:
                    StartTheGame();
                    break;

                case MOVE_CODE:
                    if (senderId != MyPlayer.PhotonPlayer.ID)
                    {
                        MoveRequestFromOpponentReceived((string) content);
                    }
                    break;

                case TURN_END_REQUEST_CODE:
                    if (IsMasterClient)
                    {
                        if (!_endTurnRequestedPendingPlayers.Contains(senderId))
                        {
                            _endTurnRequestedPendingPlayers.Add(senderId);
                        }

                        if (_endTurnRequestedPendingPlayers.Count == 2)
                        {
                            PhotonNetwork.RaiseEvent(TURN_END_REQUEST_ACCEPTED_CODE, null, true, null);
                            OnEvent(TURN_END_REQUEST_ACCEPTED_CODE,null,PhotonNetwork.player.ID);
                            _endTurnRequestedPendingPlayers.Clear();
                        }
                    }
                    break;

                case TURN_END_REQUEST_ACCEPTED_CODE:
                    _pendingTurnComplete?.Invoke();
                    break;

                case TIME_UP_REQUEST_CODE:
                    OnReceiveTimeUpRequest();
                    break;

                case WANT_TO_PLAY_AGAIN_REQUEST_CODE:
                    if (IsMasterClient)
                    {
                        if (!_requestedPendingPlayers.Contains(senderId))
                        {
                            _requestedPendingPlayers.Add(senderId);
                        }

                        if (_requestedPendingPlayers.Count == 2)
                        {
                            PhotonNetwork.RaiseEvent(RESTART_REQUEST_CODE, null, true, null);
                            OnEvent(RESTART_REQUEST_CODE, null, PhotonNetwork.player.ID);
                            _requestedPendingPlayers.Clear();
                        }
                    }
                    break;
                case RESTART_REQUEST_CODE:
                    OnReceiveRestartRequest();
                    break;
            }
            EventCalled?.Invoke(eventCode,content,senderId);
        }

        private void OnReceiveRestartRequest()
        {
            AuthManager.Player.ConsumeCoins(LevelManager.Instance.GameStartUpData.GameRoomGroupInfo.BetAmount/2);
            GameManager.StartTheGame(LevelManager.Instance.GameStartUpData);
        }

        public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
        {
            base.OnPhotonPlayerDisconnected(otherPlayer);
            OpponentPlayerLeft?.Invoke();
        }

        private void StartTheGame()
        {
            if (GameStarted)
            {
                throw new InvalidOperationException();
            }
            _levelManager.StartTheGame();
            GameStarted = true;
            GameStart?.Invoke();
        }

        private void MoveRequestFromOpponentReceived(string moveRequestEncoded)
        {
            OpponentPlayer.GamePlayer
                .RequestToMove(MoveUtils.GetMoveRequestFromEncodedString(moveRequestEncoded,_gameHandler,_checkerBoard));
        }

//        public void RestartTheGame()
//        {
//            // ReSharper disable once PossibleInvalidOperationException
//            var gameRoomGroupInfo = LevelManager.Instance.GameStartUpData.GameStartUpNetworkData.Value.GameRoomGroupInfo;
//            ResourceManager.ConsumeCoins(gameRoomGroupInfo.BetAmount / 2, new TaskAction<bool>(b =>
//            {
//                if (b)
//                {
//                    PhotonNetwork.RaiseEvent(GAME_READY_EVENT, null, true, null);
//                    OnEventCall(GAME_READY_EVENT, null, Player.ID);
//                }
//            }));
//        }

        // ReSharper disable once MethodTooLong
        public void Init(InitParams item)
        {
            if (Initialized)
            {
                return;
            }

            Active = true;
            MyPlayer = new PlayerGroup
            {
                GamePlayer = item.MyPlayer,
                PhotonPlayer = PhotonNetwork.player
            };

            OpponentPlayer = new PlayerGroup
            {
                GamePlayer = item.OpponentPlayer,
                PhotonPlayer = item.OpponentPhotonPlayer
            };

            _gameHandler = item.GameHandler;
            _checkerBoard = item.CheckerBoard;

            

            MyPlayer.GamePlayer.OnPlayerRequestAcceptedToMove += (player, request) => { SendMoveRequest(request); };
            _levelManager = item.LevelManager;
            //Request to Start the Game
            PhotonNetwork.RaiseEvent(GAME_START_REQUEST_CODE, null, true, null);
            OnEvent(GAME_START_REQUEST_CODE,null,PhotonNetwork.player.ID);
            Initialized = true;
            
        }

        public void WantToPlayAgain()
        {
            PhotonNetwork.RaiseEvent(WANT_TO_PLAY_AGAIN_REQUEST_CODE, "", true, null);
            OnEvent(WANT_TO_PLAY_AGAIN_REQUEST_CODE, null, PhotonNetwork.player.ID);
        }

        private void SendMoveRequest(MoveRequest request)
        {
            PhotonNetwork.RaiseEvent(MOVE_CODE, MoveUtils.GetEncodedMoveRequest(request), true,null);
            OnEvent(MOVE_CODE, MoveUtils.GetEncodedMoveRequest(request),PhotonNetwork.player.ID);
        }

        public void SendEvent(byte eventCode, object eventContent, bool sendReliable = true,
            RaiseEventOptions options = null)
        {
            PhotonNetwork.RaiseEvent(eventCode, eventContent, sendReliable, options);
        }

        public PlayerGroup GetPlayerGroup(IPlayer player)
        {
            return player == MyPlayer.GamePlayer ? MyPlayer : OpponentPlayer;
        }

        public PlayerGroup GetPlayerGroup(PhotonPlayer player)
        {
            return player.ID == MyPlayer.PhotonPlayer.ID ? MyPlayer : OpponentPlayer;
        }

        public struct InitParams
        {
            public IPlayer MyPlayer { get; set; }
            public IPlayer OpponentPlayer { get; set; }
            public PhotonPlayer OpponentPhotonPlayer { get; set; }
            public ICheckerBoard CheckerBoard { get; set; }
            public IGameHandler GameHandler { get; set; }
            public LevelManager LevelManager { get; set; }
        }

        public struct PlayerGroup
        {
            public PhotonPlayer PhotonPlayer { get; set; }
            public IPlayer GamePlayer { get; set; }
        }
    }



    // ReSharper disable once HollowTypeName
    public partial class NetworkLevelManager : ITurnAdapter
    {
        public void StartTurn(IPlayer player, Action completed)
        {
            completed?.Invoke();
        }

        public void EndTurn(IPlayer player, Action completed)
        {
            _pendingTurnComplete = completed;
            PhotonNetwork.RaiseEvent(TURN_END_REQUEST_CODE, GetPlayerGroup(player).PhotonPlayer.ID, true, null);
            OnEvent(TURN_END_REQUEST_CODE, GetPlayerGroup(player).PhotonPlayer.ID,PhotonNetwork.player.ID);
        }
    }


    public class DelayTurnAdapter : ITurnAdapter
    {
        private readonly Vector2 _delayStartTurnBound;
        private readonly Vector2 _delayEndTurnBound;

        public DelayTurnAdapter(Vector2 delayStartTurnBound,Vector2 delayEndTurnBound)
        {
            _delayStartTurnBound = delayStartTurnBound;
            _delayEndTurnBound = delayEndTurnBound;
        }

        public void StartTurn(IPlayer player, Action completed)
        {
            var targetTime = Time.time + UnityEngine.Random.Range(_delayStartTurnBound.x,_delayStartTurnBound.y);
            LateCall.Create().Call(() => targetTime<Time.time,completed);
        }

        public void EndTurn(IPlayer player, Action completed)
        {
            var targetTime = Time.time + UnityEngine.Random.Range(_delayEndTurnBound.x, _delayEndTurnBound.y);
            LateCall.Create().Call(() => targetTime < Time.time, completed);
        }
    }


    // ReSharper disable once HollowTypeName
    public partial class NetworkLevelManager:ITimer
    {
        public event EventHandler Completed;
        public event Action<object,float> Tick;
        public bool Running => _timer.Running;

        public float Duration
        {
            get { return _timer.Duration; }
            set { _timer.Duration = value; }
        }

        public float TimeLeft => _timer.TimeLeft;


        public void StartTimer()
        {
            _timer.StartTimer();

        }

        private void TimerOnCompleted(object sender, EventArgs eventArgs)
        {
            if (CurrentPlayer?.GamePlayer == MyPlayer.GamePlayer)
            {
                PhotonNetwork.RaiseEvent(TIME_UP_REQUEST_CODE, null, true, null);
                OnEvent(TIME_UP_REQUEST_CODE,null,MyPlayer.PhotonPlayer.ID);
            }
        }

        private void TimerOnTick(object sender, float f)
        {
            if (CurrentPlayer?.GamePlayer == OpponentPlayer.GamePlayer && f<=0.3f) //Fake timer at last steps
            {
                _timer.TimeLeft += Time.deltaTime;
            }

            Tick?.Invoke(this, f);
        }

        public void StopTimer()
        {
            _timer.StopTimer();
        }

        public void ResetTimer()
        {
            _timer.ResetTimer();
        }

        private void OnReceiveTimeUpRequest()
        {
            Completed?.Invoke(this, EventArgs.Empty);
        }
    }

    
}
