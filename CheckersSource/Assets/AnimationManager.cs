﻿using System;
using System.Collections;
using UnityEngine;

// ReSharper disable once HollowTypeName
public class AnimationManager : AutoCreateSingleton<AnimationManager>
{
    private static IEnumerator BasicLerbCor(Action<float> onUpdate,Action onComplete = null, float speed = 1f,float targetNormalized=1.2f)
    {

        var normalized = 0f;
        while (normalized < 1f)
        {
            normalized = Mathf.Clamp(
                Mathf.Lerp(normalized, targetNormalized, 2 * speed * Time.deltaTime), 0, 1);
            onUpdate?.Invoke(normalized);
            yield return null;
        }

        onComplete?.Invoke();
    }


    public static Coroutine BasicMoveTowards(Action<float> onUpdate, Action onComplete = null, float speed = 1f)
    {
        return Instance.StartCoroutine(BasicMoveTowardsCor(onUpdate, onComplete, speed));
    }


    private static IEnumerator BasicMoveTowardsCor(Action<float> onUpdate, Action onComplete = null, float speed = 1f)
    {

        var normalized = 0f;
        while (normalized < 1f)
        {
			normalized = Mathf.MoveTowards(normalized, 1,  speed * Time.deltaTime);
            onUpdate?.Invoke(normalized);
            yield return null;
        }

        onComplete?.Invoke();
    }


    public static Coroutine BasicLerb(Action<float> onUpdate, Action onComplete = null, float speed = 1f)
    {
        Instance.StopCoroutine("");
        return Instance.StartCoroutine(BasicLerbCor(onUpdate, onComplete, speed));
    }

    public  static void CancelCoroutine(Coroutine coroutine)
    {
        Instance.StopCoroutine(coroutine);
    }
}