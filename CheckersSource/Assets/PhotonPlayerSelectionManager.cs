﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable once HollowTypeName
public class PhotonPlayerSelectionManager : Photon.PunBehaviour
{
    public static PhotonPlayerSelectionManager Instance { get; private set; }
    public static event Action<GameStartUpData> ReceivedGameStart;
    private const int EVENT_START_CODE = 100;

    private const int MATCH_DONE_EVENT = EVENT_START_CODE + 1;
    private const int GAME_READY_EVENT = EVENT_START_CODE + 2;
    private const int GAME_START_EVENT = EVENT_START_CODE + 3;

    public delegate void CreatedRoomListener(string roomName);
 
    public event CreatedRoomListener CreatedRoom;

    public IGameRoomGroupInfo GameRoomGroupInfo { get; set; }

    private Room Room => PhotonNetwork.room;

    private PhotonPlayer Player => PhotonNetworkManager.Player;

    private readonly List<int> _readyPlayers = new List<int>();

    public bool IsPrivateRoom { get; private set; }

    private bool _gameStarting;

    private void Awake()
    {
        Instance = this;
    }

    // ReSharper disable once MethodTooLong
    private IEnumerator Start()
    {


        if (!PhotonNetworkManager.Connected)
        {
            throw new InvalidOperationException("The Photon Have to connected before Start Player Selection!");
        }

        var data = GameManager.GetAndRemoveLoadedSceneData<PlayerSelectionStartUpData>(Scene.PlayerSelection);
       
        GameRoomGroupInfo = data.GameRoomGroupInfo;
        IsPrivateRoom = data.IsPrivateRoom;

        if (data.OnCreatedRoom!=null)
        {
            CreatedRoom += data.OnCreatedRoom;
        }

        yield return new WaitUntil(() => PhotonNetwork.insideLobby || PhotonNetwork.inRoom);

        if (data.IsJoinToExistingRoom)
        {
            Join(data.SelectedRoomName);
        }
        else if (IsPrivateRoom)
        {
            CreateRoom(false);
        }
        else
        {
            Join();
        }
    }


    private void OnEnable()
    {
        PhotonNetwork.OnEventCall += OnEventCall;
    }


    private void OnDisable()
    {
        // ReSharper disable once DelegateSubtraction
        PhotonNetwork.OnEventCall -= OnEventCall;
        if (!_gameStarting && PhotonNetwork.room != null)
        {
            LeaveRoom();
        }

    }


    private void Join()
    {
        PhotonNetwork.JoinRandomRoom(
            new ExitGames.Client.Photon.Hashtable {{CommonProperties.GAME_ROOM_KEY, GameRoomGroupInfo.Id},{CommonProperties.RULE_KEY,GameManager.SelectedRule.Id}}, 2);
        Debug.Log("Join Random Room");
    }

    private void Join(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        base.OnPhotonRandomJoinFailed(codeAndMsg);
        CreateRoom();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        base.OnPhotonPlayerConnected(newPlayer);
        Debug.Log(
            $"New Player Connected:{newPlayer.ID} player count: {PhotonNetwork.room.PlayerCount} isMasterClient:{PhotonNetwork.isMasterClient}");

        if (Room.PlayerCount == 2 && PhotonNetwork.isMasterClient)
        {
            Room.IsOpen = false;
            PhotonNetwork.RaiseEvent(MATCH_DONE_EVENT, null, true, null);
            OnEventCall(MATCH_DONE_EVENT, null
                , Player.ID);
        }
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        base.OnPhotonPlayerDisconnected(otherPlayer);
        Debug.Log($"OnPhoton Player Disconnected {otherPlayer.ID}");
    }


    private void CreateRoom(bool visible = true)
    {
        var roomOptions = new RoomOptions
        {
            MaxPlayers = 2,
            CustomRoomPropertiesForLobby = new[] {CommonProperties.GAME_ROOM_KEY,CommonProperties.RULE_KEY},
            CustomRoomProperties =
                new ExitGames.Client.Photon.Hashtable {{CommonProperties.GAME_ROOM_KEY, GameRoomGroupInfo.Id},{CommonProperties.RULE_KEY,GameManager.SelectedRule.Id}},
            IsVisible = visible
        };
        PhotonNetwork.CreateRoom(null, roomOptions, TypedLobby.Default);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log(nameof(OnCreatedRoom));
        base.OnCreatedRoom();
        CreatedRoom?.Invoke(Room.Name);
    }

    public override void OnPhotonCreateRoomFailed(object[] codeAndMsg)
    {
        base.OnPhotonCreateRoomFailed(codeAndMsg);
        CreateRoom();
    }


    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }


    public void Terminate()
    {
        if (PhotonNetwork.room != null)
            PhotonNetwork.LeaveRoom();
    }

  

    private void OnEventCall(byte eventCode, object content, int senderId)
    {
        switch (eventCode)
        {
            case MATCH_DONE_EVENT:
                ReceivedMatchDoneEvent();
                break;
            case GAME_READY_EVENT:
                ReceivedGameReadyEvent(senderId);
                break;
            case GAME_START_EVENT:
                ReceivedGameStartEvent(JsonUtility.FromJson<GameStartEventData>(content as string));
                break;
        }
    }

    private void ReceivedMatchDoneEvent()
    {
        Debug.Log(nameof(ReceivedMatchDoneEvent));
        AuthManager.Player.ConsumeCoins(GameRoomGroupInfo.BetAmount / 2, new TaskAction<bool>(b =>
        {
            if (b)
            {
                PhotonNetwork.RaiseEvent(GAME_READY_EVENT, null, true, null);
                OnEventCall(GAME_READY_EVENT, null, Player.ID);
            }
        }));
    }

    private void ReceivedGameReadyEvent(int senderId)
    {
        Debug.Log(nameof(ReceivedGameReadyEvent));
        if (PhotonNetwork.isMasterClient)
        {
            _readyPlayers.Add(senderId);

            if (_readyPlayers.Count == 2)
            {
                if (PhotonNetwork.room.PlayerCount == 2)
                {
                    var eventData = new GameStartEventData
                    {
                        PhotonId = PhotonNetwork.player.ID,
                        UnitType = Random.Range(0, 2) == 0 ? UnitType.Black : UnitType.White
                    };
                    PhotonNetwork.RaiseEvent(GAME_START_EVENT, JsonUtility.ToJson(eventData), true, null);
                    OnEventCall(GAME_START_EVENT, JsonUtility.ToJson(eventData), Player.ID);
                }
                else
                {
                    //TODO:// If second player disconnected
                }
            }
        }
    }


    private void ReceivedGameStartEvent(GameStartEventData data)
    {
        Debug.Log(nameof(ReceivedGameStartEvent));
        _gameStarting = true;
        ReceivedGameStart?.Invoke(new GameStartUpData
        {
            MyUnitType = data.PhotonId == Player.ID ? data.UnitType : data.UnitType.GetOpponent(),
            IsNetworkMode = true,
            OpponentPlayerData = GameStartUpData.PlayerData.FromPhotonPlayer(PhotonNetwork.playerList.Except(new[] { Player }).First()),
            GameRoomGroupInfo = GameRoomGroupInfo,
            GameProgress = new GameProgress()
        });
    }

    [Serializable]
    public struct GameStartEventData
    {
        [SerializeField] private int _photonId;
        [SerializeField] private int _unitType;


        public int PhotonId
        {
            get { return _photonId; }
            set { _photonId = value; }
        }

        public UnitType UnitType
        {
            get { return (UnitType) _unitType; }
            set { _unitType = (int) value; }
        }
    }
}