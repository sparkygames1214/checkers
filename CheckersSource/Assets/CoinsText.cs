﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CoinsText : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private float _animateTime = 0.5f;

    private int _currentCoins;
    private Coroutine _coroutine;

    private int CurrentCoins
    {
        get { return _currentCoins; }
        set
        {
            _text.text = value.ToString();
            _currentCoins = value;
        }
    }

    private int _currentTargetCoins;

    private void Awake()
    {
        if(_text==null)
        _text = GetComponent<Text>();

        
    }

    private void OnEnable()
    {
        CurrentCoins = AuthManager.Player.Coins;
        _currentTargetCoins = CurrentCoins;
    }

    private void OnDisable()
    {
        if(_coroutine!=null)
        AnimationManager.CancelCoroutine(_coroutine);
    }

    private void Update()
    {
        if(_currentTargetCoins!=AuthManager.Player.Coins)
            CoinsUpdated(AuthManager.Player.Coins);
    }

    private void CoinsUpdated(int coins)
    {
        if (_coroutine!=null)
        {
            AnimationManager.CancelCoroutine(_coroutine);
        }

        _currentTargetCoins = AuthManager.Player.Coins;
        _coroutine = AnimationManager.BasicMoveTowards(f => { CurrentCoins = (int) Mathf.Lerp(CurrentCoins, coins, f); }, () => { _coroutine = null; },1/_animateTime);
    }
}