﻿using System;
using System.Collections.Generic;

namespace Game
{
    public class MoveRequest
    {
        public IUnit Unit { get; set; }
        public IEnumerable<Move> Moves{ get; set; }
    }


    public class MoveTask
    {
        //TODO:Specify Path and unit info
        public IEnumerable<Move> Moves{ get; set; }
        private readonly Action _onCompleted;

        /// <summary>
        /// Mark as MarkAsComplete after Moved
        /// </summary>
        public void MarkAsComplete()
        {
            if (_onCompleted != null) _onCompleted();
        }

        public MoveTask(Action onCompleted=null)
        {
            _onCompleted = onCompleted;
        }
       
       
    }
}