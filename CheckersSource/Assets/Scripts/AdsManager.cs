﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;


public class AdsManager : MonoBehaviour, IUnityAdsListener
{
	public static AdsManager Instance { get; private set; }

	

	// Start is called before the first frame update
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}
#if UNITY_IOS
	string gameId = "3489098";
#elif UNITY_ANDROID
	string gameId = "3489099";
#endif
	string myPlacementId = "rewardedVideo";


	void Start()
	{
		Advertisement.Initialize(gameId, true);
	}

	public  void ShowInterstitialAds()
	{
		Advertisement.Show();
	}

	public void RewardedAds()
	{
		Advertisement.Show(myPlacementId);
	}

	// Implement IUnityAdsListener interface methods:
	public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
	{

		// Define conditional logic for each ad completion status:
		if (showResult == ShowResult.Finished)
		{
			AuthManager.Player.AddCoins(300);
		}
		else if (showResult == ShowResult.Skipped)
		{
			// Do not reward the user for skipping the ad.
		}
		else if (showResult == ShowResult.Failed)
		{
			Debug.LogWarning("The ad did not finish due to an error.");
		}
	}

	public void OnUnityAdsReady(string placementId)
	{
		// If the ready Placement is rewarded, show the ad:
		if (placementId == myPlacementId)
		{
			Advertisement.Show(myPlacementId);
		}
	}

	public void OnUnityAdsDidError(string message)
	{
		// Log the error.
	}

	public void OnUnityAdsDidStart(string placementId)
	{
		// Optional actions to take when the end-users triggers an ad.
	}
}
