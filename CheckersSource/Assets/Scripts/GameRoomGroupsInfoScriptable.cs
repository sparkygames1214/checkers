﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;



public class GameRoomGroupsInfoScriptable : ScriptableObject,IEnumerable<IGameRoomGroupInfo>
{
    public const string DEFAULT_FILE_NAME = "GameRoomGroupsInfoScriptable";

    [SerializeField] private List<GameRoomGroupGroupInfo> _gameRoomInfos;

    public IEnumerable<IGameRoomGroupInfo> GameRoomInfos
    {
        get { return _gameRoomInfos.Select(info => (IGameRoomGroupInfo) info); }
    }

    public IEnumerator<IGameRoomGroupInfo> GetEnumerator()
    {
        
        return GameRoomInfos.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        
        return GetEnumerator();
    }
}