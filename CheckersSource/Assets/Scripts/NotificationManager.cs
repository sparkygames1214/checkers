﻿

using System;
using Firebase.Messaging;
#if UNITY_ANDROID
using System;
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif

public class NotificationManager : Singleton<NotificationManager>
{
	private void Start()
	{
		Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
		{
			var dependencyStatus = task.Result;
			if (dependencyStatus == Firebase.DependencyStatus.Available)
			{
				// Create and hold a reference to your FirebaseApp,
				// where app is a Firebase.FirebaseApp property of your application class.
				//   app = Firebase.FirebaseApp.DefaultInstance;

				// Set a flag here to indicate whether Firebase is ready to use by your app.
			}
			else
			{
				UnityEngine.Debug.LogError(System.String.Format(
					"Could not resolve all Firebase dependencies: {0}", dependencyStatus));
				// Firebase Unity SDK is not safe to use here.
			}
		});
		FirebaseMessaging.TokenReceived += OnTokenReceived;
		FirebaseMessaging.MessageReceived += OnMessageReceived;
	}

	private void OnMessageReceived(object sender, MessageReceivedEventArgs e)
	{

	}

	private void OnTokenReceived(object sender, TokenReceivedEventArgs e)
	{

	}

	private void OnApplicationPause(bool pause)
	{
		if (pause)
		{
			ScheduleNotification("Your reward is ready!", "Daily Reward awaits you!", DailyRewardPanel.LastRewardTime.AddHours(24));
		}
		else
		{
			CancelAllNotification();
		}
	}

	public static void ScheduleNotification(string title, string message, DateTime date)
	{
#if UNITY_ANDROID
		var channelId = "simple_notification";

		var c = new AndroidNotificationChannel()
		{
			Id = channelId,
			Name = "Default Channel",
			Importance = Importance.High,
			Description = "Generic notifications",
		};
		AndroidNotificationCenter.RegisterNotificationChannel(c);

		var notification = new AndroidNotification();
		notification.Title = title;
		notification.Text = message;
		notification.FireTime = date;
		AndroidNotificationCenter.SendNotification(notification, channelId);

#elif UNITY_IOS
        var timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = date.Subtract(DateTime.Now),
            Repeats = false
        };

        var notification = new iOSNotification()
        {
            Title = title,
            Body = message,
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);

#endif
	}

	public static void CancelAllNotification()
	{
#if UNITY_ANDROID
		AndroidNotificationCenter.CancelAllNotifications();
#elif UNITY_IOS
        iOSNotificationCenter.RemoveAllScheduledNotifications();
#endif

	}
}