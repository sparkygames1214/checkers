﻿using UnityEngine;
using UnityEngine.UI;

public class ErrorPanel:ShowHidable
{
    [SerializeField] private TMPro.TMP_Text _titleTxt;
    [SerializeField] private TMPro.TMP_Text _messageTxt;
    private ViewModel _mViewModel;

    public virtual ViewModel MViewModel
    {
        get { return _mViewModel; }
        set
        {
            _titleTxt.text = value.Title;
            _messageTxt.text = value.Message;
            _mViewModel = value;
        }
    }


    public class ViewModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
    }
}

