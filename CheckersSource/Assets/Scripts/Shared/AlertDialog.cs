﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AlertDialog : ShowHidable
{

    [SerializeField] private TMPro.TMP_Text _titleTxt;
    [SerializeField] private TMPro.TMP_Text _messageTxt;
    [SerializeField] private List<Button> _buttons = new List<Button>();

    private ViewModel _mViewModel;

    public ViewModel MViewModel
    {
        get { return _mViewModel; }
        set
        {
            var list = value.Buttons.ToList();
            if (list.Count>_buttons.Count)
            {
                throw new Exception("Too Many Buttons");
            }

            for (var i = 0; i < _buttons.Count; i++)
            {
                var button = _buttons[i];

                //Button Visiablity
                button.gameObject.SetActive(i<list.Count);

                if (i < list.Count)
                {
                    var buttonAndAction = list[i];
                    button.GetComponentInChildren<Text>().text = buttonAndAction.Title;
                    button.onClick.RemoveAllListeners();
                    button.onClick.AddListener(() =>
                    {
                        OnClickedButton(buttonAndAction.Action);
                    });
                }
            }

            _titleTxt.text = value.Title;
            _messageTxt.text = value.Message;

            _mViewModel = value;
        }
    }

    void OnClickedButton(Action action)
    {
        Hide();
        if (action != null) action();
    }

    public struct ViewModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public IEnumerable<ButtonAndAction> Buttons { get; set; }
    }


    public struct ButtonAndAction
    {
        public string Title { get; set; }
        public Action Action { get; set; }
    }
}
