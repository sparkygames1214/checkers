﻿using UnityEngine;

namespace Shared
{
    // ReSharper disable once HollowTypeName
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance { get; set; }

        [SerializeField] private CoinsItemsPanel _coinsItemPanel;
        [SerializeField] private AlertDialog _alertDialog;
        [SerializeField] private ProgressPanel _progressPanel;
        [SerializeField] private ErrorPanel _errorPanel;

        public static CoinsItemsPanel CoinsItemPanel => Instance._coinsItemPanel;
        public static AlertDialog AlertDialog => Instance._alertDialog;
        public static ProgressPanel ProgressPanel => Instance._progressPanel;
        public static ErrorPanel ErrorPanel => Instance._errorPanel;


        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

            var connectivityManager = ConnectivityManager.Instance;
        }
    }
}