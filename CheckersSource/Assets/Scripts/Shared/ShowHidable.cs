﻿using System;
using UnityEngine;

public class ShowHidable:MonoBehaviour
{

    protected static readonly int SHOW_HASH = Animator.StringToHash("Show");
    protected static readonly int HIDE_HASH = Animator.StringToHash("Hide");
   
    [SerializeField] protected Animator anim;

    public event EventHandler<bool> ShowStateChanged; 

    public bool Showing
    {
        get { return gameObject.activeSelf;}
        protected set
        {
            if (value == Showing)
            {
                return;
            }

            gameObject.SetActive(value);

            ShowStateChanged?.Invoke(this,value);
        }
    }



    // ReSharper disable once FlagArgument
    public virtual void Show(bool animate = true, Action completed = null)
    {
        if (Showing)
            throw new InvalidOperationException();

        Showing = true;
        if (animate && anim != null)
        {
            anim.Play(SHOW_HASH);
            var targetTime = Time.time + anim.GetCurrentAnimatorStateInfo(0).length;
            var call = LateCall.Create();
            call.transform.parent = transform.parent;
            call.Call(() => targetTime < Time.time, completed);
        }
        else
        {
            completed?.Invoke();
        }

    }

//     ReSharper disable once FlagArgument
    public virtual void Hide(bool animate=true, Action completed = null)
    {
        if (!Showing)
            throw new InvalidOperationException();
        
        if (animate && anim != null)
        {
            anim.Play(HIDE_HASH);
            var targetTime = Time.time + anim.GetCurrentAnimatorStateInfo(0).length;
            var lateCall = LateCall.Create();
            lateCall.transform.parent = transform.parent;
            lateCall.Call(() => targetTime < Time.time,()=>
            {
                Showing = false;
                completed?.Invoke();
            });
        }
        else
        {
            Showing = false;
            completed?.Invoke();
        }
    }

    public void Exit()
    {
        Hide();
    }

}