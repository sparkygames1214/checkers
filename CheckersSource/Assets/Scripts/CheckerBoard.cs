﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using Game;
using Model;
using UnityEngine;
using Random = UnityEngine.Random;


// ReSharper disable once CheckNamespace
namespace Game
{
    [ExecuteInEditMode]
    public class CheckerBoard : MonoBehaviour, IInilizable, IRefreshable, ICheckerBoard
    {
        [SerializeField] private Vector2 _boardPhysicalSize;
        [SerializeField] private VecInt _boardSize = new VecInt(8, 8);
        [SerializeField] private Transform _boxContainer;
        [SerializeField] private Box _boxPrefab;
        [SerializeField] private List<Transform> _blackFreeHoldBoxes;
        [SerializeField] private List<Transform> _whiteFreeHoldBoxes;

        public Vector2 UnitSize => new Vector2(_boardPhysicalSize.x / Mathf.Max(1, _boardSize.x),
            _boardPhysicalSize.y / Mathf.Max(1, _boardSize.y));

        public Vector2[,] BoardPositions
        {
            get
            {
                var boardPositions = new Vector2[_boardSize.x, _boardSize.y];

                for (var i = 0; i < boardPositions.GetLength(0); i++)
                {
                    for (var j = 0; j < boardPositions.GetLength(1); j++)
                    {
                        boardPositions[i, j] =
                            GetPositionForCoordinate(i, j);
                    }
                }

                return boardPositions;
            }
        }



        public IBox[,] Boxes
        {
            get
            {
                var boxes = new IBox[_boardSize.x, _boardSize.y];
                for (var i = 0; i < boxes.GetLength(0); i++)
                {
                    for (var j = 0; j < boxes.GetLength(1); j++)
                    {
                        boxes[i, j] = _boxList[i * _boardSize.y + j];
                    }
                }

                return boxes;
            }
        }

        public bool Initialized { get; private set; }

        private readonly List<Box> _boxList = new List<Box>();

        public IEnumerable<Box> BoxesEnumerable => _boxList;


        public VecInt BoardSize
        {
            get { return _boardSize; }
            set { _boardSize = value; }
        }

        private Vector2 GetPositionForCoordinate(int i, int j)
        {
            var leftMostPoint = -_boardPhysicalSize / 2;
            return leftMostPoint + UnitSize / 2 + new Vector2(i * UnitSize.x, j * UnitSize.y);
        }


        // Update is called once per frame

        private void OnDrawGizmos()
        {
            var rect = new Rect(-(_boardPhysicalSize / 2).x, -(_boardPhysicalSize / 2).y, _boardPhysicalSize.x,
                _boardPhysicalSize.y);

            Gizmos.color = Color.cyan;
            //Draw Vertical lines
            for (var i = 0; i <= _boardSize.x; i++)
            {
                var bottom = new Vector2(rect.x + i * UnitSize.x, rect.y);
                Gizmos.DrawLine(transform.TransformPoint(bottom),
                    transform.TransformPoint(bottom + Vector2.up * rect.height));
            }

            //Draw Vertical lines
            for (var i = 0; i <= _boardSize.x; i++)
            {
                var left = new Vector2(rect.x, rect.y + i * UnitSize.y);
                Gizmos.DrawLine(transform.TransformPoint(left),
                    transform.TransformPoint(left + Vector2.right * rect.width));
            }
        }

        public void Init()
        {
            if (Initialized)
            {
                return;
            }

            Refresh();
            Initialized = true;
        }

        // ReSharper disable once MethodTooLong
        public void Refresh()
        {
            _boxList.Clear();
            foreach (Transform go in _boxContainer)
            {
                _boxList.Add(go.GetComponent<Box>());
            }

            while (_boxList.Count < _boardSize.x * _boardSize.y)
            {
                Box box;
#if UNITY_EDITOR
                if (UnityEditor.PrefabUtility.GetPrefabObject(_boxPrefab) != null)
                {
                    box = ((GameObject) UnityEditor.PrefabUtility.InstantiatePrefab(_boxPrefab.gameObject))
                        .GetComponent<Box>();
                }
                else
#endif
                {
                    box = Instantiate(_boxPrefab);
                }

                box.transform.parent = _boxContainer;
                _boxList.Add(box);
            }

            while (_boxList.Count > _boardSize.x * _boardSize.y)
            {
                var box = _boxList[_boxList.Count - 1];
                _boxList.RemoveAt(_boxList.Count - 1);


                if (!Application.isPlaying)
                {
#if UNITY_EDITOR
                    UnityEditor.EditorApplication.CallbackFunction act = null;
                    var ctx = box;
                    //                    Debug.Log("Cell:" + cell);
                    //                    DestroyImmediate(cell.gameObject);

                    act = () =>
                    {
                        //                        Debug.Log("Destroy cell:" + (ctx));
                        if (ctx != null)
                        {
                            DestroyImmediate(ctx.gameObject);
                        }

                        // ReSharper disable once AccessToModifiedClosure
                        // ReSharper disable once DelegateSubtraction
                        UnityEditor.EditorApplication.delayCall -= act;
                    };

                    UnityEditor.EditorApplication.delayCall += act;
                    //                    act();
#endif
                }
                else
                {
                    DestroyImmediate(box.gameObject);
                }
            }

            //Setup the box
            for (var i = 0; i < _boardSize.x; i++)
            {
                for (var j = 0; j < _boardSize.y; j++)
                {
                    var box = _boxList[i * _boardSize.x + j];
                    box.transform.position = transform.TransformPoint(GetPositionForCoordinate(i, j));
                    box.Size = UnitSize;
                    box.BoxType = (j % 2 == 1 && (i % 2 == 0)) || j % 2 == 0 && (i % 2 == 1)
                        ? BoxType.White
                        : BoxType.Black;
                    box.Coordinate = new VecInt(i, j);
                }
            }
        }

        public Transform GetFreeHolder(UnitType type)
        {
            return type == UnitType.White ? _whiteFreeHoldBoxes.First() : _blackFreeHoldBoxes.First();
        }

        public void HoldTheUnitTo(IUnit unit, Transform box)
        {
            unit.transform.position = box.transform.position;
            if (unit.UnitType == UnitType.White)
            {
                _whiteFreeHoldBoxes.Remove(box);
            }

            if (unit.UnitType == UnitType.Black)
            {
                _blackFreeHoldBoxes.Remove(box);
            }
        }
    }
}


public static class ExtensionFunctions
{

    public static MoveData ToData(this Move move,IBoardData boardData)
    {
        return new MoveData
        {
            StartBox = boardData[move.StartBox.Coordinate.x, move.StartBox.Coordinate.y],
            EndBox = boardData[move.EndBox.Coordinate.x, move.EndBox.Coordinate.y],
            DestroyableUnit = move.DestroyableUnit!=null? boardData[move.DestroyableUnit.Box.Coordinate.x, move.DestroyableUnit.Box.Coordinate.y]
                .Unit:null
        };
    }

    public static MoveInfo FromMoveInfoData(this ICheckerBoard board, MoveInfoData data)
    {
        return new MoveInfo
        {
            Unit = board.Boxes[data.Unit.Coordinate.x, data.Unit.Coordinate.y].Unit,
            Moves = data.Moves.Select(board.FromMoveData)
        };
    }

    public static MoveInfo[] FromMoveInfoData(this ICheckerBoard board, MoveInfoData[] data)
    {
        return data.Select(board.FromMoveInfoData).ToArray();
    }

    public static IEnumerable<MoveInfo[]> FromMoveInfoData(this ICheckerBoard board, IEnumerable<MoveInfoData[]> data)
    {
        return data.Select(board.FromMoveInfoData).ToArray();
    }

    public static Move FromMoveData(this ICheckerBoard board, MoveData moveData)
    {
        return new Move
        {
            StartBox = board.Boxes[moveData.StartBox.Coordinate.x, moveData.StartBox.Coordinate.y],
            EndBox = board.Boxes[moveData.EndBox.Coordinate.x, moveData.EndBox.Coordinate.y],
            DestroyableUnit = moveData.DestroyableUnit!=null?
                board.Boxes[moveData.DestroyableUnit.Coordinate.x, moveData.DestroyableUnit.Coordinate.y].Unit:null
        };
    }

    public static IBoardData ToBoardData(this ICheckerBoard board)
    {
        return new BoardData(board.BoxesEnumerable.Select(box =>
        {
            var boxData = new BoxData
            {
                Coordinate = box.Coordinate
            };

            boxData.Unit = box.Unit == null
                ? null
                : new UnitData
                {
                    Crowned = box.Unit.Crowned,
                    UnitType = box.Unit.UnitType,
                    Box = boxData,
                    Coordinate = boxData.Coordinate,
                };
            return boxData;
        }), board.BoardSize);
    }

    public static void Foreach<T>(this IEnumerable<T> enumerable, Action<T> action)
    {
        foreach (var x1 in enumerable)
        {
            if (action != null) action(x1);
        }
    }

    public static T GetRandom<T>(this IEnumerable<T> enumerable)
    {
        var list = enumerable.ToList();
        return list.Count == 0 ? default(T) : list[Random.Range(0, list.Count)];
    }

    public static void Randomize<T>(this IList<T> list)
    {
        var newList = new List<T>();
        while (list.Count>0)
        {
            var range = Random.Range(0,list.Count);
            newList.Add(list[range]);
            list.RemoveAt(range);
        }
        newList.ForEach(list.Add);
        
    }

    public static void Foreach<T>(this IEnumerable<T> enumerable, Action<T, int> action)
    {
        int i = 0;
        foreach (var x1 in enumerable)
        {
            action?.Invoke(x1, i++);
        }
    }

    public static IEnumerable<T> TakeFromBackward<T>(this IEnumerable<T> enumerable, int count)
    {
        var list = enumerable.ToList();
        if (list.Count<count)
        {
            throw new Exception("Take count cannot be more than enumerable count!");
        }

        for (var i = 0; i < list.Count; i++)
        {
            if (i >= list.Count - count)
               yield  return list[i];
        }
    }

    public static string GetPlayFabId(this PhotonPlayer player)
    {
        return player.CustomProperties.ContainsKey("PlayFabId") ? (string) player.CustomProperties["PlayFabId"] : null;
    }

    public static void SetPlayFabId(this PhotonPlayer player,string id)
    {
        player.SetCustomProperties(new Hashtable{{ "PlayFabId" ,id} }) ;
    }

    public static string GetAvatarUrl(this PhotonPlayer player)
    {
        return (player.CustomProperties.ContainsKey("Avatar") ? (string)player.CustomProperties["Avatar"] : null);
    }

    public static void SetAvatarUrl(this PhotonPlayer player,string url)
    {
        player.SetCustomProperties(new Hashtable{{"Avatar",url}});
    }

    public static UnitType GetOpponent(this UnitType type)
    {
        return type == UnitType.White ? UnitType.Black : UnitType.White;
    }

    public static BoxType ToBoxType(this UnitType type)
    {
        return type == UnitType.White ? BoxType.White : BoxType.Black;
    }

    public static UnitType ToUnitType(this BoxType type)
    {
        return type == BoxType.White ? UnitType.White : UnitType.Black;
    }


    public static IDictionary<T, TJ> ConvertDict<T, TJ, TK, TL>(this Dictionary<TK, TL> dict,Func<KeyValuePair<TK,TL>,T> keySelector,Func<KeyValuePair<TK,TL>,TJ> valueSelector)
    {
        if(keySelector == null || valueSelector == null)
            throw new NullReferenceException();

        var newDict = new Dictionary<T,TJ>();
        foreach (var pair in dict)
        {
            newDict.Add(keySelector(pair),valueSelector(pair));
        }

        return newDict;
    }

    public static Sprite ToFullSizeSprite(this Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
    }
}