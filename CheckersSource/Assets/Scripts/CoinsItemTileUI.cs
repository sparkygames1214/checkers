﻿using System;
using Model;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CoinsItemTileUI : MonoBehaviour,IPointerClickHandler
{
    public event EventHandler Clicked;   

    [SerializeField] private Image _img;
    [SerializeField] private TMPro.TMP_Text _nameTxt;
    [SerializeField] private TMPro.TMP_Text _priceTxt;
    private ViewModel _mViewModel;

    public ViewModel MViewModel
    {
        get { return _mViewModel; }
        set
        {
            _mViewModel = value;
            _img.sprite = value.CoinItem.Icon;
            _nameTxt.text = string.Format("{0}", value.CoinItem.Name);
            _nameTxt.text = _nameTxt.text.Replace('#', '\n');
            _priceTxt.text= value.CoinItem.Price.ToString("N") + "$";
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Clicked != null) Clicked(this, EventArgs.Empty);
    }



    public struct ViewModel
    {
        public ICoinItem CoinItem { get; set; }
    }

}