﻿using System;
using System.Collections.Generic;
using Model;
using UnityEngine;

public class CoinsItemsPanel : ShowHidable
{
    [SerializeField] private RectTransform _contentPanel;
    [SerializeField] private CoinsItemTileUI _coinItemTilePrefab;

    private readonly List<CoinsItemTileUI> _tileList = new List<CoinsItemTileUI>();

    private ProgressPanel ProgressPanel
    {
        get { return Shared.UIManager.ProgressPanel; }
    }


    private void Awake()
    {
        LateCall.Create().Call(() => ResourceManager._Initialized, () =>
        {
            foreach (var coinsItem in ResourceManager.CoinsItems)
            {
                var coinsItemTileUI = Instantiate(_coinItemTilePrefab);
                coinsItemTileUI.MViewModel = new CoinsItemTileUI.ViewModel
                {
                    CoinItem = coinsItem
                };
                coinsItemTileUI.transform.parent = _contentPanel;
                coinsItemTileUI.transform.localScale = Vector3.one;
                _tileList.Add(coinsItemTileUI);
               coinsItemTileUI.Clicked +=CoinsItemTileUIOnClicked;
            }
        });   
                
    }

    private void CoinsItemTileUIOnClicked(object sender, EventArgs eventArgs)
    {
        var coinsItemTileUI = (CoinsItemTileUI) sender;

        ProgressPanel.BackgroundAlpha = 0.5f;
        ProgressPanel.Show();
        
        ResourceManager.BuyCoins(coinsItemTileUI.MViewModel.CoinItem,new TaskAction<IItem, bool>((item, b) =>
        {
            ProgressPanel.Hide();
        }));
    }

    public void Close()
    {
       Hide();
    }
}