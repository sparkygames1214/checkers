﻿[System.Serializable]
public struct VecInt
{
    public int x, y;

    public VecInt(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override bool Equals(object obj)
    {
        if (obj is VecInt)
        {
            var vecInt = (VecInt)obj;
            return vecInt.x == x && vecInt.y == y;
        }
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {

        // ReSharper disable once NonReadonlyMemberInGetHashCode
        // ReSharper disable once NonReadonlyMemberInGetHashCode
        // ReSharper disable once NonReadonlyMemberInGetHashCode
        return x*1000+y;
    }

    public static  bool operator ==(VecInt vec1, VecInt vec2)
    {
        return vec1.x == vec2.x && vec1.y == vec2.y;
    }

    public static bool operator !=(VecInt vec1, VecInt vec2)
    {
        return !(vec1 == vec2);
    }

    public static VecInt operator +(VecInt vec1, VecInt vec2)
    {
        return new VecInt(vec1.x + vec2.x, vec1.y + vec2.y);
    }

    public static VecInt operator -(VecInt vec1, VecInt vec2)
    {
        return new VecInt(vec1.x - vec2.x, vec1.y - vec2.y);
    }
}