﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public static AudioManager Instance { get; private set; }

	public  AudioSource selectPiece;
	public AudioSource coins;

	// Start is called before the first frame update
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}

	public  void PlayPieceSelectingSound()
	{
		selectPiece.Play();
	}

	public void PlayCoinSound()
	{
		coins.Play();
	}
}
