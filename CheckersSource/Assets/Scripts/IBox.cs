﻿using System;
using Game;
using UnityEngine;

public interface IBox:IUnityObject
{
    event Action<IBox> Clicked;
    BoxType BoxType { get; set; }
    Vector2 Size { get; set; }
    IUnit Unit { get; set; }
    VecInt Coordinate { get; set; }
    bool Highlighted { get; set; }
}