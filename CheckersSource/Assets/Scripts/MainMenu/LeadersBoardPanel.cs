﻿// /*
// Created by Darsan
// */

using System;
using System.Collections;
using System.Collections.Generic;
using Model;
using PlayFab;
using UnityEngine;

namespace MainMenu
{
    public class LeadersBoardPanel : ShowHidable
    {
        [SerializeField] private GameObject _loadingEffect;
        [SerializeField] private LeaderTileUI _leaderTileUIPrefab;
        [SerializeField] private RectTransform _content;
		[SerializeField] private GameObject _offlineText;


        private readonly List<LeaderTileUI> _tiles = new List<LeaderTileUI>();


        public override void Show(bool animate = true, Action completed = null)
        {
			if (AuthManager.Player.Id != "Offline")
			{
				_offlineText.SetActive(false);
				LoadLeadersBoard();
			}
			else
			{
				_offlineText.SetActive(true);
			}
            base.Show(animate, completed);
        }

        private void LoadLeadersBoard()
        {

            _loadingEffect.SetActive(true);
            _tiles.ForEach(ui => Destroy(ui.gameObject));
            _tiles.Clear();

            var taskAction = new TaskAction<Player[], PlayFabError>((players, error) =>
            {
                for (var index = 0; index < players.Length; index++)
                {
                    var player = players[index];
                    var leaderTileUI = Instantiate(_leaderTileUIPrefab, _content);
                    leaderTileUI.MViewModel = new LeaderTileUI.ViewModel
                    {
                        Name = player.Name,
                        Value = player.PlayerStatics.Experience,
                        No = index+1,
                        ImageUrl = player.AvatarUrl
                    };
                    _tiles.Add(leaderTileUI);
                }
                _loadingEffect.SetActive(false);
            });
            AuthManager.GetLeadersboard(taskAction);


        }
    }
}