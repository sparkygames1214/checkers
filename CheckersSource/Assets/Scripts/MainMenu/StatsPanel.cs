﻿// /*
// Created by Darsan
// */

using System;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu
{
    public class StatsPanel : ShowHidable
    {
        [SerializeField] private TMPro.TMP_Text _totalExperienceTxt,_totalGamePlayedTxt,_gameWonTxt;

        public override void Show(bool animate = true, Action completed = null)
        {
            var statics = AuthManager.Player.PlayerStatics;
            _totalExperienceTxt.text = $"{statics.Experience}";
            _totalGamePlayedTxt.text = $"{statics.GamePlayed}";
            _gameWonTxt.text = $"{statics.GameWon}";
            base.Show(animate, completed);
        }
    }
}