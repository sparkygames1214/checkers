﻿// /*
// Created by Darsan
// */

using UnityEngine;
using UnityEngine.UI;

namespace MainMenu
{
    public class LeaderTileUI : MonoBehaviour
    {
        [SerializeField] private Image _img;
        [SerializeField] private TMPro.TMP_Text _nameTxt;
        [SerializeField] private TMPro.TMP_Text _valueTxt;
        [SerializeField] private TMPro.TMP_Text _noTxt;

        private ViewModel _mViewModel;

        public ViewModel MViewModel
        {
            get => _mViewModel;
            set
            {
                _nameTxt.text = value.Name;
                _valueTxt.text = value.Value.ToString();
                
                _noTxt.text = value.No.ToString();
                _mViewModel = value;
                if (_img != null)
                {
                    var _ = new ImageTask(value.ImageUrl, onComplete: (task) => _img.sprite = task.Sprite);
                }
            }
        }

        public struct ViewModel
        {
            public string Name { get; set; }
            public string ImageUrl { get; set; }
            public int No { get; set; }
            public int Value { get; set; }
        }


    }
}