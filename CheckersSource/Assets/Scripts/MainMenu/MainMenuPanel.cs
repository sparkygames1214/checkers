﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using UnityEngine;

namespace MainMenu
{
    public class MainMenuPanel : MonoBehaviour
    {
        [SerializeField] private RuleTileUI _ruleTileUIPrefab;
        [SerializeField] private RectTransform _content;

        private readonly List<RuleTileUI> _tiles = new List<RuleTileUI>();

        private RuleTileUI SelectedRuleTile
        {
            set
            {
                _tiles.Except(new[] { value }).Foreach(ui => ui.Selected = false);
                value.Selected = true;
                GameManager.SelectedRule = value.Rule;
            }
        }

        private void Awake()
        {
            foreach (var rule in GameManager.Rules)
            {
                var ruleTileUI = Instantiate(_ruleTileUIPrefab,_content);
                ruleTileUI.Rule = rule;
                _tiles.Add(ruleTileUI);
            }

            _tiles.ForEach(ui => ui.Clicked += TileUIOnClicked);
        }

        private void OnEnable()
        {
            SelectedRuleTile = _tiles.FirstOrDefault(ui => ui.Rule == GameManager.SelectedRule);
			StartCoroutine(ShowAdsAfterSeconds());
        }


        private void TileUIOnClicked(RuleTileUI ruleTileUI)
        {
            SelectedRuleTile = ruleTileUI;
        }


        public void OnClickOnlineButton()
        {
			if (AuthManager.CurrentOrLastLoginMethod != AuthManager.LoginMethod.Offline)
			{
				GameManager.OpenThePlayerSelectionScene(new PlayerSelectionStartUpData
				{
					GameRoomGroupInfo = ResourceManager.GameRoomGroupInfos.First()
				});
			}
			else
			{
				UIManager.Instance.OfflinePanel.Show();
			}
        }

        public void OnClickVsComputerButton()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                AuthManager.Player.ConsumeCoins(ResourceManager.GameRoomGroupInfos.First().BetAmount / 2);
            }
            GameManager.StartTheGame(new GameStartUpData
            {
                MyUnitType = UnitType.Black,
                GameRoomGroupInfo = ResourceManager.GameRoomGroupInfos.First(),
                GameProgress = new GameProgress(),
                OpponentPlayerData = new GameStartUpData.PlayerData
                {
                    Name = "AI",
                    AvatarUrl = ResourceManager.SampleAvatarUrls.FirstOrDefault()
                }
            });
        }

        public void OnClickVsPlayerButton()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                AuthManager.Player.ConsumeCoins(ResourceManager.GameRoomGroupInfos.First().BetAmount / 2);
            }
            GameManager.StartTheGame(new GameStartUpData
            {
                MyUnitType = UnitType.Black,
                GameRoomGroupInfo = ResourceManager.GameRoomGroupInfos.First(),
                GameProgress = new GameProgress(),
                OpponentPlayerData = new GameStartUpData.PlayerData
                {
                    Name = "Player 2",
                    AvatarUrl = ResourceManager.SampleAvatarUrls.FirstOrDefault()
                },
                VsPlayer = true
            });
        }


        public void OnClickShopButton()
        {
            Shared.UIManager.CoinsItemPanel.Show();
        }

        public void OnClickSettingsButton()
        {
            UIManager.Instance.SettingsPanel.Show();
        }

        public void OnClickHelpButton()
        {
			UIManager.Instance.HelpPanel.Show();
        }

        public void OnClickStats()
        {
            UIManager.Instance.StatsPanel.Show();
        }

        public void OnClickLeadersboard()
        {
            UIManager.Instance.LeadersBoardPanel.Show();
        }

		public void OnClickDailyGift()
		{
			UIManager.Instance.DailyRewardPanel.Show();
		}

		public void OnClickMenu()
		{
			UIManager.Instance.MenuPanel.Show();
		}

		public void OnClickRewardedAdsw()
		{
			AdsManager.Instance.RewardedAds();
		}

		IEnumerator ShowAdsAfterSeconds()
		{
			yield return new WaitForSeconds(30);
			AdsManager.Instance.ShowInterstitialAds();
		}
	}
}