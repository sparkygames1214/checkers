﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class HelpPanel : ShowHidable
{

	[SerializeField]
	private List<GameObject> Rules;
	public void Start()
	{
		Rules[0].SetActive(true);
	}
	public void OnClickBoardSelection()
	{
		//        UIManager.Instance.BoardSelectionPanel.Show();
	}

	public void Close()
	{
		Hide();
	}

	public void OnSelectionAmerican()
	{
		CloseAllRules();
		Rules[0].SetActive(true);
	}

	public void OnSelectionBrazillian()
	{
		CloseAllRules();
		Rules[3].SetActive(true);
	}

	public void OnSelectionCanadian()
	{
		CloseAllRules();
		Rules[2].SetActive(true);
	}

	public void OnSelectionCzech()
	{
		CloseAllRules();
		Rules[7].SetActive(true);
	}

	public void OnSelectionEnglish()
	{
		CloseAllRules();
		Rules[6].SetActive(true);
	}

	public void OnSelectionInternational()
	{
		CloseAllRules();
		Rules[1].SetActive(true);
	}

	public void OnSelectionItallian()
	{
		CloseAllRules();
		Rules[5].SetActive(true);
	}

	public void OnSelectionPortuguese()
	{
		CloseAllRules();
		Rules[4].SetActive(true);
	}

	public void CloseAllRules()
	{
		foreach (GameObject rules in Rules)
		{
			rules.SetActive(false);
		}
	}
}