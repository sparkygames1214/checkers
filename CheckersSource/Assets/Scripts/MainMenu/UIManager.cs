﻿using Model;
using UnityEngine;

namespace MainMenu
{
    // ReSharper disable once HollowTypeName
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance { get; private set; }

        [SerializeField] private TopBar _topBar;
        [SerializeField] private MainMenuPanel _mainMenuPanel;

        [SerializeField] private RoomSelectionPanel _roomSelectionPanel;
        [SerializeField] private SettingsPanel _settingsPanel;
        [SerializeField] private HelpPanel _helpPanel;
        [SerializeField] private StatsPanel _statsPanel;
        [SerializeField] private DailyRewardPanel _dailyRewardPanel;
        [SerializeField] private LeadersBoardPanel _leadersBoardPanel;
		[SerializeField] private MenuPanel _menuPanel;
		[SerializeField] private OfflinePanel _offlinePanel;



		private Player Player
        {
            get => _topBar.Player;
            set => _topBar.Player = value;
        }

        public MainMenuPanel MainMenuPanel => _mainMenuPanel;
        public RoomSelectionPanel RoomSelectionPanel => _roomSelectionPanel;

		public DailyRewardPanel DailyRewardPanel => _dailyRewardPanel;
        public LeadersBoardPanel LeadersBoardPanel => _leadersBoardPanel;
        public StatsPanel StatsPanel => _statsPanel;
        public SettingsPanel SettingsPanel => _settingsPanel;
		public MenuPanel MenuPanel => _menuPanel;
        public HelpPanel HelpPanel => _helpPanel;
		public OfflinePanel OfflinePanel => _offlinePanel;
		//
		//        private bool FirstTime
		//        {
		//            get { return PlayerPrefs.GetInt(nameof(UIManager)+nameof(FirstTime), 1) == 1;}
		//            set
		//            {
		//                PlayerPrefs.SetInt(nameof(UIManager)+nameof(FirstTime),value?1:0);
		//            } }

		private void Awake()
        {
            Instance = this;
            if (DailyRewardPanel.RewardAvailable)
            {
                _dailyRewardPanel.Show();
            }
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        private void OnEnable()
        {
            Player = AuthManager.Player;
            if (Player == null)
                AuthManager.LoggedIn += OnLoggedIn;
        }

        private void OnLoggedIn()
        {
            AuthManager.LoggedIn -= OnLoggedIn;
            Player = AuthManager.Player;
        }

        private void OnDisable()
        {
            if (Player == null)
                AuthManager.LoggedIn -= OnLoggedIn;
        }
    }
}