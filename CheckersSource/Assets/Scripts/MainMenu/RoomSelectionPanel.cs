﻿using System;
using Shared.UI;

namespace MainMenu
{
    public class RoomSelectionPanel : BaseRoomSelectionPanel
    {
        public delegate void EnterToTheRoom(IGameRoomGroupInfo info);

        public EnterToTheRoom EnterToTheRoomListener { get; set; }

        protected override void RoomTileUIOnClicked(object sender, GameRoomGroupTileUI.ViewModel e)
        {
            if (!CheckForNecessaryCoins(e.GameRoomGroupInfo.BetAmount / 2))
                return;
            if (EnterToTheRoomListener==null)
            {
                throw new Exception("Listener cannot be null");
            }

            EnterToTheRoomListener(e.GameRoomGroupInfo);
            Hide();
        }
    }
}