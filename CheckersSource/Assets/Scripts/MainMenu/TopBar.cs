﻿using System;
using Model;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu
{
    public class TopBar : MonoBehaviour
    {
        [SerializeField] private Text _nameTxt;
        [SerializeField] private Image _image;

        private Player _player;
        private ImageTask _profileImgTask;

        public Player Player
        {
            get { return _player; }
            set
            {

                if (_player !=null)
                {
                    _player.Updated -= PlayerOnDataUpdated;
                }

                _player = value;
                if (_player != null)
                {
                    _player.Updated += PlayerOnDataUpdated;
                }

                UpdateUI(_player);
            }
        }

        private void PlayerOnDataUpdated(Player player)
        {
            UpdateUI(player);
        }

        private void UpdateUI(Player value)
        {
            _nameTxt.text = value?.Name;
            if(value != null && !string.IsNullOrEmpty(value.AvatarUrl))
            {
                if(_profileImgTask!=null && !_profileImgTask.IsDone) _profileImgTask.Cancel();

                _profileImgTask = new ImageTask(value.AvatarUrl,onComplete: task => { _image.sprite = task.Sprite; });
            }

        }

        private void OnDestroy()
        {
            if (_profileImgTask != null && !_profileImgTask.IsDone)
            {
                _profileImgTask.Cancel();
            }
        }
    }
}