﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class BoardSizeDependSprite : MonoBehaviour
{
    [SerializeField] private CheckerBoard _board;
    [SerializeField] private List<SizeAndSprite> _sizeAndSprites = new List<SizeAndSprite>();

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => _board.Initialized);
        GetComponent<SpriteRenderer>().sprite =
            _sizeAndSprites.FirstOrDefault(sprite => sprite.size == _board.BoardSize.x).sprite;
    }


    [System.Serializable]
    public struct SizeAndSprite
    {
        public int size;
        public Sprite sprite;
    }
}