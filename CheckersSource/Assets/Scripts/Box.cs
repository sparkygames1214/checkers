﻿using System;
using Game;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Box : MonoBehaviour, IBox
{
    public event Action<IBox> Clicked;

    [SerializeField]private Sprite[] _blackAndWhiteSprites = new Sprite[2];
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private GameObject _highlightedGO;
    private IUnit _unit;

    public Vector2 Size
    {
        get { return transform.localScale; }
        set { transform.localScale = new Vector3(value.x, value.y, transform.localScale.z); }
    }

    public IUnit Unit
    {
        get { return _unit; }
        set
        {
            if (value!=null)
            {
                value.Size = Size;
            }

            _unit = value;
        }
    }

    public VecInt Coordinate { get; set; }

    public BoxType BoxType
    {
        get { return _spriteRenderer.sprite == _blackAndWhiteSprites[0] ? BoxType.Black : BoxType.White; }
        set { _spriteRenderer.sprite = value == BoxType.Black ? _blackAndWhiteSprites[0] : _blackAndWhiteSprites[1]; }
    }

    public bool Highlighted
    {
        get { return _highlightedGO.activeSelf;}
        set
        {
            _highlightedGO.SetActive(value);
        } }

    private void OnMouseDown()
    {
        if (Clicked != null) Clicked(this);
    }
}


public enum BoxType
{
    White,
    Black
}