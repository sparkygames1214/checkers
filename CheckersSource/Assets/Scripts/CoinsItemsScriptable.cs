﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Model
{
    public class CoinsItemsScriptable:ScriptableObject,IEnumerable<ICoinItem>
    {
        public const string DEFAULT_FILE_NAME = "CoinsItemsScriptable";

        [SerializeField] private List<CoinItem> _coinItems;
        public IEnumerable<ICoinItem> CoinItems
        {
            get { return _coinItems.Select(item => (ICoinItem)item); }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<ICoinItem> GetEnumerator()
        {
            return CoinItems.GetEnumerator();
        }
    }
}