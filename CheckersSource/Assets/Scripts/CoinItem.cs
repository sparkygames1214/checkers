﻿using UnityEngine;

namespace Model
{
    [System.Serializable]
    public class CoinItem : ICoinItem
    {
#pragma warning disable 649
        [SerializeField] private int _id;
        [SerializeField] private string _name;
        [SerializeField] private string _productId;
        [SerializeField] private float _price;
        [SerializeField] private float _value;
#pragma warning restore 649

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Sprite Icon { get; set; }

        public string ProductId
        {
            get { return _productId; }
        }

        public float Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public float Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}