﻿using System;
using PlayFab;
using UnityEngine;

namespace Auth
{
    public class LoginPanel : MonoBehaviour
    {
        private void Awake()
        {
            if (AuthManager.CurrentOrLastLoginMethod != AuthManager.LoginMethod.None &&
				AuthManager.CurrentOrLastLoginMethod != AuthManager.LoginMethod.Offline)
            {
                UIManager.Instance.Loading = true;
                AuthManager.TryAutoLogIn(new TaskAction<bool, Exception>((success, exception) =>
                {
                    UIManager.Instance.Loading = false;
                    if (success)
                        GameManager.LoadScene(Scene.MainMenu);
                }));
            }
        }

        public void OnClickFacebookBtn()
        {
            UIManager.Instance.Loading = true;
            AuthManager.LogInWithFacebook(new TaskAction<Exception>(exception =>
            {
                UIManager.Instance.Loading = false;
                if (exception != null)
                {
                    //TODO:Show the Error
                    return;
                }

               GameManager.LoadScene(Scene.MainMenu);
            }));
        }

        public void OnClickGuestBtn()
        {
            UIManager.Instance.Loading = true;
            AuthManager.LogInAsGuest(new TaskAction<PlayFabError>(error =>
            {
                UIManager.Instance.Loading = false;


                GameManager.LoadScene(Scene.MainMenu);

            }));
        }
    }

}

