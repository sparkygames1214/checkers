﻿using UnityEngine;

namespace Auth
{
    // ReSharper disable once HollowTypeName
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance { get;private set; }

        public bool Loading
        {
            get
            {
               
                return ProgressPanel.Showing;
            }
            set
            {
                if (value)
                {
                    ProgressPanel.BackgroundAlpha = .3f;
                    ProgressPanel.Show();
                }
                else
                {
                    ProgressPanel.Hide();
                }
            }
        }

        private ProgressPanel ProgressPanel => Shared.UIManager.ProgressPanel;

        private void Awake()
        {
            Instance = this;
        }
    }
}
