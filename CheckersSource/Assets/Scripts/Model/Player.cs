﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Debug = UnityEngine.Debug;

namespace Game
{
    public class Player : BasePlayer
    {

        private IUnit _selectedUnit;
        private readonly List<IBox> _highlightedBoxes = new List<IBox>();
        private readonly List<IBox> _targetedBoxes = new List<IBox>();
        private readonly List<MoveInfo[]> _currentMoveInfos = new List<MoveInfo[]>();

        private IUnit SelectedUnit
        {
            get => _selectedUnit;
            set
            {
                if (_selectedUnit == value)
                    return;

                _highlightedBoxes.ForEach(box => box.Highlighted = false);
                _highlightedBoxes.Clear();

                _targetedBoxes.ForEach(box => box.Clicked -= HighlightedBoxOnClicked);
                _targetedBoxes.Clear();
                if (value != null)
                {
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    var moveInfos = _currentMoveInfos.Where(infos => infos.Length>0).SelectMany(infos => infos)
                        .Where(infos => infos.Unit == value).ToList();
                    Debug.Log("Move Info Count:"+moveInfos.Count);
					if (moveInfos.Count > 0)
					{
						AudioManager.Instance.PlayPieceSelectingSound();
					}
                    moveInfos.Foreach(info => info.Moves.Foreach(move =>
                    {
                        if (!_highlightedBoxes.Contains(move.StartBox))
                        {
                            move.StartBox.Highlighted = true;
                            _highlightedBoxes.Add(move.StartBox);
                        }

                        var boxes = MoveUtils.GetMiddleBoxes(move,checkBoard);
                        boxes.Foreach(box =>
                        {
                            if (!_highlightedBoxes.Contains(box))
                            {
                                box.Highlighted = true;
                                _highlightedBoxes.Add(box);
                            }
                        });

                        if (!_highlightedBoxes.Contains(move.EndBox))
                        {
                            move.EndBox.Highlighted = true;
                            _highlightedBoxes.Add(move.EndBox);
                        }

                        var endBox = info.Moves.Last().EndBox;
                        if (!_targetedBoxes.Contains(endBox))
                        {
                            _targetedBoxes.Add(endBox);
                        }
                    }));

                    _targetedBoxes.ForEach(box => box.Clicked +=HighlightedBoxOnClicked);

                    stopwatch.Stop();

                }
                _selectedUnit = value;
            }
        }

        private void HighlightedBoxOnClicked(IBox box)
        {
            var task= RequestForMove(new MoveRequest
            {
                Moves = _currentMoveInfos.Where(infos => infos.Length>0).Where(infos => infos.First().Unit == SelectedUnit).SelectMany(infos => infos)
                    .Where(info => info.Moves.Last().EndBox == box)
                    .OrderByDescending(info => info.Moves.Count())
                    .First().Moves,
                Unit = SelectedUnit
            });

            if (task != null)
            {
                var unit = SelectedUnit;
                SelectedUnit = null;

                StopListenUnitsForClick();
                MovedInThisTurn = true;
                
                MakeMoves(unit,task.Moves, () =>
                {
                    task.MarkAsComplete();
                });
            }
        }


        protected override void TurnStarted()
        {
            base.TurnStarted();
            _currentMoveInfos.Clear();
            MoveCalculator.Board = checkBoard.ToBoardData();
            var moveInfoData = checkBoard.FromMoveInfoData(MoveCalculator.GetMoves(Type));
            _currentMoveInfos.AddRange(moveInfoData);
            StartListenUnitsForClick();
        }

        protected override void TurnEnded()
        {
            base.TurnEnded();
            _currentMoveInfos.Clear();
            SelectedUnit = null;
            if (!MovedInThisTurn)
            {
                StopListenUnitsForClick();
            }
        }

        private void StopListenUnitsForClick()
        {
            var activeUnits = ActiveUnits();
            activeUnits.Foreach(unit => unit.Clicked -= UnitOnClicked);
        }

        private void StartListenUnitsForClick()
        {
            var activeUnits = ActiveUnits();
            //Listen Click
            activeUnits.Foreach(unit => unit.Clicked += UnitOnClicked);
        }

        public override bool RequestToMove(MoveRequest moveRequest)
        {
            return false;
        }

        private void UnitOnClicked(IUnit unit)
        {
            SelectedUnit = unit;
        }
    }

}