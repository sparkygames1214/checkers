﻿using UnityEngine;

namespace Model
{
    [CreateAssetMenu]
    public class Theme:ScriptableObject
    {
        public Game.Theme themePrefab;
    }
}