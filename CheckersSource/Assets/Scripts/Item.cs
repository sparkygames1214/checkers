﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model
{
    
    public interface IItem
    {
        int Id { get;}
        string Name { get; set; }
        Sprite Icon { get; set; }
    }

    public interface IInAppItem:IItem
    {
        string ProductId { get; }
        float Price { get; set; }
        float Value { get; set; }
    }

    public interface ICoinItem : IInAppItem
    {
        
    }
}