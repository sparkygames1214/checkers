﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

// ReSharper disable once HollowTypeName
public partial class PersistenceImageManager:MonoBehaviour
{
    private static PersistenceImageManager _instance;

    public static PersistenceImageManager Instance
    {
        get { return _instance; }
        set
        {
            if (_instance == null)
            {
                var go = new GameObject("PersistenceImageManager");
                _instance = go.AddComponent<PersistenceImageManager>();
            }

            _instance = value;
        }
    }


    public string BASE_PATH
    {
        get { return Application.persistentDataPath; }
    }


    public static bool SaveImage(Sprite sp, ImageType type, string nameWithoutExtension)
    {
        return SaveImage(sp, string.Format("{0}/{1}.png", GetPathForType(type), nameWithoutExtension));
    }


    public static void LoadImage(ImageType type, string imgNameWithoutExtension,TaskAction<Sprite> completed)
    {
        LoadImage(string.Format("{0}/{1}.png", GetPathForType(type), imgNameWithoutExtension), completed);
    }


    public static void LoadImage(string path,TaskAction<Sprite> completed)
    {
        Instance.MLoadImage(path,completed);
    }

    public static bool SaveImage( Sprite sprite, string path)
    {
        return Instance.MSaveImage(path, sprite);
    }


    private static string GetPathForType(ImageType type)
    {
        return string.Format("{0}", type.ToString());
    }


    public enum ImageType
    {
        Profile,
        General
    }
}


// ReSharper disable once HollowTypeName
public partial class PersistenceImageManager
{
    private void MLoadImage(string path, TaskAction<Sprite> completed)
    {
        StartCoroutine(LoadImageCor(path, completed));
    }


    private bool MSaveImage(string path, Sprite sprite)
    {
        var fullPath = string.Format("{0}/{1}", BASE_PATH, path);
        try
        {
            Utils.CheckAndCreateDirectories(fullPath);
            File.WriteAllBytes(path, sprite.texture.GetRawTextureData());
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private IEnumerator LoadImageCor(string path, TaskAction<Sprite> completed)
    {
        var www = new WWW(string.Format("file:///{0}/{1}", BASE_PATH, path));
        yield return www;
        if (completed != null) completed.Invoke(www.error != null ? null : www.texture.ToFullSizeSprite());
    }
}