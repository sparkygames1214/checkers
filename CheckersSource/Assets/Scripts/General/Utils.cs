﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public static class Utils
{
    public static void CheckAndCreateDirectories(string path)
    {
        // ReSharper disable once TooManyChainedReferences
        var folders = path.Split('/')
            .Where(s => s.IndexOf(".", StringComparison.Ordinal) == -1)
            .ToList();

        var folderPath = "";


        for (var i = 0; i < folders.Count; i++)
        {
            folderPath += (i == 0 ? "" : "/") + string.Format("{0}", folders[i]);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
        }
    }

    public static IEnumerable<int> GetRange(int end, int start=0)
    {
        for (var i = start; i <= end; i++)
        {
            yield return i;
        }
    }
}