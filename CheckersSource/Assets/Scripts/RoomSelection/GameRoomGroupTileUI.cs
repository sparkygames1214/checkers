﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Shared.UI
{
    public class GameRoomGroupTileUI:MonoBehaviour
    {
        public event Action<object,ViewModel> Clicked;

        [SerializeField] private Text _roomNameText;
        [SerializeField] private Text _playerCountText;
        [SerializeField] private Text _betAmount;

        private ViewModel _mViewModel;


        public ViewModel MViewModel
        {
            get { return _mViewModel; }
            set
            {
                _roomNameText.text = value.GameRoomGroupInfo.RoomName;
                _playerCountText.text = GetUserFriendlilyMessageToPlayerCount(value.GameRoomGroupInfo.PlayerCount);
                _betAmount.text = $"{value.GameRoomGroupInfo.BetAmount}";
                _playerCountText.gameObject.SetActive(value.ShowPlayerCount);
                _mViewModel = value;
            }
        }

        private static string GetUserFriendlilyMessageToPlayerCount(int count)
        {
            return count == 0 ? "No Players!" : $"{count} Players";
        }

        public struct ViewModel
        {
            public IGameRoomGroupInfo GameRoomGroupInfo { get; set; }
            public bool ShowPlayerCount { get; set; }
        }

        public void OnClick()
        {
            Clicked?.Invoke(this, MViewModel);
        }
    }
}