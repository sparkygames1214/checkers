﻿using System.Linq;
using Shared.UI;

namespace RoomSelection
{
    public class RoomSelectionPanel:BaseRoomSelectionPanel
    {
        protected override void Awake()
        {
            base.Awake();
            UpdatePlayerOfRooms();
            InvokeRepeating(nameof(UpdatePlayerOfRooms),0,3);
        }


        protected override void RoomTileUIOnClicked(object sender, GameRoomGroupTileUI.ViewModel viewModel)
        {
            if(!CheckForNecessaryCoins(viewModel.GameRoomGroupInfo.BetAmount/2))
                return;
            GameManager.OpenThePlayerSelectionScene(new PlayerSelectionStartUpData
            {
                 GameRoomGroupInfo = viewModel.GameRoomGroupInfo
            });
        }

        // ReSharper disable once TooManyDeclarations
        private void UpdatePlayerOfRooms()
        {
            var groups = PhotonNetworkManager.Rooms.GroupBy(info => info.CustomProperties[CommonProperties.GAME_ROOM_KEY]).ToDictionary(infos => infos.Key);
            foreach (var roomTileUI in roomTileUIList)
            {
                var mViewModel = roomTileUI.MViewModel;
                var gameRoomInfo = (GameRoomGroupGroupInfo)mViewModel.GameRoomGroupInfo;
                gameRoomInfo.PlayerCount = groups.ContainsKey(mViewModel.GameRoomGroupInfo.Id)
                    ? groups[mViewModel.GameRoomGroupInfo.Id].Where(info => (string)info.CustomProperties[CommonProperties.RULE_KEY] == GameManager.SelectedRule.Id).Sum(info => info.PlayerCount)
                    : 0;
                mViewModel.GameRoomGroupInfo = gameRoomInfo;
                roomTileUI.MViewModel = mViewModel;
            }
        }

    }
}