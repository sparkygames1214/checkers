﻿using System.Collections.Generic;
using UnityEngine;

namespace Shared.UI
{
    public abstract class BaseRoomSelectionPanel : ShowHidable
    {
        [SerializeField] protected RectTransform contentPanel;
        [SerializeField] protected GameRoomGroupTileUI gameRoomGroupTilePrefab;
        [SerializeField] protected bool showPlayerCount;

        protected readonly List<GameRoomGroupTileUI> roomTileUIList = new List<GameRoomGroupTileUI>();

        protected AlertDialog AlertDialog => UIManager.AlertDialog;

        protected CoinsItemsPanel CoinsItemsPanel => UIManager.CoinsItemPanel;


        public bool ShowPlayerCount
        {
            get { return showPlayerCount; }
            set { showPlayerCount = value; }
        }

        protected virtual void Awake()
        {
            foreach (var gameRoomInfo in ResourceManager.GameRoomGroupInfos)
            {
                var roomTileUI = Instantiate(gameRoomGroupTilePrefab);
                roomTileUI.MViewModel = new GameRoomGroupTileUI.ViewModel
                {
                    GameRoomGroupInfo = gameRoomInfo,
                    ShowPlayerCount = ShowPlayerCount
                };
                roomTileUI.transform.parent = contentPanel;
                roomTileUI.Clicked += RoomTileUIOnClicked;
                roomTileUI.transform.localScale = Vector3.one;
                roomTileUIList.Add(roomTileUI);

            }

        }


        protected abstract void RoomTileUIOnClicked(object sender, GameRoomGroupTileUI.ViewModel e);


        protected bool CheckForNecessaryCoins(int necessaryCoins)
        {

            if (necessaryCoins > ResourceManager.Coins)
            {
                AlertDialog.MViewModel = new AlertDialog.ViewModel
                {
                    Title = "Not Enough Coins",
                    Message = "Not Enough Coins to enter the room.Do you want buy more coins?",
                    Buttons = new[]{new AlertDialog.ButtonAndAction
                    {
                        Title = "No",
                    },new AlertDialog.ButtonAndAction
                    {
                        Title = "Yes",
                        Action = () => CoinsItemsPanel.Show()
                    },}
                };
                AlertDialog.Show();
                return false;
            }

            return true;
        }
    }
}