﻿using UnityEngine;

namespace Game
{
    // ReSharper disable once HollowTypeName
    public class UIManager : MonoBehaviour,IInilizable
    {
        public static UIManager Instance { get;private set; }

        [SerializeField] private SinglePlayer.GameOverPanel _singlePlayerGameOverPanel;
        [SerializeField] private MultiPlayer.GameOverPanel _multiPlayerGameOverPanel;
        [SerializeField] private GamePlayPanel _gamePlayPanel;


        public bool Initialized { get; private set; }
        private LevelManager LevelManager => LevelManager.Instance;



        private void Awake()
        {
            Instance = this;
        }

        private void OnEnable()
        {
            LevelManager.GameOver += LevelManagerOnGameOver;
        }

        private void OnDisable()
        {
            LevelManager.GameOver -= LevelManagerOnGameOver;
        }

        private void LevelManagerOnGameOver(bool isWin)
        {
            if (LevelManager.IsNetworkMode)
            {
                _multiPlayerGameOverPanel.MViewModel = new MultiPlayer.GameOverPanel.ViewModel
                {
                    IsWin = isWin
                };
                _multiPlayerGameOverPanel.gameObject.SetActive(true);
            }
            else
            {
                _singlePlayerGameOverPanel.MViewModel = new SinglePlayer.GameOverPanel.ViewModel
                {
                    IsWin = isWin
                };
                _singlePlayerGameOverPanel.gameObject.SetActive(true);
            }
        }

        public void Init()
        {
            if (Initialized)
            {
                return;
            }

            _gamePlayPanel.Init(new GamePlayPanel.InitParams
            {
                GameHandler = LevelManager.GameHandler,
                MyPlayerUnitType = LevelManager.MyPlayer.Type
            });

            Initialized = true;
        }
    }
}