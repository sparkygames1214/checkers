﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Game.MultiPlayer
{
    public class GameOverPanel : MonoBehaviour
    {
        [SerializeField] private Button _wantPlayAgainButton,_menuButton;
        [SerializeField] private TMPro.TMP_Text _statusTxt;
        [SerializeField] private GameObject _winGroup, _lostGroup;
        [SerializeField] private TMPro.TMP_Text _coinTxt;

        public ViewModel MViewModel
        {
            get { return _mViewModel; }
            set
            {
                _winGroup.SetActive(value.IsWin);
                _lostGroup.SetActive(!value.IsWin);
                // ReSharper disable once PossibleInvalidOperationException
                _coinTxt.text = LevelManager.Instance.GameStartUpData.GameRoomGroupInfo
                    .BetAmount.ToString();
                _mViewModel = value;
            }
        }

        private ViewModel _mViewModel;

        private void OnEnable()
        {
            _wantPlayAgainButton.interactable = PhotonNetwork.room.PlayerCount >= 2;
        }


        public void OnClickMainMenu()
        {

            LevelManager.Instance.CancelGame();
        }

        public void OnClickWantToPlayAgain()
        {
            StartCoroutine(WantToPlayAgain());
        }

        private IEnumerator WantToPlayAgain()
        {
            NetworkLevelManager.Instance.WantToPlayAgain();
            _wantPlayAgainButton.interactable = false;
            _menuButton.interactable = false;
            _statusTxt.text = "Waiting...";

            while (PhotonNetwork.room.PlayerCount>=2)
            {
                yield return null;
            }

            _statusTxt.text = "Player Can't Play Right Now.";
            yield return new WaitForSeconds(2);
           LevelManager.Instance.CancelGame();
        }

        public struct ViewModel
        {
            public bool IsWin { get; set; }
        }
    }
}