﻿// /*
// Created by Darsan
// */

using Game;
using UnityEngine;
using UnityEngine.UI;

public class AvatarUI : MonoBehaviour,IProgressBar
{
    [SerializeField] private TMPro.TMP_Text _nameTxt;
    [SerializeField] private Image _img;
    [SerializeField] private Image _progressImg;
	[SerializeField] private Text _kills;


	private ViewModel _mViewModel;


    public bool Active
    {
        get { return _progressImg.gameObject.activeSelf;}
        set
        {
            
            _progressImg.gameObject.SetActive(value);
            if(value)
                Value = 0;
        }
    }

    public float Value
    {
        get { return 1-_progressImg.fillAmount;}
        set { _progressImg.fillAmount =1 - value; }
    }

	public int Kill
	{
		set { _kills.text = value.ToString(); }
	}

    public UnitType UnitType { get; set; }

    public ViewModel MViewModel
    {
        get { return _mViewModel; }
        set
        {
            if(_nameTxt!=null)
            _nameTxt.text = value.Name;
            if (_img != null)
            {
                var _ = new ImageTask(value.ImageUrl, onComplete: (task) => _img.sprite = task.Sprite);
            }
            _mViewModel = value;

        }
    }

    public struct ViewModel
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }

}