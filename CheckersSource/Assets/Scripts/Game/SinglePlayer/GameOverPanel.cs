﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.SinglePlayer
{
    public class GameOverPanel : MonoBehaviour
    {
        [SerializeField] private GameObject _winGroup, _lostGroup;
        [SerializeField] private TMPro.TMP_Text _coinTxt;

        public ViewModel MViewModel
        {
            get { return _mViewModel; }
            set
            {

                _winGroup.SetActive(value.IsWin);
                _lostGroup.SetActive(!value.IsWin);
                // ReSharper disable once PossibleInvalidOperationException
                _coinTxt.text = LevelManager.Instance.GameStartUpData.GameRoomGroupInfo
                    .BetAmount.ToString();
                _mViewModel = value;
            }
        }

        private ViewModel _mViewModel;


        public void OnClickRestart()
        {
            GameManager.StartTheGame(LevelManager.Instance.GameStartUpData);
        }

        public void OnClickMainMenu()
        {
LevelManager.Instance.CancelGame();
        }

        public struct ViewModel
        {
            public bool IsWin { get; set; }
        }

    }
}