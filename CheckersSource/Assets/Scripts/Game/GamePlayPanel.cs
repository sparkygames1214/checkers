﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class GamePlayPanel : MonoBehaviour,IInilizable<GamePlayPanel.InitParams>
    {

        [SerializeField] private Image _fadePanel;
        [SerializeField] private AvatarUI _playerAvatar,_opponentAvatar;

        private IGameHandler _gameHandler;
        private UnitType _myUnitType;

        public bool Initialized { get; private set; }


        private IGameHandler GameHandler
        {
            get { return _gameHandler; }
            set
            {
                if (_gameHandler!=null)
                {
                    throw new NotImplementedException("Currently Game Handler Can be only one time!");
                }

                _gameHandler = value;
                _gameHandler.TurnStart +=GameHandlerOnTurnStart;
                _gameHandler.TurnEnd +=GameHandlerOnTurnEnd;

            }
        }

        private void OnEnable()
        {
            LevelManager.GameOver +=LevelManagerOnGameOver;
        }

        private void LevelManagerOnGameOver(bool obj)
        {
            _playerAvatar.Active = false;
            _opponentAvatar.Active = false;
        }

        private void OnDisable()
        {
            LevelManager.GameOver -= LevelManagerOnGameOver;
        }

        private void GameHandlerOnTurnEnd(IPlayer player)
        {


        }

        private void GameHandlerOnTurnStart(IPlayer player)
        {

            _fadePanel.gameObject.SetActive(player.Type != _myUnitType);
        }


        public void Init(InitParams item)
        {
            if (Initialized)
            {
                return;
            }

            GameHandler = item.GameHandler;
            _myUnitType = item.MyPlayerUnitType;
            _playerAvatar.MViewModel = new AvatarUI.ViewModel
            {
                Name = LevelManager.Instance.MyPlayer.Name,
                ImageUrl = string.IsNullOrEmpty(LevelManager.Instance.MyPlayer.AvatarUrl)?ResourceManager.SampleAvatarUrls.GetRandom():LevelManager.Instance.MyPlayer.AvatarUrl
            };
            _opponentAvatar.MViewModel = new AvatarUI.ViewModel
            {
                Name = LevelManager.Instance.OpponentPlayer.Name,
                ImageUrl = string.IsNullOrEmpty(LevelManager.Instance.OpponentPlayer.AvatarUrl) ? ResourceManager.SampleAvatarUrls.Where(s => _playerAvatar.MViewModel.ImageUrl!=s)
                    .GetRandom(): LevelManager.Instance.OpponentPlayer.AvatarUrl
            };

            ((BasePlayer) LevelManager.Instance.MyPlayer).TimerUI = _playerAvatar;
            ((BasePlayer) LevelManager.Instance.OpponentPlayer).TimerUI = _opponentAvatar;

            Initialized = true;
        }


        public void ExitGame()
        {
            var alertDialog = Shared.UIManager.AlertDialog;
            alertDialog.MViewModel = new AlertDialog.ViewModel
            {
                Title = "Are you sure?",
                Message = "Bet will be lost when you quit the game",
                Buttons = new []{new AlertDialog.ButtonAndAction
                {
                    Title = "No",
                }, new AlertDialog.ButtonAndAction
                {
                    Title = "Yes",
                    Action = LevelManager.Instance.CancelGame
                }, }
            };
            alertDialog.Show();
        }

        public struct InitParams
        {
            public IGameHandler GameHandler { get; set; }
            public UnitType MyPlayerUnitType { get; set; }
        }
    }
}