﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

namespace Test
{
    public class TestCornerCoordinate : MonoBehaviour
    {
        private readonly List<IBox> _currentHighlightedList = new List<IBox>();
        private CheckerBoard _board;

        private void Start()
        {
            _board = FindObjectOfType<CheckerBoard>();
            var boxes = _board.Boxes;

            for (var i = 0; i < boxes.GetLength(0); i++)
            {
                for (var j = 0; j < boxes.GetLength(0); j++)
                {
                    boxes[i,j].Clicked += Clicked;
                }
            }
        }

        // ReSharper disable once TooManyDeclarations
        private void Clicked(IBox box)
        {
            _currentHighlightedList.ForEach(b => b.Highlighted = false);
            _currentHighlightedList.Clear();
            var boxes = _board.Boxes;
            var coordinates = (new CheckerMoveCalculator(_board)).GetCornerCoordinates(box.Coordinate);
            coordinates.Foreach(coordinate =>
            {
                if (coordinate != null)
                    coordinate.Foreach(i =>
                    {
                        var b = boxes[i.x, i.y];
                        b.Highlighted = true;
                        _currentHighlightedList.Add(b);
                    });
            } );
        }
    }
}