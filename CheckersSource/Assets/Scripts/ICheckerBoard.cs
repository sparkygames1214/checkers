﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public interface ICheckerBoard
    {
       VecInt BoardSize { get; }
        Vector2[,] BoardPositions { get; }
        IBox[,] Boxes { get; }
        Vector2 UnitSize { get; }
        void HoldTheUnitTo(IUnit unit, Transform box);
        Transform GetFreeHolder(UnitType type);
        IEnumerable<Box> BoxesEnumerable { get; }
    }
}