﻿using UnityEngine;

public class DontDestroyMono : MonoBehaviour
{
    private static DontDestroyMono _instance;

    public static DontDestroyMono Instance
    {
        get
        {
            if(_instance == null)
            {
                var go = new GameObject();
                _instance = go.AddComponent<DontDestroyMono>();
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    public static GameObject CreateChild(string name="Child")
    {
        var go = new GameObject(name);
        go.transform.parent = Instance.transform;
        return go;
    }
}