﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Model;
using UnityEngine;

// ReSharper disable once HollowTypeName
public class ResourceManager : Singleton<ResourceManager>, IInilizable
{

   

    // ReSharper disable once InconsistentNaming
    public static bool _Initialized => Instance != null && Instance.Initialized;


    public static PlayFabSharedSettings PlayFabShardSettings => Resources.Load<PlayFabSharedSettings>("PlayFabSharedSettings");

    [SerializeField] private GameRoomGroupsInfoScriptable _gameRoomGroupsInfoScriptable;
    [SerializeField] private CoinsItemsScriptable _coinsItemsScriptable;
    [SerializeField]private List<Theme> _themes = new List<Theme>();
    [SerializeField]private List<Sprite> _sampleAvatars = new List<Sprite>();
    [SerializeField]private List<FakePlayerData> _fakePlayerDatas = new List<FakePlayerData>();


    public static IEnumerable<IGameRoomGroupInfo> GameRoomGroupInfos => Instance.GameRoomGroupInfoScriptable;

    public static IEnumerable<FakePlayerData> FakePlayerDatas => Instance._fakePlayerDatas;
    public static IEnumerable<ICoinItem> CoinsItems => Instance._coinsItems;

    public static IEnumerable<Theme> Themes => Instance._themes;

    public static IEnumerable<Sprite> SampleAvatars => Instance._sampleAvatars;
    public static IEnumerable<string> SampleAvatarUrls => Instance._sampleAvatars.Select((url,i)=>$"local/{i}");

    private GameRoomGroupsInfoScriptable GameRoomGroupInfoScriptable
    {
        get
        {
            if (_gameRoomGroupsInfoScriptable == null)
            {
                _gameRoomGroupsInfoScriptable =
                    Resources.Load<GameRoomGroupsInfoScriptable>(GameRoomGroupsInfoScriptable.DEFAULT_FILE_NAME);
            }

            return _gameRoomGroupsInfoScriptable;
        }
    }

    private CoinsItemsScriptable CoinsItemsScriptable
    {
        get
        {
            if (_coinsItemsScriptable == null)
            {
                _coinsItemsScriptable =
                    Resources.Load<CoinsItemsScriptable>(CoinsItemsScriptable.DEFAULT_FILE_NAME);
            }

            return _coinsItemsScriptable;
        }
    }


    public static IGameRoomGroupInfo GetRoomGroupInfoById(string id) =>
        GameRoomGroupInfos.FirstOrDefault(info => info.Id == id);

    //Persistence 

    private Player Player => AuthManager.Player;

    public static int Coins => Instance.Player.PlayerData.Coins;

    private readonly Dictionary<string, Sprite> _urlVsSpriteDict = new Dictionary<string, Sprite>();
    private readonly List<ICoinItem> _coinsItems = new List<ICoinItem>();
    public bool Initialized { get; private set; }

    private Purchaser _purchaser;


    protected override void Awake()
    {
        base.Awake();
        Init();
    }

    public void Init()
    {
        if (Initialized)
        {
            return;
        }

        _purchaser = new Purchaser(CoinsItemsScriptable.Select(item => item.ProductId), new List<string>());
        _coinsItems.AddRange(CoinsItemsScriptable);

        Initialized = true;
    }

    private void LoadAndSaveImageLoc(string url, TaskAction<Sprite> completed)
    {
        if (_urlVsSpriteDict.ContainsKey(url.ToLower()))
        {
            completed?.Invoke(_urlVsSpriteDict[url.ToLower()]);
            return;
        }

        LoadImageLoc(url, new TaskAction<Sprite>(sprite =>
        {
            if (_urlVsSpriteDict.ContainsKey(url.ToLower()))
            {
                return;
            }

            _urlVsSpriteDict.Add(url.ToLower(), sprite);

            completed?.Invoke(sprite);
        }));
    }

    public static Sprite GetAvatarByUrl(string url)
    {
        var index = SampleAvatarUrls.ToList().IndexOf(url);
        return index>=0? SampleAvatars.ElementAtOrDefault(index) : null;
    }

//    public static void ConsumeCoins(int coins,TaskAction<bool>  completed=null)
//    {
//        // ReSharper disable once TooManyChainedReferences
//        var player = Instance.Player;
//        if (player.PlayerData.Coins < coins)
//        {
//            completed?.Invoke(false);
//        }
//        else
//        {
//            Instance.UpdateCoins(player.PlayerData.Coins - coins,completed);
//        }
//    }
//
//    public static void AddCoins(int coins, TaskAction<bool> completed=null)
//    {
//        // ReSharper disable once TooManyChainedReferences
//        var player = Instance.Player;
//        Instance.UpdateCoins(player.PlayerData.Coins + coins, completed);
//    }
//
//    private void UpdateCoins(int coins,TaskAction<bool> completed)
//    {
//        var playerData = Player.PlayerData;
//        playerData.Coins = coins;
//        Player.UpdatePlayerData(playerData,error =>
//        {
//            
//
//            completed?.Invoke(error == null);
//        });
//    }

    public static void BuyCoins(ICoinItem item, TaskAction<IItem, bool> completed)
    {
        Instance._purchaser.BuyProduct(item.ProductId, success =>
        {
            if (success)
            {
                // ReSharper disable once TooManyChainedReferences
                var player = Instance.Player;
                player.AddCoins((int)item.Value,new TaskAction<bool>(b =>
                {
                    completed?.Invoke(item, b);
                }));   
            }
            else
            {
                completed?.Invoke(item, false);
            }
        });
    }

    public static void LoadAndSaveImage(string url, TaskAction<Sprite> completed)
    {
        Instance.LoadAndSaveImageLoc(url, completed);
    }

    public static void LoadImage(string url, TaskAction<Sprite> completed)
    {
        Instance.LoadImageLoc(url, completed);
    }

    private void LoadImageLoc(string url, TaskAction<Sprite> completed)
    {
        StartCoroutine(CallWeb(new WWW(url), www =>
        {
            if (www.error != null || www.texture == null)
            {
                completed?.Invoke(null);
                return;
            }

            completed?.Invoke(www.texture.ToFullSizeSprite());
        }));
    }

    IEnumerator CallWeb(WWW www, Action<WWW> completed)
    {
        yield return www;
        completed?.Invoke(www);
    }

}

[System.Serializable]
public struct FakePlayerData
{
    public string name;
    public string avatarUrl;
}